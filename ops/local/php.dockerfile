FROM composer:2.0.8 AS composer
# @see https://hub.docker.com/r/jpswade/php7.4-fpm-alpine
FROM php:7.4-fpm
# https://ithelp.ithome.com.tw/articles/10246952
# 安裝 laravel 所需的 pre-request
RUN apt-get update && apt-get install -y \
    libzip-dev \
    libmagickwand-dev \
    unzip \
    git \
    vim \
    libpng-dev \
    supervisor \
    cron \
    htop
RUN pecl install redis-5.1.1 \
    && pecl install imagick-3.4.4 \
    && pecl install xdebug-2.8.1 \
    && docker-php-ext-enable redis xdebug imagick

COPY --from=composer /usr/bin/composer /usr/bin/composer

MAINTAINER Trevor

# 指定工作目錄
WORKDIR /app

RUN docker-php-ext-install bcmath opcache zip pdo_mysql
RUN docker-php-ext-install gd pcntl

RUN echo "PHP_INI_DIR=$PHP_INI_DIR" >> /tmp/php_ini_dir.log
RUN cp "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN echo "\n \
memory_limit=-1" >> "$PHP_INI_DIR/php.ini"

RUN echo "\n \
disable_function=passthru,exec,system,putenv,chroot,chgrp,chown,shell_exec,popen,proc_open" >> "$PHP_INI_DIR/php.ini"

RUN echo "access.log=/dev/null" >> /usr/local/etc/php-fpm.d/www.conf

# xdebug
COPY ./ops/local/php/conf.d/xdebug.ini $PHP_INI_DIR/conf.d/
# supervisor
COPY ./ops/local/supervisor/conf.d /etc/supervisor/conf.d/
# php 脚本
COPY ./ops/local/php.sh /

# php artisan schedule:run
RUN echo "* * * * * root php /app/projects/server/artisan schedule:run >> /dev/null 2>&1" >> /etc/crontab
RUN echo "\n \
export LS_OPTIONS='--color=auto' \n \
alias ls='ls \$LS_OPTIONS'\n \
alias ll='ls \$LS_OPTIONS -l'\n \
alias l='ls \$LS_OPTIONS -lA'\n \
alias rm='rm -i'\n \
alias cp='cp -i'\n \
alias artisan='php artisan'\n \
alias a='php artisan'\n \
alias mv='mv -i'" >> ~/.bashrc

# CMD php-fpm -F
RUN chmod +x /php.sh
CMD bash /php.sh


