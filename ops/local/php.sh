#!/bin/bash

# /app/projects/server
path=/app/projects/server

cd ${path}

echo 'now pwd...'
pwd

echo 'composer install....'
composer install

if [[ ! -f "${path}/.env" ]]; then
    # copy env
    cp ${path}/.env.local.example ${path}/.env
    # TODO check 權限
    php artisan cache:clear
    chmod -R 777 .env
    composer dump-autoload
    # key generate
    php artisan key:generate
    # migrate
    php artisan migrate --seed
    # JWT
    php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
    # 建立 JWT
    echo 'JWT secret create...'
    php artisan jwt:secret --force
fi

# 建立地址資料
echo 'Address Initialize...'
php artisan initialize:AddressPostalInitialize

# 建立 Cache
echo 'Cache Initialize...'
php artisan initialize:CacheInitialize
echo 'Y'

echo 'supervisor running...'
cd / && exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
