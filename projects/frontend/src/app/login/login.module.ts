import { NgModule } from '@angular/core';
// module
import { LoginRoutingModule } from './login-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { LoginComponent } from './login.component';

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    LoginRoutingModule,
    ShareModule,
  ]
})
export class LoginModule {
}
