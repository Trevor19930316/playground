import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { ProfileRightsComponent } from './profile-rights.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileRightsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ProfileRightsRoutingModule {
}
