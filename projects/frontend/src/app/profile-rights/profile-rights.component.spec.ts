import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileRightsComponent } from './profile-rights.component';

describe('ProfileRightsComponent', () => {
  let component: ProfileRightsComponent;
  let fixture: ComponentFixture<ProfileRightsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileRightsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileRightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
