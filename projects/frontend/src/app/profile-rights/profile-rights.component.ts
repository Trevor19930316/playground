import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/_service';

@Component({
  selector: 'app-profile-rights',
  templateUrl: './profile-rights.component.html',
  styleUrls: ['./profile-rights.component.scss']
})
export class ProfileRightsComponent implements OnInit {

  rights!: any;
  loading = true;

  constructor(
    private userService: UserService,
  ) { }

  private _fetchRights() {
    this.loading = true;
    this.userService.getRights().subscribe(res => {
      this.rights = res;
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchRights();
  }

}
