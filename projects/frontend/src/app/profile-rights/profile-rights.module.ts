import { NgModule } from '@angular/core';
// module
import { ProfileRightsRoutingModule } from './profile-rights-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { ProfileRightsComponent } from './profile-rights.component';

@NgModule({
  declarations: [
    ProfileRightsComponent,
  ],
  imports: [
    ShareModule,
    ProfileRightsRoutingModule,
  ]
})
export class ProfileRightsModule { }
