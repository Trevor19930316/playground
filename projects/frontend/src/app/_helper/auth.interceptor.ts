import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, finalize, mergeMap, retry, retryWhen, take, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { SnackbarService } from 'src/app/_service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  token: String | null | undefined;

  constructor(
    private snackbarService: SnackbarService,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let apiUrl = environment.apiUri;
    this.token = sessionStorage.getItem('token');

    const req = request.clone({
      ...request,
      setHeaders: {
        'Access-Control-Allow-Headers': 'Authorization, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X-Custom-header',
        'Access-Control-Expose-Headers': 'Authorization',
        Authorization: 'Bearer ' + this.token || '',
      },
      url: apiUrl + request.url,
    });

    return next.handle(req).pipe(
      tap(event => {

        if (event instanceof HttpResponse) {
          // console.log('auth event', event)
        }

      }),
      retryWhen((errors: Observable<any>) => errors.pipe(
        mergeMap((error, index) => {

          // console.log('retryWhen mergeMap')
          // console.log(index)
          // console.log(error)

          if (index == 0 && error.status == 401) {
            // this.authService.refreshToken().subscribe(res => {
            //   console.log('refreshToken');
            //   console.log(res);
            //   this.token = res.data.token;
            //   sessionStorage.setItem('token', res.data.token);
            // })
          }

          return throwError(error);
        }),
        take(2),
      )),
      retry(1),
      catchError((error: HttpErrorResponse) => {
        // this.snackbarService.danger(error.name + ' ' + error.status);
        return throwError(error);
      }),
      finalize(() => {
      })
    );
  }
}
