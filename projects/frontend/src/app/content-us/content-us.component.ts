import { Component, OnInit } from '@angular/core';

import { IWebsiteInfo } from 'src/app/_model';
import { WebsiteInfoService } from 'src/app/_service';

@Component({
  selector: 'app-content-us',
  templateUrl: './content-us.component.html',
  styleUrls: ['./content-us.component.scss']
})
export class ContentUsComponent implements OnInit {

  websiteInfo !: IWebsiteInfo;
  loading = true;

  constructor(
    private websiteInfoService: WebsiteInfoService,
  ) { }

  private _fetchWebsiteInfo() {
    this.loading = true;
    this.websiteInfoService.get().subscribe(res => {
      this.websiteInfo = res;
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchWebsiteInfo();
  }

}
