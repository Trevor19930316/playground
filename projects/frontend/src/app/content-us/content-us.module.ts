import { NgModule } from '@angular/core';

import { ContentUsRoutingModule } from './content-us-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
import { ContentUsComponent } from './content-us.component';

@NgModule({
  declarations: [
    ContentUsComponent,
  ],
  imports: [
    ContentUsRoutingModule,
    ShareModule,
  ]
})
export class ContentUsModule { }
