import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentUsComponent } from './content-us.component';

const routes: Routes = [
  {
    path: '',
    component: ContentUsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentUsRoutingModule { }
