import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { BulletinBoardsComponent } from './bulletin-boards.component';
import { BulletinBoardDetialComponent } from './bulletin-board-detial/bulletin-board-detial.component';

const routes: Routes = [
  {
    path: '',
    component: BulletinBoardsComponent,
  },
  {
    path: ':bulletinBoardId',
    component: BulletinBoardDetialComponent,
    data: {
      breadcrumb: '明細',
      isClickable: false,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class BulletinBoardsRoutingModule {
}
