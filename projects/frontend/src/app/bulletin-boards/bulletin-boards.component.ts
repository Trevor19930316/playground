import { Component, OnInit } from '@angular/core';

import { IBulletinBoard } from 'src/app/_model';
import { BulletinBoardService } from 'src/app/_service';

@Component({
  selector: 'app-bulletin-boards',
  templateUrl: './bulletin-boards.component.html',
  styleUrls: ['./bulletin-boards.component.scss']
})
export class BulletinBoardsComponent implements OnInit {

  bulletinBoards!: IBulletinBoard[];
  loading = true;

  constructor(
    private bulletinBoardService: BulletinBoardService,
  ) { }

  // 公佈欄清單
  private _fetchBulletinBoards(filter: any = false) {
    this.loading = true;
    this.bulletinBoardService.getAll().subscribe(res => {
      this.bulletinBoards = res;
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchBulletinBoards();
  }

}
