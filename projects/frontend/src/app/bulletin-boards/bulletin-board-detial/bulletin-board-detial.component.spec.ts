import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletinBoardDetialComponent } from './bulletin-board-detial.component';

describe('BulletinBoardDetialComponent', () => {
  let component: BulletinBoardDetialComponent;
  let fixture: ComponentFixture<BulletinBoardDetialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulletinBoardDetialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletinBoardDetialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
