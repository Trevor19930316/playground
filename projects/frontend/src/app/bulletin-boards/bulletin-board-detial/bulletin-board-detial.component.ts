import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { GlobalRoutes } from 'src/app/_model/_enum/routes.enum';
import { IBulletinBoard } from 'src/app/_model';
import { BulletinBoardService } from 'src/app/_service';

@Component({
  selector: 'app-bulletin-board-detial',
  templateUrl: './bulletin-board-detial.component.html',
  styleUrls: ['./bulletin-board-detial.component.scss']
})
export class BulletinBoardDetialComponent implements OnInit {

  bulletinBoard!: IBulletinBoard;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bulletinBoardService: BulletinBoardService,
  ) { }

  private _fetchBulletinBoard(id: number) {
    this.bulletinBoardService.get(id).subscribe(res => {
      if (res.length == 0) {
        this.router.navigate([GlobalRoutes.Bulletin_Boards]);
      } else {
        this.bulletinBoard = res;
      }
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(res => {
      this._fetchBulletinBoard(res.bulletinBoardId);
    });
  }

}
