import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletinBoardsComponent } from './bulletin-boards.component';

describe('BulletinBoardsComponent', () => {
  let component: BulletinBoardsComponent;
  let fixture: ComponentFixture<BulletinBoardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulletinBoardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletinBoardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
