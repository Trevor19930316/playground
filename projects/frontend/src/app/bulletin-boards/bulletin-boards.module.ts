import { NgModule } from '@angular/core';
// module
import { BulletinBoardsRoutingModule } from './bulletin-boards-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { BulletinBoardsComponent } from './bulletin-boards.component';
import { BulletinBoardDetialComponent } from './bulletin-board-detial/bulletin-board-detial.component';

@NgModule({
  declarations: [
    BulletinBoardsComponent,
    BulletinBoardDetialComponent,
  ],
  imports: [
    BulletinBoardsRoutingModule,
    ShareModule,
  ]
})
export class BulletinBoardsModule { }
