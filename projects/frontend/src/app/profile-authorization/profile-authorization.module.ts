import { NgModule } from '@angular/core';
// module
import { ProfileAuthorizationRoutingModule } from './profile-authorization-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { ProfileAuthorizationComponent } from './profile-authorization.component';

@NgModule({
  declarations: [
    ProfileAuthorizationComponent,
  ],
  imports: [
    ShareModule,
    ProfileAuthorizationRoutingModule,
  ]
})
export class ProfileAuthorizationModule { }
