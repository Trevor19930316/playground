import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/_service';

@Component({
  selector: 'app-profile-authorization',
  templateUrl: './profile-authorization.component.html',
  styleUrls: ['./profile-authorization.component.scss']
})
export class ProfileAuthorizationComponent implements OnInit {

  authorization!: any;
  loading = true;

  constructor(
    private userService: UserService,
  ) { }

  private _fetchAuthorization() {
    this.loading = true;
    this.userService.getAuthorization().subscribe(res => {
      this.authorization = res;
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchAuthorization();
  }

}
