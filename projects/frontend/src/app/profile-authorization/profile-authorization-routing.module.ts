import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { ProfileAuthorizationComponent } from './profile-authorization.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileAuthorizationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ProfileAuthorizationRoutingModule {
}
