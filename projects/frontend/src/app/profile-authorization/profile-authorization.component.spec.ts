import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAuthorizationComponent } from './profile-authorization.component';

describe('ProfileAuthorizationComponent', () => {
  let component: ProfileAuthorizationComponent;
  let fixture: ComponentFixture<ProfileAuthorizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileAuthorizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
