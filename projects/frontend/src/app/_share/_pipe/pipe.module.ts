import { NgModule } from "@angular/core";

import { AgePipe } from './age.pipe';
import { GenderPipe } from './gender.pipe';

@NgModule({
  declarations: [
    AgePipe,
    GenderPipe,
  ],
  imports: [],
  exports: [
    AgePipe,
    GenderPipe,
  ],
})
export class PipeModule {
}
