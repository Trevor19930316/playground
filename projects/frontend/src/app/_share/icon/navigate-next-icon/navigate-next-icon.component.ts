import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-navigate-next',
  templateUrl: './navigate-next-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class NavigateNextIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
