import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-delete',
  templateUrl: './delete-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class DeleteIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
