import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-expand-more',
  templateUrl: './expand-more-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class ExpandMoreIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
