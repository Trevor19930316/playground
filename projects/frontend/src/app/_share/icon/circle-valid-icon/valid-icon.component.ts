import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-valid-circle',
  templateUrl: './valid-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class CircleValidIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
