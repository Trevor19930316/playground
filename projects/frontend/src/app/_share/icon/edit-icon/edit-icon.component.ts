import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-edit',
  templateUrl: './edit-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class EditIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
