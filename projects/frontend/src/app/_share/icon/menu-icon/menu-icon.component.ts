import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-menu',
  templateUrl: './menu-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class MenuIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
