import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-key',
  templateUrl: './key-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class KeyIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
