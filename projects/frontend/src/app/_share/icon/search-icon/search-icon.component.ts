import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-search',
  templateUrl: './search-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class SearchIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
