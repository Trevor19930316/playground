import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-account-circle',
  templateUrl: './account-circle-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class AccountCircleIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
