import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-account',
  templateUrl: './account-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class AccountIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
