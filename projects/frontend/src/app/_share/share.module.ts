import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
// material
import { MateriaModule } from './materia/materia.module';
// icon
import { IconModule } from './icon/icon.module';
// pipe
import { PipeModule } from './_pipe/pipe.module';
// component
import { SpinnerComponent } from './spinner/spinner.component';
import { ComponentModule } from '../_component/component.module';

@NgModule({
  declarations: [
    SpinnerComponent,
  ],
  imports: [
    CommonModule,
    MateriaModule,
    IconModule,
    PipeModule,
    ComponentModule,
  ],
  exports: [
    MateriaModule,
    IconModule,
    PipeModule,
    // modules
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxQRCodeModule,
    // component
    SpinnerComponent,
    ComponentModule,
  ],
})
export class ShareModule {
}
