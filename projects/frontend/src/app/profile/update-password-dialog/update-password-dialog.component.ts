import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ApiService, UserService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-update-password-dialog',
  templateUrl: './update-password-dialog.component.html',
  styleUrls: ['./update-password-dialog.component.scss']
})
export class UpdatePasswordDialogComponent implements OnInit {

  form = this.fb.group({
    old_password: ['', Validators.compose([
      Validators.required,
    ])],
    new_password: ['', Validators.compose([
      Validators.required,
      Validators.minLength(5),
    ])],
    confirm_new_password: ['', Validators.compose([
      Validators.required,
    ])],
  }, { validator: this.passwordConfirm });

  loading = false;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  passwordConfirm(c: AbstractControl): { invalid: boolean } | void {
    if (c.get('new_password')?.value !== c.get('confirm_new_password')?.value) {
      c.get('confirm_new_password')?.setErrors({ 'message': '與新密碼不一致' });
      return { invalid: true };
    }
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.userService.updatePassword(this.form.value).subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
    });
  }

  ngOnInit(): void {
  }

}
