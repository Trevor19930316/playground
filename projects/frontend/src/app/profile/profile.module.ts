import { NgModule } from '@angular/core';
// module
import { ProfileRoutingModule } from './profile-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { ProfileComponent } from './profile.component';
import { UpdatePasswordDialogComponent } from './update-password-dialog/update-password-dialog.component';

@NgModule({
  declarations: [
    ProfileComponent,
    UpdatePasswordDialogComponent,
  ],
  imports: [
    ProfileRoutingModule,
    ShareModule,
  ]
})
export class ProfileModule { }
