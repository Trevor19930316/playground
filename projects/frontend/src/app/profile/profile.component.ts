import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { IUser } from 'src/app/_model';
import { ApiService, UserService, SnackbarService } from 'src/app/_service';
import { UpdatePasswordDialogComponent } from './update-password-dialog/update-password-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profile!: IUser;

  form = this.fb.group({
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(30)
    ])],
    phone: this.fb.group({
      phone_country: ['', Validators.compose([
        Validators.required,
      ])],
      phone_number: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern("(09)[0-9 ]{8}"),
      ])],
    }),
    email: ['', Validators.compose([
      Validators.required,
      Validators.email,
      Validators.maxLength(100)
    ])],
    address: this.fb.group({
      country_id: ['', Validators.compose([
        Validators.required,
      ])],
      city: ['', Validators.compose([
        Validators.required,
      ])],
      address_postal_id: ['', Validators.compose([
        Validators.required,
      ])],
      address: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])],
    }),
  });

  loading = false;
  errorMessage = [];

  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private apiService: ApiService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) {
  }

  get f() {
    return this.form.controls;
  }

  // 個人資料
  private _fetchProfile() {
    this.userService.fetchProfile().subscribe(res => {
      this.profile = res;
      this._formSetValue(res);
    });
  }

  private _formSetValue(data: any) {
    this.form.setValue({
      name: data.name,
      phone: {
        phone_country: data.phone_country,
        phone_number: data.phone_number,
      },
      email: data.email,
      address: {
        country_id: data.addressData.country_id,
        city: data.addressData.city,
        address_postal_id: data.address_postal_id,
        address: data.address,
      },
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    // 編輯個人資料
    this.userService.updateProfile(this.form.value).subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this._fetchProfile();
        this.loading = false;
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          if (key.startsWith('address')) {
            this.form.get(['address', key])?.setErrors({ 'message': value.join(', ') });
          } else if (key.startsWith('phone')) {
            this.form.get(['phone', key])?.setErrors({ 'message': value.join(', ') });
          } else {
            this.form.get(key)?.setErrors({ 'message': value.join(', ') });
          }
        });
      }
    });
  }

  cancel() {
    this._fetchProfile();
  }

  // 變更密碼
  clickUpdatePassword() {
    this.dialog.open(UpdatePasswordDialogComponent, {
      autoFocus: false,
      // minWidth: '60%',
      maxWidth: '95%',
      data: {},
    });
  }

  ngOnInit(): void {
    this._fetchProfile();
  }

}
