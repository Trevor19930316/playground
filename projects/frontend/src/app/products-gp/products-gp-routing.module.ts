import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { ProductsGpComponent } from './products-gp.component';

const routes: Routes = [
  {
    path: '',
    component: ProductsGpComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsGpRoutingModule { }
