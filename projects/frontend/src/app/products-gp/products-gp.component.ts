import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { ProductService } from 'src/app/_service';

@Component({
  selector: 'app-products-gp',
  templateUrl: './products-gp.component.html',
  styleUrls: ['./products-gp.component.scss']
})
export class ProductsGpComponent implements OnInit, AfterViewInit {

  displayedColumns = ['no', 'name', 'gp', 'updated_at'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'product_no', Name: '商品編號' },
    { Key: 'product_name', Name: '商品名稱' },
    { Key: 'product_item_no', Name: '規格編號' },
    { Key: 'product_item_name', Name: '規格名稱' },
    { Key: 'gp', Name: 'GP' },
  ];
  filterDatetimeRanges = [
    // {
    //   Column: 'created_at', Name: '新增時間',
    // },
    {
      Column: 'product_item_updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
  ];

  constructor(
    private filtersService: FiltersService,
    private productService: ProductService,
  ) { }

  // 商品GP清單
  private _fetchProductsGp(filter: any = false) {
    this.tableIsLoading = true;
    this.productService.getAllItemGp().subscribe(res => {

      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchProductsGp(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  ngOnInit(): void {
    this._fetchProductsGp();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
