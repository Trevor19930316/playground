import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private apiService: ApiService,
  ) { }

  // GP
  getAllItemGp() {
    return this.apiService.get(`/product_item_gp`).pipe(
      map(p => {
        let res: any[] = [];
        _(p).forEach(function (product) {
          _(product.product_items).forEach(function (product_item) {
            const valid_flag = !product.valid_flag ? product.valid_flag : product_item.valid_flag;
            res.push({
              product_id: product.id,
              product_no: product.no,
              product_name: product.name,
              product_item_id: product_item.id,
              product_item_no: product_item.no,
              product_item_name: product_item.name,
              product_item_updated_at: product_item.updated_at,
              gp: product_item.gp,
              valid_flag: valid_flag,
            });
          })
        });
        return res;
      }),
    );
  }
}
