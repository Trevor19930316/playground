import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';
import * as _ from 'lodash';

import { IAddress, ICountry } from 'src/app/_model';
import { ApiService, DefinitionService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // 授權書 id
  authorizationId = 1;
  // 會員權益 id
  rightsId = 2;

  countries!: ICountry[];
  address!: IAddress[];

  constructor(
    private apiService: ApiService,
    private definitionService: DefinitionService,
  ) {
    this.definitionService.getCountries().subscribe(res => {
      this.countries = res;
    });

    this.definitionService.getAddress().subscribe(res => {
      this.address = res;
    });
  }
  /**
   * API - 取得個人資訊
   */
  fetchProfile() {
    return this.apiService.get(`/auth/user_profile`).pipe(
      map(data => {
        const result = _.find(this.address, { 'id': data.address_postal_id });
        return ({
          ...data,
          addressData: {
            country_id: !!result ? result.country_id : null,
            country_name: !!result ? result.country_name : null,
            city: !!result ? result.city : null,
            district: !!result ? result.district : null,
            zip_code: !!result ? result.zip_code : null,
          },
        });
      }),
    );
  }
  /**
   * API - 編輯個人資訊
   *
   * @param data
   */
  updateProfile(data: any) {
    return this.apiService.post(`/auth/user_profile`, {
      name: data.name,
      phone_country: data.phone.phone_country,
      phone_number: data.phone.phone_number,
      email: data.email,
      address_postal_id: data.address.address_postal_id,
      address: data.address.address,
    });
  }
  /**
   * API - 變更密碼
   *
   * @param data
   */
  updatePassword(data: any) {
    return this.apiService.post(`/auth/change_password`, {
      old_password: Md5.hashStr(data.old_password),
      new_password: Md5.hashStr(data.new_password),
    });
  }
  /**
   * API - 取得授權書資料
   */
  getAuthorization() {
    return this.apiService.get(`/system_extra_data/${this.authorizationId}}`);
  }
  /**
   * API - 取得會員權益資料
   */
  getRights() {
    return this.apiService.get(`/system_extra_data/${this.rightsId}}`);
  }
}
