import { Injectable } from '@angular/core';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class WebsiteInfoService {

  id = 1;

  constructor(
    private apiService: ApiService,
  ) { }

  get() {
    return this.apiService.get(`/website_info/${this.id}`);
  }
}
