import { Injectable } from '@angular/core';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class BulletinBoardService {

  constructor(
    private apiService: ApiService,
  ) { }

  // 公佈欄
  getAll() {
    return this.apiService.get(`/bulletin_boards`);
  }

  get(id: number) {
    return this.apiService.get(`/bulletin_boards/${id}`);
  }
}
