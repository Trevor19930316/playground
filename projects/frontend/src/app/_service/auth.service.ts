import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  // 登入
  login(account: string, password: string) {
    return this.http.post<any>('/auth/login', {
      account: account,
      password: Md5.hashStr(password),
    }).pipe(
      catchError(p => {
        // this.logout();
        return of(true);
      }),
    );
  }

  // 登出
  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  // 判斷是否登入
  hasLogin() {
    return !!this.getToken();
  }

  // refresh token
  refreshToken() {
    return this.http.post<any>('/auth/refresh', {}).pipe(
      catchError(p => {
        // this.logout();
        return of(true);
      }),
    );
  }

  setToken(token: string) {
    sessionStorage.setItem('token', token);
  }

  getToken() {
    return sessionStorage.getItem('token');
  }
}
