import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DefinitionService {

  constructor(
    private apiService: ApiService,
  ) { }

  // 取得國家
  getCountries() {
    return this.apiService.get(`/definition/getCountries`);
  }

  // 取得國家地區
  getAddress() {
    return this.apiService.get(`/definition/getAddress`);
  }

  // 取得國碼
  getCountriesCode() {
    return this.apiService.get(`/definition/getCountriesCode`);
  }
}
