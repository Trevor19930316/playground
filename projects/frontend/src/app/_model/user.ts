export interface IUser {
  id: number;
  // 會員等級id
  user_level_id: number;
  user_level: {
    id: number;
    // 名稱
    name: string;
    // 代號
    no: string;
    // 可抽成階數
    bonus_level: number;
    // 排序
    sort: number;
    // 有效狀態
    valid_flag: boolean;
    created_at: string | null;
    updated_at: string | null;
  };
  // 帳號
  account: string;
  // 名稱
  name: string;
  // 電子信箱
  email: string;
  // 電子信箱認證時間 timestamp
  email_verified_at: string | null;
  // 國碼
  phone_country: string;
  // 電話
  phone_number: string;
  // 地址
  address: string;
  // 地址郵遞區號id
  address_postal_id: number;
  // 地址資訊
  addressData: any;
  // 有效狀態
  valid_flag: boolean;
  created_at: string | null;
  updated_at: string | null;
}
