export interface IWebsiteInfo {
  id: number;
  name: string;
  phone: string;
  title: string;
  title_logo_url: string;
  website_logo_url: string;
  email: string;
  company_address: string;
  line: string;
  facebook: string;
  instagram: string;
  created_at: string;
  updated_at: string;
}
