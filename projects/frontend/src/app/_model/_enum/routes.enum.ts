export enum GlobalRoutes {
  // 登入
  Login = 'login',
  // 首頁
  Home = '',
  // 公佈欄
  Bulletin_Boards = 'bulletin_boards',
  Bulletin_Boards_Detail = 'bulletin_boards/:bulletinBoardId',
  // GP 查詢
  Products_Gp = 'products_gp',
  // 聯絡我們
  Content_Us = 'content_us',
  // 授權書
  Authorization = 'authorization',
  // 會員權益
  Rights = 'rights',
  // 個人資料
  Profile = 'profile',
  // 我的銷售
  Order_Sales = 'order_sales',
  // 對帳單
  Billing = 'billing',
  Billing_Detail = 'billing/:billingId',
  // 獎金試算
  Bonuses = 'bonuses',
}
