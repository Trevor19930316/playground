import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// model
import {
  GlobalRoutes,
} from '../_model/_enum/routes.enum';
// component
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: GlobalRoutes.Bulletin_Boards,
        loadChildren: () => import('../bulletin-boards/bulletin-boards.module').then((m) => m.BulletinBoardsModule),
        data: {
          breadcrumb: '公佈欄',
          isClickable: true,
        },
      },
      {
        path: GlobalRoutes.Products_Gp,
        loadChildren: () => import('../products-gp/products-gp.module').then((m) => m.ProductsGpModule),
        data: {
          breadcrumb: '商品 GP 查詢',
          isClickable: true,
        },
      },
      {
        path: GlobalRoutes.Content_Us,
        loadChildren: () => import('../content-us/content-us.module').then((m) => m.ContentUsModule),
        data: {
          breadcrumb: '聯絡我們',
          isClickable: true,
        },
      },
      {
        path: GlobalRoutes.Profile,
        loadChildren: () => import('../profile/profile.module').then((m) => m.ProfileModule),
        data: {
          breadcrumb: '個人資料',
          isClickable: true,
        },
      },
      {
        path: GlobalRoutes.Authorization,
        loadChildren: () => import('../profile-authorization/profile-authorization.module').then((m) => m.ProfileAuthorizationModule),
        data: {
          breadcrumb: '授權書',
          isClickable: true,
        },
      },
      {
        path: GlobalRoutes.Rights,
        loadChildren: () => import('../profile-rights/profile-rights.module').then((m) => m.ProfileRightsModule),
        data: {
          breadcrumb: '會員權益',
          isClickable: true,
        },
      },
    ],
  },
  // otherwise redirect
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class HomeRoutingModule {
}
