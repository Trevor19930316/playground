import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { IUser } from 'src/app/_model';
import { AuthService, UserService } from 'src/app/_service';
import { GlobalRoutes } from '../_model/_enum/routes.enum';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  profileData!: IUser;
  collapsed = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
  ) { }

  get globalRoute() {
    return GlobalRoutes;
  }

  toggle() {
    this.collapsed = this.collapsed ? false : true;
  }

  // 個人資料
  private _fetchProfile() {
    this.userService.fetchProfile().subscribe(res => {
      this.profileData = res;
    });
  }

  clickHome() {
    this.router.navigate(['/']);
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit(): void {
    this._fetchProfile();
  }
}
