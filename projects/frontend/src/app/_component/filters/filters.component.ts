import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { FormControl, FormGroup } from '@angular/forms';

interface IKeywordsColumn {
  Key: string;
  Name: string;
}
interface IFilterDatetimeRange {
  Column: string | any;
  Name: string;
}
interface IFilterSelect {
  Column: string;
  Name: string;
  Options: IKeywordsColumn[];
}

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  keywordsColumn: string = '';
  keywords: string = '';
  filterDatetimeRangeMulti: any = {};
  filterSelectMulti: any = {};
  expandType = false;
  expandClass = 'hide';

  @Input() dataSource: any;
  @Input() keywordsColumns: IKeywordsColumn[] = [];
  @Input() filterSelects: IFilterSelect[] = [];
  filterDatetimeRanges: IFilterDatetimeRange[] = [];
  @Input('filterDatetimeRanges') set filterDatetimeRangesSetting(val: IFilterDatetimeRange[]) {
    this.filterDatetimeRanges = val;
    val.forEach(res => {
      this.filterDatetimeRangeMulti[res.Column] = {
        start: null,
        end: null,
      };
    })
  }

  @Output() searchEvent = new EventEmitter<any>();

  constructor() {
  }

  get filterMore() {
    return this.filterSelects.length > 0 || this.filterDatetimeRanges.length > 0;
  }

  modelDatatimeRangeMulti(column: string, type: string, event: any) {
    this.filterDatetimeRangeMulti[column][type] = event.value;
  }

  expand(type: boolean) {
    this.expandType = type;
    this.expandClass = type ? '' : 'hide';
  }

  onSubmit() {
    this.searchEvent.emit({
      keywordsColumn: this.keywordsColumn,
      keywords: this.keywords,
      filterSelectMulti: this.filterSelectMulti,
      filterDatetimeRangeMulti: this.filterDatetimeRangeMulti,
    });
  }

  // setFilterSelectsObject() {
  //   this.filterSelects.filter((o) => {
  //     o.Options = this.getFilterSelectsObject(this.dataSource, o.Column);
  //   });
  // }

  // getFilterSelectsObject(fullObj: any, key: any) {
  //   const uniqChk: any = [];
  //   fullObj.filter((obj: any) => {
  //     if (!uniqChk.includes(obj[key])) {
  //       uniqChk.push(obj[key]);
  //     }
  //     return obj;
  //   });
  //   return uniqChk;
  // }

  ngOnInit(): void {

  }
}
