import { Component, OnInit } from '@angular/core';
import { FormGroup, ControlContainer, FormGroupDirective } from '@angular/forms';

import { DefinitionService } from 'src/app/_service';
import { ICountryCode } from 'src/app/_model';

@Component({
  selector: 'app-country-code',
  templateUrl: './country-code.component.html',
  styleUrls: ['./country-code.component.scss'],
  providers: [
  ]
})
export class CountryCodeComponent implements OnInit {

  countries_code!: ICountryCode[];

  phoneFormGroup!: FormGroup;

  constructor(
    private definitionService: DefinitionService,
    private controlContainer: ControlContainer,
    // private fg: FormGroupDirective,
  ) {
    this.definitionService.getCountriesCode().subscribe(res => {
      this.countries_code = res;
    })
  }

  get f() {
    return this.phoneFormGroup.controls;
  }

  get value() {
    return this.phoneFormGroup.value;
  }

  ngOnInit(): void {
    this.phoneFormGroup = <FormGroup>this.controlContainer.control;
  }
}
