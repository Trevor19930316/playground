import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

import { IconModule } from '../_share/icon/icon.module';
import { MateriaModule } from '../_share/materia/materia.module';
import { AdiButtonComponent } from './button/button.component';
import { AdiButtonLoaderIconDirective } from './button/button.directive';
import { CountryAddressComponent } from './country-address/country-address.component';
import { CountryCodeComponent } from './country-code/country-code.component';
import { FiltersComponent } from './filters/filters.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { RowComponent } from './row/row.component';
import { TableBlockComponent } from './table-block/table-block.component';

@NgModule({
  declarations: [
    BreadcrumbComponent,
    AdiButtonComponent,
    AdiButtonLoaderIconDirective,
    CountryAddressComponent,
    CountryCodeComponent,
    FiltersComponent,
    ProgressBarComponent,
    RowComponent,
    TableBlockComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    IconModule,
    MateriaModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    BreadcrumbComponent,
    AdiButtonComponent,
    AdiButtonLoaderIconDirective,
    CountryAddressComponent,
    CountryCodeComponent,
    FiltersComponent,
    ProgressBarComponent,
    RowComponent,
    TableBlockComponent,
  ]
})
export class ComponentModule { }
