import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ApiService, UserService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-user-rights',
  templateUrl: './user-rights.component.html',
  styleUrls: ['./user-rights.component.scss']
})
export class UserRightsComponent implements OnInit {

  rights!: { title: string; content: string; updated_at: string; };

  form = this.fb.group({
    title: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(255)
    ])],
    content: ['', Validators.compose([
      Validators.required,
    ])],
  });

  isEditing = false;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchRights() {
    this.loading = true;
    this.userService.getRights().subscribe(res => {
      this.rights = res;
      this._formSetValue(res);
      this.loading = false;
    });
  }

  private _formSetValue(data: any) {
    this.form.setValue({
      title: data.title,
      content: data.content,
    });
  }

  handleUpdate(isEditing: boolean) {
    this.isEditing = isEditing;
    if (!isEditing) {
      this._fetchRights();
    }
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.userService.updateRights(this.form.value).subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this._fetchRights();
        this.isEditing = false;
      } else {
        // 失敗
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchRights();
  }

}
