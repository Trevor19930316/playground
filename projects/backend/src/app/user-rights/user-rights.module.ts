import { NgModule } from '@angular/core';

import { ShareModule } from 'src/app/_share/share.module';
import { UserRightsRoutingModule } from './user-rights-routing.module';
import { UserRightsComponent } from './user-rights.component';

@NgModule({
  declarations: [
    UserRightsComponent
  ],
  imports: [
    UserRightsRoutingModule,
    ShareModule
  ]
})
export class UserRightsModule { }
