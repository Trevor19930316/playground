import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserAuthorizationComponent } from './user-authorization.component';

const routes: Routes = [
  {
    path: '',
    component: UserAuthorizationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class UserAuthorizationRoutingModule { }
