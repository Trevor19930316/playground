import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ApiService, UserService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-user-authorization',
  templateUrl: './user-authorization.component.html',
  styleUrls: ['./user-authorization.component.scss']
})
export class UserAuthorizationComponent implements OnInit {

  authorization!: { title: string; content: string; updated_at: string; };

  form = this.fb.group({
    title: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(255)
    ])],
    content: ['', Validators.compose([
      Validators.required,
    ])],
  });

  isEditing = false;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchAuthorization() {
    this.loading = true;
    this.userService.getAuthorization().subscribe(res => {
      this.authorization = res;
      this._formSetValue(res);
      this.loading = false;
    });
  }

  private _formSetValue(data: any) {
    this.form.setValue({
      title: data.title,
      content: data.content,
    });
  }

  handleUpdate(isEditing: boolean) {
    this.isEditing = isEditing;
    if (!isEditing) {
      this._fetchAuthorization();
    }
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.userService.updateAuthorization(this.form.value).subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this._fetchAuthorization();
        this.isEditing = false;
      } else {
        // 失敗
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchAuthorization();
  }

}
