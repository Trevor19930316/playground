import { NgModule } from '@angular/core';

import { ShareModule } from 'src/app/_share/share.module';
import { UserAuthorizationRoutingModule } from './user-authorization-routing.module';
import { UserAuthorizationComponent } from './user-authorization.component';

@NgModule({
  declarations: [
    UserAuthorizationComponent
  ],
  imports: [
    UserAuthorizationRoutingModule,
    ShareModule,
  ]
})
export class UserAuthorizationModule { }
