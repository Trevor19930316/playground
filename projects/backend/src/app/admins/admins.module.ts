import { NgModule } from '@angular/core';
// module
import { AdminsRoutingModule } from './admins-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { AdminsComponent } from './admins.component';
import { AdminDialogComponent } from './admin-dialog/admin-dialog.component';

@NgModule({
  declarations: [
    AdminsComponent,
    AdminDialogComponent,
  ],
  imports: [
    AdminsRoutingModule,
    ShareModule,
  ]
})
export class AdminsModule {
}
