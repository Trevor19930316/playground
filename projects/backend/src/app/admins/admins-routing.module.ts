import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// model
import { AdminRoutes, UserRoutes, FrontendRoutes } from '../_model/_enum/routes.enum';
// component
import { AdminsComponent } from './admins.component';

const routes: Routes = [
  {
    path: '',
    component: AdminsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AdminsRoutingModule {
}
