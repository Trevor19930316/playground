import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IAdmin } from 'src/app/_model';
import { ApiService, AdminService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-admin-dialog',
  templateUrl: './admin-dialog.component.html',
  styleUrls: ['./admin-dialog.component.scss']
})
export class AdminDialogComponent implements OnInit {

  is_super: boolean = false;
  admin?: IAdmin;

  form = this.fb.group({
    account: ['', Validators.compose([
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30)
    ])],
    password: [''],
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(30)
    ])],
    valid_flag: ['1', Validators.required],
    is_super: ['0',]
  });

  loading = false;
  errorMessage = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private adminService: AdminService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchAdmin(id: number) {
    this.adminService.get(id).subscribe(res => {
      this.admin = res;

      this.form.setValue({
        account: res.account,
        password: '',
        name: res.name,
        valid_flag: (res.valid_flag).toString(),
        is_super: (res.is_super).toString(),
      });
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const apiFunc = this.data.type == 'add' ?
      this.adminService.add(this.form.value) : this.adminService.update(this.data.id, this.form.value, this.is_super);
    apiFunc.subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
    });
  }

  ngOnInit(): void {
    this.adminService.isSuperAdmin().then(res => {
      this.is_super = res;
    });

    if (!!this.data.id) {
      this._fetchAdmin(this.data.id);
    }

    if (this.data.type == 'add') {
      this.form.get('password')?.setValidators([Validators.required, Validators.minLength(5)]);
      this.form.get('password')?.updateValueAndValidity();
      // this.form.get('password')?.setValidators(null);
    }
  }
}
