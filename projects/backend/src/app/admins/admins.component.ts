import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IAdmin } from 'src/app/_model';
import { AdminService, SnackbarService } from 'src/app/_service';
import { AdminDialogComponent } from './admin-dialog/admin-dialog.component';
import { DialogDeleteConfirmComponent } from 'src/app/_component/dialog-delete-confirm/dialog-delete-confirm.component';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['account', 'name', 'is_super', 'valid_flag', 'updated_at', 'maintain'];
  dataSource = new MatTableDataSource<IAdmin>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'account', Name: '帳號' },
    { Key: 'name', Name: '名稱' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'is_super', Name: '最高權限', Options: [
        { Key: '0', Name: '是' },
        { Key: '1', Name: '否' },
      ],
    },
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    }
  ];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private adminService: AdminService,
    private snackbarService: SnackbarService,
  ) { }

  private _fetchAdmins(filter: any = false) {
    this.tableIsLoading = true;

    this.adminService.getAll().subscribe(res => {

      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchAdmins(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  // 明細
  clickDetail(data: IAdmin) {
    this._openDialog('detail', data.id);
  }

  // 新增
  clickAdd() {
    this._openDialog('add');
  }

  // 編輯
  clickUpdate(data: IAdmin) {
    this._openDialog('update', data.id);
  }

  // 刪除
  clickDelete(data: IAdmin) {
    const dialogDelete = this.dialog.open(DialogDeleteConfirmComponent, {
      autoFocus: false,
      data: {
        name: data.account + ' ' + data.name,
      },
    });
    dialogDelete.afterClosed().subscribe(result => {
      if (result) {
        this.adminService.destroy(data.id).subscribe(res => {
          this.snackbarService.apiResopnse(res);
          this._fetchAdmins();
        });
      }
    });
  }

  private _openDialog(type: string, id?: number) {
    const dialogRef = this.dialog.open(AdminDialogComponent, {
      autoFocus: false,
      minWidth: '80%',
      maxWidth: '95%',
      data: { type: type, id: id },
    });

    dialogRef.afterClosed().pipe<boolean>(
      filter(p => !!p),
    ).subscribe(() => {
      this._fetchAdmins();
    });
  }

  ngOnInit() {
    this._fetchAdmins();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
