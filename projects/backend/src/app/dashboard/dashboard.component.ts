import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit() {
    // 先導去會員資料
    this.router.navigate(['/users']);
  }

  ngOnDestroy() {
  }
}
