import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-invalid-circle',
  templateUrl: './invalid-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class CircleInvalidIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
