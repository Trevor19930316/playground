import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-import',
  templateUrl: './import-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class ImportIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
