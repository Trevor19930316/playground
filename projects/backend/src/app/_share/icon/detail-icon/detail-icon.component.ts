import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-detail',
  templateUrl: './detail-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class DetailIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
