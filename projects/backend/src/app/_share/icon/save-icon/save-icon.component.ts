import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'icon-save',
  templateUrl: './save-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class SaveIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
