import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-add',
  templateUrl: './add-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class AddIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
