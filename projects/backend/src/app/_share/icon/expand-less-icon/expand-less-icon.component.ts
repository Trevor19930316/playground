import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'icon-expand-less',
  templateUrl: './expand-less-icon.svg',
  styleUrls: ['../icon.component.scss']
})
export class ExpandLessIconComponent {

  @Input('class') customClassName = '';
  @HostBinding('class')

  get hostClasses() {
    return [
      'icon',
      this.customClassName,
    ].join(' ');
  }
}
