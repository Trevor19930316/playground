import { NgModule } from "@angular/core";

import { AccountCircleIconComponent } from './account-circle-icon/account-circle-icon.component';
import { AddIconComponent } from './add-icon/add-icon.component';
import { DeleteIconComponent } from './delete-icon/delete-icon.component';
import { DetailIconComponent } from './detail-icon/detail-icon.component';
import { EditIconComponent } from './edit-icon/edit-icon.component';
import { ExpandLessIconComponent } from './expand-less-icon/expand-less-icon.component';
import { ExpandMoreIconComponent } from './expand-more-icon/expand-more-icon.component';
import { ImportIconComponent } from './import-icon/import-icon.component';
import { MenuIconComponent } from './menu-icon/menu-icon.component';
import { NavigateNextIconComponent } from './navigate-next-icon/navigate-next-icon.component';
import { SaveIconComponent } from './save-icon/save-icon.component';
import { SearchIconComponent } from './search-icon/search-icon.component';
import { CircleInvalidIconComponent } from './circle-invalid-icon/invalid-icon.component';
import { CircleValidIconComponent } from './circle-valid-icon/valid-icon.component';

@NgModule({
  declarations: [
    AccountCircleIconComponent,
    AddIconComponent,
    DeleteIconComponent,
    DetailIconComponent,
    EditIconComponent,
    ExpandLessIconComponent,
    ExpandMoreIconComponent,
    ImportIconComponent,
    MenuIconComponent,
    NavigateNextIconComponent,
    SaveIconComponent,
    SearchIconComponent,
    CircleInvalidIconComponent,
    CircleValidIconComponent,
  ],
  imports: [],
  exports: [
    AccountCircleIconComponent,
    AddIconComponent,
    DeleteIconComponent,
    DetailIconComponent,
    EditIconComponent,
    ExpandLessIconComponent,
    ExpandMoreIconComponent,
    ImportIconComponent,
    MenuIconComponent,
    NavigateNextIconComponent,
    SaveIconComponent,
    SearchIconComponent,
    CircleInvalidIconComponent,
    CircleValidIconComponent,
  ],
})
export class IconModule {
}
