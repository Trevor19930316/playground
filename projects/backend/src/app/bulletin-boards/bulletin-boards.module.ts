import { NgModule } from '@angular/core';
// module
import { BulletinBoardsRoutingModule } from './bulletin-boards-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { BulletinBoardsComponent } from './bulletin-boards.component';
import { BulletinBoardDialogComponent } from './bulletin-board-dialog/bulletin-board-dialog.component';

@NgModule({
  declarations: [
    BulletinBoardsComponent,
    BulletinBoardDialogComponent,
  ],
  imports: [
    BulletinBoardsRoutingModule,
    ShareModule,
  ]
})
export class BulletinBoardsModule { }
