import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';

import { IBulletinBoard } from 'src/app/_model';
import { ApiService, BulletinBoardService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-bulletin-board-dialog',
  templateUrl: './bulletin-board-dialog.component.html',
  styleUrls: ['./bulletin-board-dialog.component.scss']
})
export class BulletinBoardDialogComponent implements OnInit {

  bulletinBoard?: IBulletinBoard;

  form = this.fb.group({
    title: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(100)
    ])],
    subtitle: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(100)
    ])],
    content: ['', Validators.compose([
      Validators.required,
    ])],
    date_settings: [true, Validators.compose([
      Validators.required,
    ])],
    start_date: [{ value: moment().hours(0).minutes(0).seconds(0), disabled: true },],
    end_date: [{ value: moment().hours(0).minutes(0).seconds(0), disabled: true },],
    sort: ['0', Validators.compose([
      Validators.required,
      Validators.min(0),
      Validators.pattern("^[0-9]+$"),
    ])],
    valid_flag: ['1', Validators.required],
  });

  loading = false;
  errorMessage = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private bulletinBoardService: BulletinBoardService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchBulletinBoard(id: number) {
    this.bulletinBoardService.get(id).subscribe(res => {
      this.bulletinBoard = res;

      let date_settings = false;
      if (res.start_date == null && res.end_date == null) {
        date_settings = true;
      } else {
        date_settings = false;
        this.form.controls.start_date.enable();
        this.form.controls.end_date.enable();
        this.form.get('start_date')?.setValidators([Validators.required]);
        this.form.get('end_date')?.setValidators([Validators.required]);
        this.form.get('start_date')?.updateValueAndValidity();
        this.form.get('end_date')?.updateValueAndValidity();
      }

      this.form.setValue({
        title: res.title,
        subtitle: res.subtitle,
        content: res.content,
        date_settings: date_settings,
        start_date: res.start_date,
        end_date: res.end_date,
        sort: res.sort,
        valid_flag: (res.valid_flag).toString(),
      });
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const apiFunc = this.data.type == 'add' ?
      this.bulletinBoardService.add(this.form.value) : this.bulletinBoardService.update(this.data.id, this.form.value);
    apiFunc.subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
    });
  }

  ngOnInit(): void {
    if (!!this.data.id) {
      this._fetchBulletinBoard(this.data.id);
    }

    this.form.get('date_settings')?.valueChanges.subscribe(value => {

      if (value) {
        this.form.controls.start_date.disable();
        this.form.controls.end_date.disable();
        this.form.get('start_date')?.clearValidators();
        this.form.get('end_date')?.clearValidators();
      } else {
        this.form.controls.start_date.enable();
        this.form.controls.end_date.enable();
        this.form.get('start_date')?.setValidators([Validators.required]);
        this.form.get('end_date')?.setValidators([Validators.required]);
      }
      this.form.get('start_date')?.updateValueAndValidity();
      this.form.get('end_date')?.updateValueAndValidity();

    });
  }

}
