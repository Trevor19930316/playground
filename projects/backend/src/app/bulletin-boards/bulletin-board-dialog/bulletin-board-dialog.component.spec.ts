import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletinBoardDialogComponent } from './bulletin-board-dialog.component';

describe('BulletinBoardDialogComponent', () => {
  let component: BulletinBoardDialogComponent;
  let fixture: ComponentFixture<BulletinBoardDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulletinBoardDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletinBoardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
