import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { BulletinBoardsComponent } from './bulletin-boards.component';

const routes: Routes = [
  {
    path: '',
    component: BulletinBoardsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class BulletinBoardsRoutingModule {
}
