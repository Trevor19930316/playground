import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IBulletinBoard } from 'src/app/_model';
import { BulletinBoardService, SnackbarService } from 'src/app/_service';
import { DialogDeleteConfirmComponent } from 'src/app/_component/dialog-delete-confirm/dialog-delete-confirm.component';
import { BulletinBoardDialogComponent } from './bulletin-board-dialog/bulletin-board-dialog.component';

@Component({
  selector: 'app-bulletin-boards',
  templateUrl: './bulletin-boards.component.html',
  styleUrls: ['./bulletin-boards.component.scss']
})
export class BulletinBoardsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['sort', 'image', 'title', 'intervalDate', 'valid_flag', 'updated_at', 'maintain'];
  dataSource = new MatTableDataSource<IBulletinBoard>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'title', Name: '標題' },
    // { Key: 'subtitle', Name: '副標題' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    },
  ];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private bulletinBoardService: BulletinBoardService,
    private snackbarService: SnackbarService,
  ) { }

  // 公佈欄清單
  private _fetchBulletinBoards(filter: any = false) {
    this.tableIsLoading = true;
    this.bulletinBoardService.getAll().subscribe(res => {
      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchBulletinBoards(event);
  }

  // 明細
  clickDetail(data: IBulletinBoard) {
    this._openDialog('detail', data.id);
  }

  // 新增
  clickAdd() {
    this._openDialog('add');
  }

  // 編輯
  clickUpdate(data: IBulletinBoard) {
    this._openDialog('update', data.id);
  }

  // 刪除
  clickDelete(data: IBulletinBoard) {
    const dialogDelete = this.dialog.open(DialogDeleteConfirmComponent, {
      autoFocus: false,
      data: {
        name: data.title,
      },
    });
    dialogDelete.afterClosed().subscribe(result => {
      if (result) {
        this.bulletinBoardService.destroy(data.id).subscribe(res => {
          this.snackbarService.apiResopnse(res);
          this._fetchBulletinBoards();
        });
      }
    });
  }

  private _openDialog(type: string, id?: number) {
    const dialogRef = this.dialog.open(BulletinBoardDialogComponent, {
      autoFocus: false,
      minWidth: '80%',
      maxWidth: '95%',
      data: { type: type, id: id },
    });

    dialogRef.afterClosed().pipe<boolean>(
      filter(p => !!p),
    ).subscribe(() => {
      this._fetchBulletinBoards();
    });
  }

  ngOnInit(): void {
    this._fetchBulletinBoards();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
