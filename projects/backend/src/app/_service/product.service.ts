import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private apiService: ApiService,
  ) { }

  // 商品
  getAll() {
    return this.apiService.get(`/admin/products`);
  }

  get(id: number) {
    return this.apiService.get(`/admin/products/${id}`);
  }

  add(data: any) {
    return this.apiService.post(`/admin/products`, {
      no: data.no,
      name: data.name,
      valid_flag: Number(data.valid_flag),
    });
  }

  update(id: number, data: any) {
    return this.apiService.put(`/admin/products/${id}`, {
      no: data.no,
      name: data.name,
      valid_flag: Number(data.valid_flag),
    });
  }

  destroy(id: number) {
    return this.apiService.delete(`/admin/products/${id}`);
  }

  // 規格
  getAllItem(product_id: number) {
    return this.apiService.get(`/admin/products/${product_id}/product_items`);
  }

  getItem(id: number) {
    return this.apiService.get(`/admin/product_items/${id}`);
  }

  addItem(product_id: number, data: any) {
    return this.apiService.post(`/admin/products/${product_id}/product_items`, {
      no: data.no,
      name: data.name,
      price: data.price,
      gp: data.gp,
      valid_flag: Number(data.valid_flag),
    });
  }

  updateItem(id: number, data: any) {
    return this.apiService.put(`/admin/product_items/${id}`, {
      no: data.no,
      name: data.name,
      price: data.price,
      gp: data.gp,
      valid_flag: Number(data.valid_flag),
    });
  }

  destroyItem(id: number) {
    return this.apiService.delete(`/admin/product_items/${id}`);
  }

  // GP
  getAllItemGp() {
    return this.apiService.get(`/admin/product_item_gp`).pipe(
      map(p => {
        let res: any[] = [];
        _(p).forEach(function (product) {
          _(product.product_items).forEach(function (product_item) {
            const valid_flag = !product.valid_flag ? product.valid_flag : product_item.valid_flag;
            res.push({
              product_id: product.id,
              product_no: product.no,
              product_name: product.name,
              product_item_id: product_item.id,
              product_item_no: product_item.no,
              product_item_name: product_item.name,
              product_item_updated_at: product_item.updated_at,
              gp: product_item.gp,
              valid_flag: valid_flag,
            });
          })
        });
        return res;
      }),
    );
  }

  updateItemGp(id: number, gp: any) {
    return this.apiService.put(`/admin/product_item_gp/${id}`, {
      gp: gp,
    });
  }
}
