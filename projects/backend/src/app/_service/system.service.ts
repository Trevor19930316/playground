import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SystemService {

  constructor(
    private apiService: ApiService,
  ) { }

  // api/admin/system_extra_data
  getAllSystemExtraData() {

  }

  getSystemExtraData() {

  }

  getSystemExtraDataTypeList() {

  }
}
