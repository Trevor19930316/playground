import { Injectable } from '@angular/core';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class WebsiteInfoService {

  id = 1;

  constructor(
    private apiService: ApiService,
  ) { }

  get() {
    return this.apiService.get(`/admin/website_info/${this.id}`);
  }

  update(data: any) {
    return this.apiService.put(`/admin/website_info/${this.id}`, {
      name: data.name,
      website_logo_url: data.website_logo_url,
      title: data.title,
      title_logo_url: data.title_logo_url,
      phone: data.phone,
      email: data.email,
      company_address: data.company_address,
      line: data.line,
      facebook: data.facebook,
      instagram: data.instagram,
    });
  }
}
