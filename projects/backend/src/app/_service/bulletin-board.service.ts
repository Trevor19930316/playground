import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class BulletinBoardService {

  constructor(
    private apiService: ApiService,
  ) { }

  // 公佈欄
  getAll() {
    return this.apiService.get(`/admin/bulletin_boards`);
  }

  get(id: number) {
    return this.apiService.get(`/admin/bulletin_boards/${id}`);
  }

  add(data: any) {
    return this.apiService.post(`/admin/bulletin_boards`, {
      title: data.title,
      subtitle: data.subtitle,
      content: data.content,
      start_date: !!data.start_date ? moment(data.start_date).format('YYYY-MM-DD HH:mm:ss') : null,
      end_date: !!data.end_date ? moment(data.end_date).format('YYYY-MM-DD HH:mm:ss') : null,
      // image_url: data.image_url,
      sort: Number(data.sort),
      valid_flag: Number(data.valid_flag),
    });
  }

  update(id: number, data: any) {
    return this.apiService.put(`/admin/bulletin_boards/${id}`, {
      title: data.title,
      subtitle: data.subtitle,
      content: data.content,
      start_date: !!data.start_date ? moment(data.start_date).format('YYYY-MM-DD HH:mm:ss') : null,
      end_date: !!data.end_date ? moment(data.end_date).format('YYYY-MM-DD HH:mm:ss') : null,
      // image_url: data.image_url,
      sort: Number(data.sort),
      valid_flag: Number(data.valid_flag),
    });
  }

  destroy(id: number) {
    return this.apiService.delete(`/admin/bulletin_boards/${id}`);
  }

}
