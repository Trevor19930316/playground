import { Injectable } from '@angular/core';

import { ApiService } from 'src/app/_service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private apiService: ApiService,
  ) { }

  // 訂單
  getAll() {
    return this.apiService.get(`/admin/orders`);
  }

  get(id: number) {
    return this.apiService.get(`/admin/orders/${id}`);
  }

  // 訂單明細
  getAllItem(order_id: number) {
    return this.apiService.get(`/admin/orders/${order_id}/order_items`);
  }

  getItem(id: number) {
    return this.apiService.get(`/admin/order_items/${id}`);
  }

}
