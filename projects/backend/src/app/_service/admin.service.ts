import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private apiService: ApiService,
  ) { }

  getAll() {
    return this.apiService.get(`/admin/admins`);
  }

  get(id: number) {
    return this.apiService.get(`/admin/admins/${id}`);
  }

  add(data: any) {
    return this.apiService.post(`/admin/admins`, {
      account: data.account,
      password: Md5.hashStr(data.password),
      name: data.name,
      valid_flag: Number(data.valid_flag),
    });
  }

  update(id: number, data: any, is_super: boolean) {
    let params = {
      account: data.account,
      name: data.name,
      valid_flag: Number(data.valid_flag),
    };
    if (data.password) {
      Object.assign(params, { password: Md5.hashStr(data.password) })
    }
    if (is_super) {
      Object.assign(params, { is_super: Number(data.is_super) })
    }
    return this.apiService.put(`/admin/admins/${id}`, params);
  }

  destroy(id: number) {
    return this.apiService.delete(`/admin/admins/${id}`);
  }

  // 取得個人資訊
  fetchProfile() {
    return this.apiService.get(`/admin/auth/user_profile`);
  }

  // 最高權限
  async isSuperAdmin(): Promise<boolean> {
    const res = await this.fetchProfile().toPromise();
    return res.is_super;
  }
}
