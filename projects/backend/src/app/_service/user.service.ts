import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';
import * as _ from 'lodash';

import { IAddress, ICountry, IUserLevel } from 'src/app/_model';
import { ApiService, DefinitionService } from 'src/app/_service';
/**
 *
 *
 * @export
 * @class UserService
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  // 授權書 id
  authorizationId = 1;
  // 會員權益 id
  rightsId = 2;

  countries!: ICountry[];
  address!: IAddress[];

  constructor(
    private apiService: ApiService,
    private definitionService: DefinitionService,
  ) {
    this.definitionService.getCountries().subscribe(res => {
      this.countries = res;
    });

    this.definitionService.getAddress().subscribe(res => {
      this.address = res;
    });
  }

  /**
   * API - 所有會員資料
   */
  getAll() {
    return this.apiService.get('/admin/users');
  }
  /**
   * API - 會員資料
   *
   * @param id 會員id
   */
  get(id: number) {
    return this.apiService.get(`/admin/users/${id}`).pipe(
      map(data => {
        const result = _.find(this.address, { 'id': data.address_postal_id });
        return ({
          ...data,
          addressData: {
            country_id: !!result ? result.country_id : null,
            country_name: !!result ? result.country_name : null,
            city: !!result ? result.city : null,
            district: !!result ? result.district : null,
            zip_code: !!result ? result.zip_code : null,
          },
        });
      }),
    );
  }
  /**
   * API - 新增會員資料
   *
   * @param data
   */
  add(data: any) {
    return this.apiService.post(`/admin/users`, {
      user_level_id: data.user_level_id,
      account: data.account,
      password: Md5.hashStr(data.phone.phone_number.slice(-6)), // 密碼預設手機後六碼
      name: data.name,
      phone_country: data.phone.phone_country,
      phone_number: data.phone.phone_number,
      email: data.email,
      address_postal_id: data.address.address_postal_id,
      address: data.address.address,
      valid_flag: Number(data.valid_flag),
    });
  }
  /**
   * API - 更新會員資料
   *
   * @param id   會員id
   * @param data
   */
  update(id: number, data: any) {
    return this.apiService.put(`/admin/users/${id}`, {
      user_level_id: data.user_level_id,
      name: data.name,
      phone_country: data.phone.phone_country,
      phone_number: data.phone.phone_number,
      email: data.email,
      address_postal_id: data.address.address_postal_id,
      address: data.address.address,
      valid_flag: Number(data.valid_flag),
    });
  }
  /**
   * API - 會員密碼重置
   *
   * @param id   會員id
   */
  resetPassword(id: number) {
    return this.apiService.put(`/admin/users/resetPassword/${id}`);
  }
  /**
   * API - 所有會員等級資料
   */
  getAllLevel() {
    return this.apiService.get('/admin/user_levels').pipe(
      map(userLevels => {
        return userLevels.map((userLevel: IUserLevel) => {
          // 自身抽成
          const selfBonus = _.find(userLevel.user_level_bonuses, { 'bonus_from': 0 });
          return ({
            ...userLevel,
            selfBonus: selfBonus,
          });
        })
      }),
    );
  }
  /**
   * API - 會員等級資料
   *
   * @param id 會員等級id
   */
  getLevel(id: number) {
    return this.apiService.get(`/admin/user_levels/${id}`);
  }
  /**
   * API - 新增會員等級資料
   *
   * @param data
   */
  addLevel(data: any) {
    return this.apiService.post(`/admin/user_levels`, {
      no: data.no,
      name: data.name,
      sort: Number(data.sort),
      valid_flag: Number(data.valid_flag),
    });
  }
  /**
   * API - 更新會員等級資料
   *
   * @param id   會員等級id
   * @param data
   */
  updateLevel(id: number, data: any) {
    return this.apiService.put(`/admin/user_levels/${id}`, {
      no: data.no,
      name: data.name,
      sort: Number(data.sort),
      valid_flag: Number(data.valid_flag),
    });
  }
  /**
   * API - 更新會員等級抽成資料
   *
   * @param id 會員等級抽成id
   */
  updateLevelBonus(id: number, data: any) {
    return this.apiService.put(`/admin/user_level_bonuses/${id}`, {
      bonus_from: data.bonus_from,
      bonus_percent: data.bonus_percent,
    });
  }
  /**
   * API - 授權書資料
   */
  getAuthorization() {
    return this.apiService.get(`/admin/system_extra_data/${this.authorizationId}}`);
  }
  /**
   * API - 更新授權書資料
   *
   * @param data
   */
  updateAuthorization(data: any) {
    return this.apiService.put(`/admin/system_extra_data/${this.authorizationId}`, {
      title: data.title,
      content: data.content,
    });
  }
  /**
   * API - 會員權益資料
   */
  getRights() {
    return this.apiService.get(`/admin/system_extra_data/${this.rightsId}}`);
  }
  /**
   * API - 更新會員權益資料
   *
   * @param data
   */
  updateRights(data: any) {
    return this.apiService.put(`/admin/system_extra_data/${this.rightsId}`, {
      title: data.title,
      content: data.content,
    });
  }
}
