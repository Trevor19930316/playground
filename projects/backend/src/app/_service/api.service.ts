import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { IApiResponse } from 'src/app/_model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }

  apiResponseStatus(res: IApiResponse): boolean {
    return res.status;
  }

  apiResponseMessage(res: IApiResponse): string {
    return res.message;
  }

  apiResponseData(res: IApiResponse): Array<any> {
    return res.data;
  }

  apiResponseErrorMessage(res: IApiResponse): Array<any> {
    return res.validation_error_message;
  }

  private formatErrors(error: any) {
    return throwError(error.error);
  }

  // private formatApiResponse(res: IApiResponse) {
  //   return res.status ? res.data : this.formatApiResponseErrors(res);
  // }

  // private formatApiResponseErrors(res: IApiResponse) {
  //   // error_code 111
  //   // 表單驗證錯誤
  // }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get<any>(path, { params })
      .pipe(
        map((data) => {
          return data.status ? data.data : [];
        }),
        catchError(this.formatErrors),
      );
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(path, body)
      .pipe(
        catchError(this.formatErrors)
      );
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(path, body)
      .pipe(
        catchError(this.formatErrors)
      );
  }

  delete(path: string): Observable<any> {
    return this.http.delete(path)
      .pipe(
        catchError(this.formatErrors)
      );
  }
}
