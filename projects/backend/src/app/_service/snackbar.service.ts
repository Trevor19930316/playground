import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { ApiService } from 'src/app/_service';

interface ToasterMessage {
  type: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class SnackbarService implements OnDestroy {

  private toastStream: BehaviorSubject<ToasterMessage | {}> = new BehaviorSubject<ToasterMessage | {}>({});
  private toastStream$ = this.toastStream.asObservable();

  constructor(
    public snackBar: MatSnackBar,
    private zone: NgZone,
    private apiService: ApiService,
  ) {
    this.toastStream$.pipe(
      concatMap((toast: ToasterMessage | {}) => {
        if (!('type' in toast)) {
          return EMPTY;
        }
        return this.handleToastMessage(toast).afterDismissed();
      })
    ).subscribe((_) => {
    });
  }

  openSnackBar(
    message: string,
    action: string | undefined,
    color: string = 'green-snackbar'
  ): MatSnackBarRef<TextOnlySnackBar> {
    return this.zone.run(() => {
      const snackBarRef = this.snackBar.open(message, action, {
        duration: 5000,
        panelClass: color,
        horizontalPosition: 'center',
      });
      snackBarRef.onAction().subscribe(() => snackBarRef.dismiss());
      return snackBarRef;
    });
  }

  handleToastMessage(toast: ToasterMessage): MatSnackBarRef<TextOnlySnackBar> {
    return this.openSnackBar(toast.message, 'x', `${toast.type}-snackbar`);
  }

  addToast(toast: ToasterMessage): void {
    this.toastStream.next(toast);
  }

  success(message: string) {
    this.addToast({ type: 'green', message: message });
  }

  danger(message: string) {
    this.addToast({ type: 'red', message: message });
  }

  warning(message: string) {
    this.addToast({ type: 'warning', message: message });
  }

  apiResopnse(response: any) {
    const message = this.apiService.apiResponseMessage(response);
    if (this.apiService.apiResponseStatus(response)) {
      this.success(message);
    } else {
      this.danger(message);
    }
  }

  login(status: boolean) {
    if (!status) {
      // 登入失敗
      this.danger('登入失敗');
    }
  }

  ngOnDestroy(): void {
    this.toastStream.next({});
    this.toastStream.complete();
  }
}
