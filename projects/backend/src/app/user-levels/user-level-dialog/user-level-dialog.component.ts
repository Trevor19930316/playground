import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IUserLevel } from 'src/app/_model';
import { ApiService, UserService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-user-level-dialog',
  templateUrl: './user-level-dialog.component.html',
  styleUrls: ['./user-level-dialog.component.scss']
})
export class UserLevelDialogComponent implements OnInit {

  userLevel?: IUserLevel;

  form = this.fb.group({
    no: ['', Validators.compose([
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
      Validators.pattern("^[0-9a-zA-Z]+$"),
    ])],
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(30)
    ])],
    sort: ['0', Validators.compose([
      Validators.required,
      Validators.min(0),
      Validators.pattern("^[0-9]+$"),
    ])],
    valid_flag: ['1', Validators.required],
  });

  loading = false;
  errorMessage = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchUserLevel(id: number) {
    this.userService.getLevel(id).subscribe(res => {
      this.userLevel = res;
      this._formSetValue(res);
    });
  }

  private _formSetValue(data: any) {
    this.form.setValue({
      no: data.no,
      name: data.name,
      sort: data.sort,
      valid_flag: (data.valid_flag).toString(),
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const apiFunc = this.data.type == 'add' ?
      this.userService.addLevel(this.form.value) : this.userService.updateLevel(this.data.id, this.form.value);
    apiFunc.subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
    });
  }

  ngOnInit(): void {
    if (!!this.data.id) {
      this._fetchUserLevel(this.data.id);
    }
  }

}
