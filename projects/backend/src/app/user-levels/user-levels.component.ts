import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IUserLevel } from 'src/app/_model';
import { UserService, SnackbarService } from 'src/app/_service';
import { UserLevelDialogComponent } from './user-level-dialog/user-level-dialog.component';

@Component({
  selector: 'app-user-levels',
  templateUrl: './user-levels.component.html',
  styleUrls: ['./user-levels.component.scss']
})
export class UserLevelsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['sort', 'no', 'name', 'selfBonus', 'valid_flag', 'maintain'];
  dataSource = new MatTableDataSource<IUserLevel>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'no', Name: '編號' },
    { Key: 'name', Name: '名稱' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    }
  ];

  dataEdit: any = [];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) { }

  private _fetchUserLevels(filter: any = false) {
    this.tableIsLoading = true;

    this.userService.getAllLevel().subscribe(res => {

      this.dataSource.data = res;

      res.forEach((data: any) => {
        data.user_level_bonuses.forEach((user_level_bonus: any) => {
          this.dataEdit[user_level_bonus.id] = {
            isEdit: false,
            isChange: false,
            bonus: user_level_bonus.bonus_percent == 0.07 ? 7 : user_level_bonus.bonus_percent * 100,
          };
        });
      });

      console.log(this.dataEdit)

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchUserLevels(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  // 明細
  clickDetail(data: IUserLevel) {
    this._openDialog('detail', data.id);
  }

  // 新增
  clickAdd() {
    this._openDialog('add');
  }

  // 編輯
  clickUpdate(data: IUserLevel) {
    this._openDialog('update', data.id);
  }

  // 刪除
  clickDelete(data: IUserLevel) {
    // const dialogDelete = this.dialog.open(DialogDeleteConfirmComponent, {
    // autoFocus: false,
    // data: {
    //   name: data.name,
    // },
    // });
    // dialogDelete.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.adminService.destroy(data.id).subscribe(res => {
    //       console.log(res)
    //       this.snackbarService.apiResopnse(res);
    //       this.fetchAdminList();
    //     });
    //   }
    // });
  }

  private _openDialog(type: string, id?: number) {
    const dialogRef = this.dialog.open(UserLevelDialogComponent, {
      autoFocus: false,
      minWidth: '80%',
      maxWidth: '95%',
      data: { type: type, id: id },
    });

    dialogRef.afterClosed().pipe<boolean>(
      filter(p => !!p),
    ).subscribe(() => {
      this._fetchUserLevels();
    });
  }

  // 編輯抽成
  onEdited(event: any, user_level_bonus: any) {
    this.dataEdit[user_level_bonus.id].isEdit = true;
    this.dataEdit[user_level_bonus.id].isChange = true;
    if (user_level_bonus.bonus_percent * 100 == event) {
      this.dataEdit[user_level_bonus.id].isChange = false;
    }
  }

  inputFocusOut(user_level_bonus: any) {
    if (user_level_bonus.bonus_percent * 100 == this.dataEdit[user_level_bonus.id].bonus) {
      this.dataEdit[user_level_bonus.id].isEdit = false;
      this.dataEdit[user_level_bonus.id].isChange = false;
    }
  }

  saveUpdate(user_level_bonus: any, status = true) {
    if (status && this.dataEdit[user_level_bonus.id].bonus !== null) {
      const data = {
        bonus_from: user_level_bonus.bonus_from,
        bonus_percent: this.dataEdit[user_level_bonus.id].bonus / 100,
      };
      this.userService.updateLevelBonus(user_level_bonus.id, data).subscribe(res => {
        this.snackbarService.apiResopnse(res);
        user_level_bonus.bonus_percent = this.dataEdit[user_level_bonus.id].bonus / 100;
      });
    } else if (!status) {
      this.dataEdit[user_level_bonus.id].bonus = user_level_bonus.bonus_percent * 100;
    }
    this.dataEdit[user_level_bonus.id].isEdit = false;
    this.dataEdit[user_level_bonus.id].isChange = false;
  }

  ngOnInit() {
    this._fetchUserLevels();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
