import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { UserLevelsComponent } from './user-levels.component';

const routes: Routes = [
  {
    path: '',
    component: UserLevelsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class UserLevelsRoutingModule {
}
