import { NgModule } from '@angular/core';
// module
import { UserLevelsRoutingModule } from './user-levels-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { UserLevelsComponent } from './user-levels.component';
import { UserLevelDialogComponent } from './user-level-dialog/user-level-dialog.component';


@NgModule({
  declarations: [
    UserLevelsComponent,
    UserLevelDialogComponent
  ],
  imports: [
    UserLevelsRoutingModule,
    ShareModule,
  ]
})
export class UserLevelsModule { }
