import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { OrdersComponent } from './orders.component';
import { OrderImportComponent } from './order-import/order-import.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
  },
  {
    path: 'import',
    component: OrderImportComponent,
    data: {
      breadcrumb: '匯入',
      isClickable: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class OrdersRoutingModule {
}
