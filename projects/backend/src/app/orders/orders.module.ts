import { NgModule } from '@angular/core';
// module
import { OrdersRoutingModule } from './orders-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { OrdersComponent } from './orders.component';
import { OrderImportComponent } from './order-import/order-import.component';

@NgModule({
  declarations: [
    OrdersComponent,
    OrderImportComponent,
  ],
  imports: [
    OrdersRoutingModule,
    ShareModule,
  ]
})
export class OrdersModule { }
