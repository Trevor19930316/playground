import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IOrder } from 'src/app/_model';
import { OrderService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, AfterViewInit {

  displayedColumns = ['no', 'price', 'created_at', 'updated_at', 'maintain'];
  dataSource = new MatTableDataSource<IOrder>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'no', Name: '編號' },
    { Key: 'price', Name: '金額' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    // {
    //   Column: 'valid_flag', Name: '狀態', Options: [
    //     { Key: '0', Name: '無效' },
    //     { Key: '1', Name: '有效' },
    //   ],
    // }
  ];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private orderService: OrderService,
    private snackbarService: SnackbarService,
  ) { }

  // 訂單清單
  private _fetchOrders(filter: any = false) {
    this.tableIsLoading = true;

    this.orderService.getAll().subscribe(res => {

      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchOrders(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  ngOnInit(): void {
    this._fetchOrders();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
