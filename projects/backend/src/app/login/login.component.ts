import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

// service
import { AuthService } from 'src/app/_service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = this.fb.group({
    Account: ['', Validators.required],
    Pwd: ['', Validators.required]
  });

  loginFailed = false;
  loginLoading = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
  ) { }

  // 登入
  login() {
    this.loginFailed = false;

    if (this.form.invalid) {
      return;
    }

    this.loginLoading = true;

    this.authService.login(this.form.value.Account, this.form.value.Pwd).toPromise().then(res => {
      this.loginLoading = false;
      if (res.status) {
        this.authService.setToken(res.data.token);
        this.router.navigate(['/']);
      } else {
        this.loginFailed = true;
      }
    });
  }

  ngOnInit(): void {

  }

}
