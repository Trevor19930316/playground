import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IProduct } from 'src/app/_model';
import { ProductService, SnackbarService } from 'src/app/_service';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';
import { DialogDeleteConfirmComponent } from 'src/app/_component/dialog-delete-confirm/dialog-delete-confirm.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['no', 'name', 'valid_flag', 'created_at', 'updated_at', 'maintain'];
  dataSource = new MatTableDataSource<IProduct>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'no', Name: '編號' },
    { Key: 'name', Name: '名稱' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    }
  ];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private productService: ProductService,
    private snackbarService: SnackbarService,
  ) { }

  // 商品清單
  private _fetchProducts(filter: any = false) {
    this.tableIsLoading = true;

    this.productService.getAll().subscribe(res => {

      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchProducts(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  // 新增
  clickAdd() {
    this._openDialog('add');
  }

  // 編輯
  clickUpdate(data: IProduct) {
    this._openDialog('update', data.id);
  }

  // 刪除
  clickDelete(data: IProduct) {
    const dialogDelete = this.dialog.open(DialogDeleteConfirmComponent, {
      autoFocus: false,
      data: {
        name: data.no + ' ' + data.name,
      },
    });
    dialogDelete.afterClosed().subscribe(result => {
      if (result) {
        this.productService.destroy(data.id).subscribe(res => {
          this.snackbarService.apiResopnse(res);
          this._fetchProducts();
        });
      }
    });
  }

  private _openDialog(type: string, id?: number) {
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      autoFocus: false,
      minWidth: '80%',
      maxWidth: '95%',
      data: { type: type, id: id },
    });

    dialogRef.afterClosed().pipe<boolean>(
      filter(p => !!p),
    ).subscribe(() => {
      this._fetchProducts();
    });
  }

  ngOnInit() {
    this._fetchProducts();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
