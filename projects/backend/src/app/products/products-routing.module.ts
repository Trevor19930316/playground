import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { ProductsComponent } from './products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
// model
import { ProductRoutes } from 'src/app/_model/_enum/routes.enum';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
  },
  {
    path: ':productId',
    component: ProductDetailComponent,
    data: {
      breadcrumb: '明細',
      isClickable: false,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule { }
