import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IProduct } from 'src/app/_model';
import { ApiService, ProductService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {

  product?: IProduct;

  form = this.fb.group({
    no: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(255)
    ])],
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(100)
    ])],
    valid_flag: ['1', Validators.required],
  });

  loading = false;
  errorMessage = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private productService: ProductService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchProduct(id: number) {
    this.productService.get(id).subscribe(res => {
      this.product = res;

      this.form.setValue({
        no: res.no,
        name: res.name,
        valid_flag: (res.valid_flag).toString(),
      });
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const apiFunc = this.data.type == 'add' ?
      this.productService.add(this.form.value) : this.productService.update(this.data.id, this.form.value);
    apiFunc.subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
    });
  }

  ngOnInit() {
    if (!!this.data.id) {
      this._fetchProduct(this.data.id);
    }
  }

}
