import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { IProductItem } from 'src/app/_model';
import { FiltersService } from 'src/app/_component/filters/filters.service';
import { ProductService, SnackbarService } from 'src/app/_service';
import { ProductItemDialogComponent } from './product-item-dialog/product-item-dialog.component';
import { DialogDeleteConfirmComponent } from 'src/app/_component/dialog-delete-confirm/dialog-delete-confirm.component';

@Component({
  selector: 'app-product-items',
  templateUrl: './product-items.component.html',
  styleUrls: ['./product-items.component.scss']
})
export class ProductItemsComponent implements OnInit, AfterViewInit {

  @Input() productId!: number;

  displayedColumns = ['no', 'name', 'price', 'gp', 'valid_flag', 'created_at', 'updated_at', 'maintain'];
  dataSource = new MatTableDataSource<IProductItem>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'no', Name: '編號' },
    { Key: 'name', Name: '名稱' },
    { Key: 'price', Name: '價格' },
    { Key: 'gp', Name: 'GP' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    }
  ];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private productService: ProductService,
    private snackbarService: SnackbarService,
  ) { }

  private _fetchProductItems(filter: any = false) {
    this.tableIsLoading = true;

    this.productService.getAllItem(this.productId).subscribe(res => {

      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchProductItems(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  // 明細
  clickDetail(data: IProductItem) {
    this._openDialog('detail', data.id);
  }

  // 新增
  clickAdd() {
    this._openDialog('add');
  }

  // 編輯
  clickUpdate(data: IProductItem) {
    this._openDialog('update', data.id);
  }

  // 刪除
  clickDelete(data: IProductItem) {
    const dialogDelete = this.dialog.open(DialogDeleteConfirmComponent, {
      autoFocus: false,
      data: {
        name: data.no + ' ' + data.name,
      },
    });
    dialogDelete.afterClosed().subscribe(result => {
      if (result) {
        this.productService.destroyItem(data.id).subscribe(res => {
          this.snackbarService.apiResopnse(res);
          this._fetchProductItems();
        });
      }
    });
  }

  private _openDialog(type: string, id?: number) {
    const dialogRef = this.dialog.open(ProductItemDialogComponent, {
      autoFocus: false,
      minWidth: '80%',
      maxWidth: '95%',
      data: { type: type, productId: this.productId, productItemId: id },
    });

    dialogRef.afterClosed().pipe<boolean>(
      filter(p => !!p),
    ).subscribe(() => {
      this._fetchProductItems();
    });
  }

  ngOnInit(): void {
    this._fetchProductItems();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
