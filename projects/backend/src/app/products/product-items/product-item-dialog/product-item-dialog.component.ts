import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IProductItem } from 'src/app/_model';
import { ApiService, ProductService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-product-item-dialog',
  templateUrl: './product-item-dialog.component.html',
  styleUrls: ['./product-item-dialog.component.scss']
})
export class ProductItemDialogComponent implements OnInit {

  productItem?: IProductItem;

  form = this.fb.group({
    no: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(255)
    ])],
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(100)
    ])],
    price: ['0', Validators.compose([
      Validators.required,
    ])],
    gp: ['0', Validators.compose([
      Validators.required,
    ])],
    valid_flag: ['1', Validators.required],
  });

  loading = false;
  errorMessage = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private productService: ProductService,
    private snackbarService: SnackbarService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchProductItem(id: number) {
    this.productService.getItem(id).subscribe(res => {
      this.productItem = res;

      this.form.setValue({
        no: res.no,
        name: res.name,
        price: res.price,
        gp: res.gp,
        valid_flag: (res.valid_flag).toString(),
      });
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const apiFunc = this.data.type == 'add' ?
      this.productService.addItem(this.data.productId, this.form.value) : this.productService.updateItem(this.data.productItemId, this.form.value);
    apiFunc.subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
    });
  }

  ngOnInit(): void {
    if (!!this.data.productItemId) {
      this._fetchProductItem(this.data.productItemId);
    }
  }

}
