import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductService } from 'src/app/_service';
import { IProduct } from 'src/app/_model';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product!: IProduct;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
  ) { }

  private _fetchProduct(id: number) {
    this.productService.get(id).subscribe(res => {
      this.product = res;
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(res => {
      this._fetchProduct(res.productId);
    });
  }
}
