import { NgModule } from '@angular/core';
// module
import { ProductsRoutingModule } from './products-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { ProductsComponent } from './products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';
import { ProductItemsComponent } from './product-items/product-items.component';
import { ProductItemDialogComponent } from './product-items/product-item-dialog/product-item-dialog.component';

@NgModule({
  declarations: [
    ProductsComponent,
    ProductDetailComponent,
    ProductDialogComponent,
    ProductItemsComponent,
    ProductItemDialogComponent,
  ],
  imports: [
    ProductsRoutingModule,
    ShareModule,
  ]
})
export class ProductsModule { }
