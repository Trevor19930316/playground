import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdiButtonComponent } from './button.component';

describe('AdiButtonComponent', () => {
  let component: AdiButtonComponent;
  let fixture: ComponentFixture<AdiButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdiButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdiButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
