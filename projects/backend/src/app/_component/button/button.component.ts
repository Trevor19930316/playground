import { Attribute, Component, ContentChild, HostBinding, Input } from '@angular/core';

import { AdiButtonLoaderIconDirective } from './button.directive';

@Component({
  selector: 'button[adiButton]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class AdiButtonComponent {
  @HostBinding("class.loading")
  @HostBinding("attr.aria-disabled")
  @Input() loading = false;
  @HostBinding("class") get classes(): string {
    return this.variant || "primary";
  }

  @ContentChild(AdiButtonLoaderIconDirective)
  icon!: AdiButtonLoaderIconDirective;

  constructor(
    @Attribute("variant")
    private variant: "primary" | "secondary" | "outline" | "danger" = "primary",
  ) { }
}
