import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextValidFlagComponent } from './text-valid-flag.component';

describe('TextValidFlagComponent', () => {
  let component: TextValidFlagComponent;
  let fixture: ComponentFixture<TextValidFlagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextValidFlagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextValidFlagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
