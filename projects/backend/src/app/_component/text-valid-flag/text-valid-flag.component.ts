import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-text-valid-flag',
  templateUrl: './text-valid-flag.component.html',
  styleUrls: ['./text-valid-flag.component.scss']
})
export class TextValidFlagComponent implements OnInit {

  @Input() valid_flag?: boolean | number = false;

  constructor() { }

  ngOnInit(): void {
  }

}
