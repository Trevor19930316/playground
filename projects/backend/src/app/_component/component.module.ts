import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

import { IconModule } from '../_share/icon/icon.module';
import { MateriaModule } from '../_share/materia/materia.module';
import { FiltersComponent } from './filters/filters.component';
import { AdiButtonComponent } from './button/button.component';
import { AdiButtonLoaderIconDirective } from './button/button.directive';
import { ButtonAddComponent } from './button-add/button-add.component';
import { ButtonDeleteComponent } from './button-delete/button-delete.component';
import { ButtonEditComponent } from './button-edit/button-edit.component';
import { ButtonImportComponent } from './button-import/button-import.component';
import { CountryAddressComponent } from './country-address/country-address.component';
import { CountryCodeComponent } from './country-code/country-code.component';
import { DialogDeleteConfirmComponent } from './dialog-delete-confirm/dialog-delete-confirm.component';
import { SectionComponent } from './section/section.component';
import { RowComponent } from './row/row.component';
import { RowFormComponent } from './row-form/row-form.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { TableBlockComponent } from './table-block/table-block.component';
import { TextValidFlagComponent } from './text-valid-flag/text-valid-flag.component';

@NgModule({
  declarations: [
    BreadcrumbComponent,
    FiltersComponent,
    AdiButtonComponent,
    AdiButtonLoaderIconDirective,
    ButtonAddComponent,
    ButtonDeleteComponent,
    ButtonEditComponent,
    ButtonImportComponent,
    CountryAddressComponent,
    CountryCodeComponent,
    DialogDeleteConfirmComponent,
    SectionComponent,
    RowComponent,
    RowFormComponent,
    ProgressBarComponent,
    TableBlockComponent,
    TextValidFlagComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    IconModule,
    MateriaModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    BreadcrumbComponent,
    FiltersComponent,
    AdiButtonComponent,
    AdiButtonLoaderIconDirective,
    ButtonAddComponent,
    ButtonDeleteComponent,
    ButtonEditComponent,
    ButtonImportComponent,
    CountryAddressComponent,
    CountryCodeComponent,
    DialogDeleteConfirmComponent,
    SectionComponent,
    RowComponent,
    RowFormComponent,
    ProgressBarComponent,
    TableBlockComponent,
    TextValidFlagComponent,
  ]
})
export class ComponentModule { }
