import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-block',
  templateUrl: './table-block.component.html',
  styleUrls: ['./table-block.component.scss']
})
export class TableBlockComponent implements OnInit {

  @Input() tableIsLoading: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
