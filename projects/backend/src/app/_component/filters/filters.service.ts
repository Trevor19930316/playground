import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  constructor() { }

  filters(filter: any, data: any) {

    const keywords = filter.keywords;
    const keywordsColumn = filter.keywordsColumn;
    const filterSelectMulti = filter.filterSelectMulti;
    const filterDatetimeRangeMulti = filter.filterDatetimeRangeMulti;

    if (!!keywordsColumn) {
      data = data.filter((val: any) => val[keywordsColumn].includes(keywords));
    }

    _.forIn(filterDatetimeRangeMulti, function (values, key) {
      data = data.filter((val: any) => {
        if (values.start != null && !moment(val[key]).isSameOrAfter(values.start)) {
          return false;
        }
        if (values.end != null && !moment(val[key]).isSameOrBefore(values.end)) {
          return false;
        }
        return true;
      });
    });

    _.forIn(filterSelectMulti, function (values, key) {
      if (values.length > 0) {
        data = data.filter((val: any) => {
          return values.indexOf(String(val[key])) >= 0;
        });
      }
    });

    return data;
  }
}
