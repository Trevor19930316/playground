import { Component, Input } from '@angular/core';

@Component({
  selector: 'button-import',
  templateUrl: './button-import.component.html',
  styleUrls: ['./button-import.component.scss']
})
export class ButtonImportComponent  {

  @Input() loading = false;

}
