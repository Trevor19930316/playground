import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as _ from 'lodash';

import { DefinitionService } from 'src/app/_service';
import { IAddress, ICountry } from 'src/app/_model';
import { ControlContainer, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-country-address',
  templateUrl: './country-address.component.html',
  styleUrls: ['./country-address.component.scss']
})
export class CountryAddressComponent implements OnInit {

  @Input() address_id!: number;
  fullAddress !: string;

  countries!: ICountry[];
  address!: IAddress[];
  cities!: IAddress[];
  districts !: IAddress[];
  // 選取資訊 [zip_code+country_name+city+district]
  selectedInfo!: string;

  addressFormGroup!: FormGroup;

  constructor(
    private definitionService: DefinitionService,
    private controlContainer: ControlContainer,
  ) {
    this.definitionService.getCountries().subscribe(res => {
      this.countries = res;
    });

    this.definitionService.getAddress().subscribe(res => {
      this.address = res;
      if (!!this.address_id) {
        const result = _.find(this.address, { 'id': this.address_id });
        if (!!result) {
          this.fullAddress = result.zip_code + ' ' + result.country_name + result.city + result.district;
        }
      }

      if (!!this.controlContainer.control?.value.address_postal_id) {
        this.filterCountry(this.controlContainer.control.value.country_id);
        this.filterCity(this.controlContainer.control.value.city);
        this.chooseAddress(this.controlContainer.control.value.address_postal_id);
      }
    });
  }

  get f() {
    return this.addressFormGroup.controls;
  }

  // 選擇國家
  filterCountry(event: any) {
    this.selectedInfo = '';
    this.cities = _(this.address)
      .filter({ 'country_id': event })
      .uniqBy('city')
      .value();
  }

  // 選擇城市
  filterCity(event: any) {
    this.selectedInfo = '';
    this.districts = _(this.address)
      .filter({ 'city': event })
      .value();
  }

  // 選擇鄉鎮市區
  chooseAddress(event: any) {
    this.selectedInfo = '';
    const res = _.find(this.districts, { 'id': event });
    if (res) {
      this.selectedInfo = res.zip_code + res.country_name + res.city + res.district;
    }
  }

  ngOnInit() {
    this.addressFormGroup = this.controlContainer.control as FormGroup;
  }

}
