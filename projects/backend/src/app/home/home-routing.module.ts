import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// model
import {
  AdminRoutes,
  UserRoutes,
  ProductRoutes,
  FrontendRoutes,
  OrderRoutes,
} from '../_model/_enum/routes.enum';
// component
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: UserRoutes.User_Levels,
        loadChildren: () => import('../user-levels/user-levels.module').then((m) => m.UserLevelsModule),
        data: {
          breadcrumb: '會員等級',
          isClickable: true,
        },
      },
      {
        path: UserRoutes.Users,
        loadChildren: () => import('../users/users.module').then((m) => m.UsersModule),
        data: {
          breadcrumb: '會員',
          isClickable: true,
        },
      },
      {
        path: FrontendRoutes.WebsiteInfo,
        loadChildren: () => import('../website-info/website-info.module').then((m) => m.WebsiteInfoModule),
        data: {
          breadcrumb: '站台資料',
          isClickable: true,
        },
      },
      {
        path: FrontendRoutes.Bulletin_Boards,
        loadChildren: () => import('../bulletin-boards/bulletin-boards.module').then((m) => m.BulletinBoardsModule),
        data: {
          breadcrumb: '公佈欄',
          isClickable: true,
        },
      },
      {
        path: FrontendRoutes.Authorization,
        loadChildren: () => import('../user-authorization/user-authorization.module').then((m) => m.UserAuthorizationModule),
        data: {
          breadcrumb: '授權書',
          isClickable: true,
        },
      },
      {
        path: FrontendRoutes.Rights,
        loadChildren: () => import('../user-rights/user-rights.module').then((m) => m.UserRightsModule),
        data: {
          breadcrumb: '會員權益',
          isClickable: true,
        },
      },
      {
        path: OrderRoutes.Orders,
        loadChildren: () => import('../orders/orders.module').then((m) => m.OrdersModule),
        data: {
          breadcrumb: '訂單',
          isClickable: true,
        },
      },
      {
        path: ProductRoutes.Products,
        loadChildren: () => import('../products/products.module').then((m) => m.ProductsModule),
        data: {
          breadcrumb: '商品',
          isClickable: true,
        },
      },
      {
        path: ProductRoutes.Products_Gp,
        loadChildren: () => import('../products-gp/products-gp.module').then((m) => m.ProductsGpModule),
        data: {
          breadcrumb: '商品 GP',
          isClickable: true,
        },
      },
      {
        path: AdminRoutes.Admins,
        loadChildren: () => import('../admins/admins.module').then((m) => m.AdminsModule),
        data: {
          breadcrumb: '管理員',
          isClickable: true,
        },
      },
    ],
  },
  // otherwise redirect
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class HomeRoutingModule {
}
