import { NgModule } from '@angular/core';
// module
import { HomeRoutingModule } from './home-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    HomeRoutingModule,
    ShareModule,
  ]
})
export class HomeModule {
}
