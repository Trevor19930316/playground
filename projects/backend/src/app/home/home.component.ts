import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';

import { IAdmin } from 'src/app/_model';
import { AuthService, AdminService } from 'src/app/_service';
import {
  AdminRoutes,
  UserRoutes,
  ProductRoutes,
  FrontendRoutes,
  OrderRoutes
} from '../_model/_enum/routes.enum';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSidenav) sidenav!: MatSidenav;
  profileData!: IAdmin;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private authService: AuthService,
    private adminService: AdminService,
  ) {
  }

  sideNav: any = [
    {
      header: '會員管理',
      menus: [
        {
          routerLink: UserRoutes.User_Levels,
          name: '會員等級',
        },
        {
          routerLink: UserRoutes.Users,
          name: '會員資料',
        },
      ],
    },
    {
      header: '前台管理',
      menus: [
        {
          routerLink: FrontendRoutes.WebsiteInfo,
          name: '站台資料',
        },
        {
          routerLink: FrontendRoutes.Bulletin_Boards,
          name: '公佈欄',
        },
        {
          routerLink: FrontendRoutes.Authorization,
          name: '授權書',
        },
        {
          routerLink: FrontendRoutes.Rights,
          name: '會員權益',
        },
      ],
    },
    {
      header: '訂單管理',
      menus: [
        {
          routerLink: OrderRoutes.Orders,
          name: '訂單資料',
        },
      ],
    },
    {
      header: '商品管理',
      menus: [
        {
          routerLink: ProductRoutes.Products,
          name: '商品資料',
        },
        {
          routerLink: ProductRoutes.Products_Gp,
          name: '商品 GP',
        },
      ],
    },
    {
      header: '後台管理',
      menus: [
        {
          routerLink: AdminRoutes.Admins,
          name: '管理員資料',
        },
      ],
    },
  ];

  // 個人資料
  private _fetchProfile() {
    this.adminService.fetchProfile().subscribe(res => {
      this.profileData = res;
    });
  }

  clickHome() {
    this.router.navigate(['/']);
  }

  clickSidenav() {
    if (this.sidenav.mode == 'over') {
      this.sidenav.close();
    }
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit(): void {
    // 個人資料
    this._fetchProfile();
  }

  ngAfterViewInit(): void {
    this.breakpointObserver.observe(['(max-width: 800px)']).subscribe((res) => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else if (this.sidenav.mode == 'over') {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    });
  }
}
