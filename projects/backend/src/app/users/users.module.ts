import { NgModule } from '@angular/core';
// module
import { UsersRoutingModule } from './users-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { UsersComponent } from './users.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';

@NgModule({
  declarations: [
    UsersComponent,
    UserDialogComponent
  ],
  imports: [
    UsersRoutingModule,
    ShareModule,
  ]
})
export class UsersModule { }
