import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IUser, IUserLevel } from 'src/app/_model';
import { ApiService, UserService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {

  user?: IUser;
  userLevels!: IUserLevel[];

  form = this.fb.group({
    account: ['', Validators.compose([
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
      Validators.pattern("^[0-9a-zA-Z]+$"),
    ])],
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(30)
    ])],
    user_level_id: ['', Validators.compose([
      Validators.required,
    ])],
    phone: this.fb.group({
      phone_country: ['', Validators.compose([
        Validators.required,
      ])],
      phone_number: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern("(09)[0-9 ]{8}"),
      ])],
    }),
    email: ['', Validators.compose([
      Validators.required,
      Validators.email,
      Validators.maxLength(100)
    ])],
    address: this.fb.group({
      country_id: ['', Validators.compose([
        Validators.required,
      ])],
      city: ['', Validators.compose([
        Validators.required,
      ])],
      address_postal_id: ['', Validators.compose([
        Validators.required,
      ])],
      address: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])],
    }),
    valid_flag: ['1', Validators.required],
  });

  loading = false;
  errorMessage = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apiService: ApiService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) {
    this._fetchUserLevelsList();
  }

  get f() {
    return this.form.controls;
  }

  // 會員等級
  private _fetchUserLevelsList() {
    this.userService.getAllLevel().subscribe(res => {
      this.userLevels = res;
    });
  }

  private _fetchUser(id: number) {
    this.userService.get(id).subscribe(res => {
      this.user = res;
      this._formSetValue(res);
    });
  }

  private _formSetValue(data: any) {
    this.form.setValue({
      account: data.account,
      name: data.name,
      user_level_id: data.user_level_id,
      phone: {
        phone_country: data.phone_country,
        phone_number: data.phone_number,
      },
      email: data.email,
      address: {
        country_id: data.addressData.country_id,
        city: data.addressData.city,
        address_postal_id: data.address_postal_id,
        address: data.address,
      },
      valid_flag: (data.valid_flag).toString(),
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const apiFunc = this.data.type == 'add' ?
      this.userService.add(this.form.value) : this.userService.update(this.data.id, this.form.value);
    apiFunc.subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this.dialogRef.close(true);
      } else {
        // 失敗
        this.loading = false;
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          if (key.startsWith('address')) {
            this.form.get(['address', key])?.setErrors({ 'message': value.join(', ') });
          } else if (key.startsWith('phone')) {
            this.form.get(['phone', key])?.setErrors({ 'message': value.join(', ') });
          } else {
            this.form.get(key)?.setErrors({ 'message': value.join(', ') });
          }
        });
      }
    });
  }

  // 密碼重置
  resetPassword() {
    this.userService.resetPassword(this.data.id).subscribe(res => {
      this.snackbarService.apiResopnse(res);
    });
  }

  ngOnInit(): void {
    if (!!this.data.id) {
      this._fetchUser(this.data.id);
    }
  }

}
