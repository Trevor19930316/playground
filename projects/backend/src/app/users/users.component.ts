import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IUser, IUserLevel } from 'src/app/_model';
import { UserService } from 'src/app/_service';
import { UserDialogComponent } from './user-dialog/user-dialog.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  displayedColumns = ['account', 'name', 'user_level', 'email', 'phone', 'valid_flag', 'maintain'];
  dataSource = new MatTableDataSource<IUser>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'account', Name: '帳號' },
    { Key: 'name', Name: '名稱' },
    { Key: 'email', Name: 'Email' },
    { Key: 'phone_country', Name: '國碼' },
    { Key: 'phone_number', Name: '電話' },
  ];
  filterDatetimeRanges = [
    {
      Column: 'created_at', Name: '新增時間',
    },
    {
      Column: 'updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'user_level_id', Name: '等級', Options: [],
    },
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    },
  ];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private userService: UserService,
  ) { }

  // 會員等級
  private _fetchUserLevelsList() {
    this.userService.getAllLevel().subscribe(res => {
      // 篩選選項
      const result = this.filterSelects.find((data: any) => data.Column === 'user_level_id');
      res.forEach((element: IUserLevel) => {
        result?.Options.push({ Key: String(element.id), Name: element.name });
      });
    });
  }

  // 會員清單
  private _fetchUsersList(filter: any = false) {
    this.tableIsLoading = true;
    this.userService.getAll().subscribe(res => {
      this.dataSource.data = res;

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchUsersList(event);
  }

  // 明細
  clickDetail(data: IUser) {
    this._openDialog('detail', data.id);
  }

  // 新增
  clickAdd() {
    this._openDialog('add');
  }

  // 編輯
  clickUpdate(data: IUser) {
    this._openDialog('update', data.id);
  }

  // 刪除
  clickDelete(data: IUser) {
    // const dialogDelete = this.dialog.open(DialogDeleteConfirmComponent, {
    //   autoFocus: false,
    //   data: {
    //     name: data.account + ' ' + data.name,
    //   },
    // });
    // dialogDelete.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.adminService.destroy(data.id).subscribe(res => {
    //       this.snackbarService.apiResopnse(res);
    //       this.fetchAdminList();
    //     });
    //   }
    // });
  }

  private _openDialog(type: string, id?: number) {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      autoFocus: false,
      minWidth: '80%',
      maxWidth: '95%',
      data: { type: type, id: id },
    });

    dialogRef.afterClosed().pipe<boolean>(
      filter(p => !!p),
    ).subscribe(() => {
      this._fetchUserLevelsList();
      this._fetchUsersList();
    });
  }

  ngOnInit(): void {
    this._fetchUserLevelsList();
    this._fetchUsersList();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
