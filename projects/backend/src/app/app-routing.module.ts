import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GlobalRoutes } from './_model/_enum/routes.enum';
import { AuthGuard } from './_helper/auth.guard';

const routes: Routes = [
  {
    path: GlobalRoutes.Login,
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: GlobalRoutes.Home,
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    canActivate: [AuthGuard],
    data: {
      breadcrumb: '首頁',
      isClickable: true,
    },
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
