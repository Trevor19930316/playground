import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { WebsiteInfoComponent } from './website-info.component';

const routes: Routes = [
  {
    path: '',
    component: WebsiteInfoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class WebsiteInfoRoutingModule {
}
