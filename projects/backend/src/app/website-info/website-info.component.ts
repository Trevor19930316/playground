import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ApiService, WebsiteInfoService, SnackbarService } from 'src/app/_service';
import { IWebsiteInfo } from 'src/app/_model';

@Component({
  selector: 'app-website-info',
  templateUrl: './website-info.component.html',
  styleUrls: ['./website-info.component.scss']
})
export class WebsiteInfoComponent implements OnInit {

  form = this.fb.group({
    name: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(30)
    ])],
    website_logo_url: ['', Validators.compose([
      // Validators.required,
    ])],
    title: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(50)
    ])],
    title_logo_url: ['', Validators.compose([
      // Validators.required,
    ])],
    phone: ['', Validators.compose([
      // Validators.required,
    ])],
    email: ['', Validators.compose([
      Validators.required,
      Validators.email,
      Validators.maxLength(100)
    ])],
    company_address: ['', Validators.compose([
      // Validators.required,
    ])],
    line: ['', Validators.compose([
      // Validators.required,
    ])],
    facebook: ['', Validators.compose([
      // Validators.required,
    ])],
    instagram: ['', Validators.compose([
      // Validators.required,
    ])],
  });

  loading = false;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private snackbarService: SnackbarService,
    private websiteInfoService: WebsiteInfoService,
  ) { }

  get f() {
    return this.form.controls;
  }

  private _fetchWebsiteInfo() {
    this.loading = true;
    this.websiteInfoService.get().subscribe(res => {
      this._formSetValue(res);
      this.loading = false;
    });
  }

  private _formSetValue(data: IWebsiteInfo) {
    this.form.setValue({
      name: data.name,
      website_logo_url: '',
      title: data.title,
      title_logo_url: '',
      phone: data.phone,
      email: data.email,
      company_address: data.company_address,
      line: data.line,
      facebook: data.facebook,
      instagram: data.instagram,
    });
  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.websiteInfoService.update(this.form.value).subscribe(res => {
      this.snackbarService.apiResopnse(res);
      if (this.apiService.apiResponseStatus(res)) {
        // 成功
        this._fetchWebsiteInfo();
      } else {
        // 失敗
        Object.entries(this.apiService.apiResponseErrorMessage(res)).forEach(([key, value]: [string, any]) => {
          this.form.get(key)?.setErrors({ 'message': value.join(', ') });
        });
      }
      this.loading = false;
    });
  }

  ngOnInit(): void {
    this._fetchWebsiteInfo();
  }

}
