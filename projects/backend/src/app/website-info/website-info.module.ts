import { NgModule } from '@angular/core';
// module
import { WebsiteInfoRoutingModule } from './website-info-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { WebsiteInfoComponent } from './website-info.component';

@NgModule({
  declarations: [
    WebsiteInfoComponent,
  ],
  imports: [
    WebsiteInfoRoutingModule,
    ShareModule,
  ]
})
export class WebsiteInfoModule { }
