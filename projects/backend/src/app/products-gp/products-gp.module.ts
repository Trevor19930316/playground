import { NgModule } from '@angular/core';
// module
import { ProductsGpRoutingModule } from './products-gp-routing.module';
import { ShareModule } from 'src/app/_share/share.module';
// component
import { ProductsGpComponent } from './products-gp.component';

@NgModule({
  declarations: [
    ProductsGpComponent
  ],
  imports: [
    ProductsGpRoutingModule,
    ShareModule,
  ]
})
export class ProductsGpModule { }
