import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs/operators';

import { FiltersService } from 'src/app/_component/filters/filters.service';
import { IProduct } from 'src/app/_model';
import { ProductService, SnackbarService } from 'src/app/_service';

@Component({
  selector: 'app-products-gp',
  templateUrl: './products-gp.component.html',
  styleUrls: ['./products-gp.component.scss']
})
export class ProductsGpComponent implements OnInit, AfterViewInit {

  displayedColumns = ['no', 'name', 'gp', 'valid_flag', 'updated_at'];
  dataSource = new MatTableDataSource<IProduct>();
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  tableIsLoading = true;

  keywordsColumns = [
    { Key: 'product_no', Name: '商品編號' },
    { Key: 'product_name', Name: '商品名稱' },
    { Key: 'product_item_no', Name: '規格編號' },
    { Key: 'product_item_name', Name: '規格名稱' },
    { Key: 'gp', Name: 'GP' },
  ];
  filterDatetimeRanges = [
    // {
    //   Column: 'created_at', Name: '新增時間',
    // },
    {
      Column: 'product_item_updated_at', Name: '更新時間',
    },
  ];
  filterSelects = [
    {
      Column: 'valid_flag', Name: '狀態', Options: [
        { Key: '0', Name: '無效' },
        { Key: '1', Name: '有效' },
      ],
    }
  ];

  dataEdit: any = [];

  constructor(
    private dialog: MatDialog,
    private filtersService: FiltersService,
    private productService: ProductService,
    private snackbarService: SnackbarService,
  ) { }

  // 商品GP清單
  private _fetchProductsGp(filter: any = false) {
    this.tableIsLoading = true;

    this.productService.getAllItemGp().subscribe(res => {

      this.dataSource.data = res;

      res.forEach(data => {
        this.dataEdit[data.product_item_id] = {
          isEdit: false,
          isChange: false,
          gp: data.gp,
        };
      });

      if (filter) {
        this.dataSource.data = this.filtersService.filters(filter, res);
        this.dataSource.filter = filter.keywords;
      }

      this.tableIsLoading = false;
    });
  }

  onSearch(event: any) {
    this._fetchProductsGp(event);
  }

  handleSort(event: Sort) {

  }

  handlePage(event: PageEvent) {

  }

  // 編輯
  onEdited(event: any, row: any) {
    this.dataEdit[row.product_item_id].isEdit = true;
    this.dataEdit[row.product_item_id].isChange = true;
    if (row.gp == event) {
      this.dataEdit[row.product_item_id].isChange = false;
    }
  }

  inputFocusOut(row: any) {
    if (row.gp == this.dataEdit[row.product_item_id].gp) {
      this.dataEdit[row.product_item_id].isEdit = false;
      this.dataEdit[row.product_item_id].isChange = false;
    }
  }

  saveUpdate(row: any, status = true) {
    if (status && this.dataEdit[row.product_item_id].gp !== null) {
      this.productService.updateItemGp(row.product_item_id, this.dataEdit[row.product_item_id].gp).subscribe(res => {
        this.snackbarService.apiResopnse(res);
        row.gp = this.dataEdit[row.product_item_id].gp;
        this.productService.getItem(row.product_item_id).subscribe(res => {
          row.product_item_updated_at = res.updated_at;
        });
        this.inputFocusOut(row);
      });
    } else if (!status) {
      this.dataEdit[row.product_item_id].gp = row.gp;
      this.inputFocusOut(row);
    }
  }

  ngOnInit() {
    this._fetchProductsGp();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
