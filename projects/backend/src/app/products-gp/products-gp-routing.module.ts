import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { ProductsGpComponent } from './products-gp.component';
// model
import { ProductRoutes } from 'src/app/_model/_enum/routes.enum';

const routes: Routes = [
  {
    path: '',
    component: ProductsGpComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsGpRoutingModule { }
