import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsGpComponent } from './products-gp.component';

describe('ProductsGpComponent', () => {
  let component: ProductsGpComponent;
  let fixture: ComponentFixture<ProductsGpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductsGpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsGpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
