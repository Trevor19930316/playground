export interface ICountry {
  id: number;
  // 國家名稱
  name: string;
}

export interface IAddress {
  id: number;
  // 國家id
  country_id: number;
  // 國家名稱
  country_name: string;
  // 城市
  city: string;
  // 區
  district: string;
  // 郵遞區號
  zip_code: string;
  // 緯度
  latitude: number;
  // 經度
  longitude: number;
};

export interface ICountryCode {
  // 國家id
  country_id: number;
  // 手機國碼
  country_code: string;
  // 國家名稱
  country_name: string;
}
