export interface IApiResponse {
  data: Array<any>;
  message: string;
  status: boolean;
  validation_error_message: Array<any>;
};
