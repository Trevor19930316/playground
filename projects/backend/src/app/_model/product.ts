export interface IProduct {
  id: number;
  // 編號
  no: string;
  // 名稱
  name: string;
  // 有效狀態
  valid_flag: boolean;
  created_at: string | null;
  updated_at: string | null;
};

export interface IProductItem {
  id: number;
  // 商品id
  product_id: number;
  // 編號
  no: string;
  // 名稱
  name: string;
  // 價格
  price: number;
  // GP
  gp: number;
  // 有效狀態
  valid_flag: boolean;
  created_at: string | null;
  updated_at: string | null;
};

export interface IProductItemGp {
  // 商品id
  id: number;
  // 商品編號
  no: string;
  // 商品名稱
  name: string;
  // 有效狀態
  valid_flag: boolean;
  created_at: string | null;
  updated_at: string | null;
  // 規格
  product_items: Array<{
    // 規格id
    id: number;
    // 規格編號
    no: string;
    // 規格名稱
    name: string;
    // 規格gp
    gp: number;
    // 有效狀態
    valid_flag: boolean;
    created_at: string | null;
    updated_at: string | null;
  }>;
};
