export enum GlobalRoutes {
  Login = 'login',
  Home = '',
}

// 管理員
export enum AdminRoutes {
  Admins = 'admins',
  Admins_Detail = 'admins/:adminId',
}

// 會員
export enum UserRoutes {
  Users = 'users',
  Users_Detail = 'users/:userId',
  User_Levels = 'user_levels',
  User_Levels_Detail = 'user_levels/:userLevelId',
}

// 商品
export enum ProductRoutes {
  Products = 'products',
  Products_Detail = ':productId',
  Product_Items = 'products/:productId/product_items',
  Product_Items_Detail = 'products/:productId/product_items/:productItemId',
  Products_Gp = 'products_gp',
}

// 前台設定
export enum FrontendRoutes {
  // 公佈欄
  Bulletin_Boards = 'bulletin_boards',
  // 授權書
  Authorization = 'authorization',
  // 會員權益
  Rights = 'rights',
  // 站台資料
  WebsiteInfo = 'website_info',
}

// 訂單
export enum OrderRoutes {
  Orders = 'orders',
}
