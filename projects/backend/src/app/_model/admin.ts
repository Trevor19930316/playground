export interface IAdmin {
  id: number;
  // 帳號
  account: string;
  // 名稱
  name: string;
  // 最高管理權限
  is_super: boolean;
  // 有效狀態
  valid_flag: boolean;
  created_at: string | null;
  updated_at: string | null;
};
