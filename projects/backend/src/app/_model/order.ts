export interface IOrder {
  id: number;
  // 編號
  no: string;
  // 金額
  price: number;
  created_at: string | null;
  updated_at: string | null;
};
