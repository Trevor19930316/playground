export interface IBulletinBoard {
  id: number;
  // 公佈欄 標題
  title: string;
  // 公佈欄 副標題
  subtitle: string;
  // 開始時間
  start_date: string;
  // 結束時間
  end_date: string;
  // 公佈欄 圖片
  image_url: string;
  // 公佈欄 內容
  content: string;
  // 排序
  sort: number;
  // 有效狀態
  valid_flag: boolean;
  created_at: string | null;
  updated_at: string | null;
};
