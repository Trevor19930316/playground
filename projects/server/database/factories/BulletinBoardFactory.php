<?php

namespace Database\Factories;

use App\Libraries\Definition\DataDefine;
use App\Models\BulletinBoard;
use Illuminate\Database\Eloquent\Factories\Factory;

class BulletinBoardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BulletinBoard::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {

        return [
            "title" => $this->faker->text(30),
            "subtitle" => $this->faker->text(30),
            "start_date" => date('Y-m-d 00:00:00', strtotime("now")),
            "end_date" => date('Y-m-d 00:00:00', strtotime("+30 day")),
            "image_url" => $this->faker->imageUrl(),
            "content" => $this->faker->text(),
            "valid_flag" => DataDefine::BOOLEAN_TRUE,
            "sort" => 0,
            'created_by' => 'System',
            'updated_by' => 'System',
        ];
    }
}
