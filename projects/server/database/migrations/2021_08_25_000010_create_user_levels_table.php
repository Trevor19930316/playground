<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_levels', function (Blueprint $table) {
            $table->id();
            $table->string('no', 30)->unique()->comment('代號');
            $table->string('name', 30)->comment('名稱');
            $table->integer('sort')->default(0)->index()->comment('排序');
            $table->boolean('valid_flag')->default(false)->index()->comment('有效狀態');
            $table->string('created_by', 255)->index();
            $table->string('updated_by', 255)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_levels');
    }
}
