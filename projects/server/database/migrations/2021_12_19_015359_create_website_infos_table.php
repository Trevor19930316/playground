<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsiteInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_infos', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30)->comment('網站(品牌)名稱');
            $table->string('website_logo_url')->comment('網站LOGO');
            $table->string('title', 50)->comment('網站標題');
            $table->string('title_logo_url')->comment('網站標題LOGO(頁籤LOGO)');
            $table->string('phone', 30)->comment('聯絡電話');
            $table->string('email', 100)->comment('聯絡信箱');
            $table->string('company_address', 100)->comment('公司地址');
            $table->string('line')->comment('LINE');
            $table->string('facebook')->comment('Facebook');
            $table->string('instagram')->comment('Instagram');
            $table->string('created_by', 255)->index();
            $table->string('updated_by', 255)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_infos');
    }
}
