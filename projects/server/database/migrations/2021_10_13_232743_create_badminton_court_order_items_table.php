<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBadmintonCourtOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badminton_court_order_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('badminton_court_order_id')
                ->comment('訂單id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('product_id')->index()->comment('商品id');
            $table->string('product_no')->index()->comment('商品編號');
            $table->string('product_name')->index()->comment('商品名稱');
            $table->unsignedBigInteger('product_item_id')->index()->comment('規格id');
            $table->string('product_item_no')->index()->comment('規格編號');
            $table->string('product_item_name')->index()->comment('規格名稱');
            $table->unsignedInteger('price')->index()->comment('明細單價');
            $table->unsignedInteger('quantity')->index()->comment('規格數量');
            $table->unsignedInteger('amount')->index()->comment('明細金額');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badminton_court_order_items');
    }
}
