<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefinitionAddressPostalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('definition_address_postal', function (Blueprint $table) {
            $table->id();
            $table->foreignId('definition_country_id')
                ->comment('國家id')
                ->constrained('definition_countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('zip_code', 50)->index()->comment('郵遞區號');
            $table->string('city', 50)->index()->comment('城市');
            $table->string('district', 50)->index()->comment('區');
            $table->float('latitude')->nullable()->index()->comment('緯度');
            $table->float('longitude')->nullable()->index()->comment('經度');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('definition_address_postal');
    }
}
