<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLevelRecordLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_level_record_logs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->comment('會員id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('old_user_level_id')->nullable()->index()->comment('原會員等級id');;
            $table->unsignedBigInteger('new_user_levels_id')->nullable()->index()->comment('新會員等級id');
            $table->string('created_by', 255)->index();
            $table->string('updated_by', 255)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_level_record_logs');
    }
}
