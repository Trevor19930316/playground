<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBadmintonCourtOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badminton_court_orders', function (Blueprint $table) {
            $table->id();
            $table->string('no')->unique()->comment('訂單編號');
            $table->unsignedBigInteger('user_id')->nullable()->index()->comment('會員id');
            $table->string('status')->index()->comment('訂單狀態');
            $table->boolean('is_pay')->index()->default(false)->comment('是否付款');
            $table->unsignedInteger('amount')->comment('訂單金額');
            $table->date('order_day')->index()->comment('訂購日期');
            $table->year('order_year')->index()->comment('訂購年分');
            $table->string('order_month')->index()->comment('訂購月分');
            $table->unsignedBigInteger('badminton_courts_id')->index()->comment('羽球場id');
            $table->unsignedBigInteger('badminton_court_charges_id')->index()->nullable()->comment('羽球場特別收費方式id');
            $table->unsignedBigInteger('order_invoice_id')->index()->nullable()->comment('訂單發票id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badminton_court_orders');
    }
}
