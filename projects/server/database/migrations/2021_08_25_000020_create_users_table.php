<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_level_id')
                ->comment('會員等級id')
                ->constrained('user_levels')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('account', 100)->index()->comment('帳號');
            $table->string('password')->comment('密碼');
            $table->string('name', 30)->comment('名稱');
            $table->string('phone_country', 10)->comment('手機國碼');
            $table->string('phone_number', 30)->comment('手機號碼');
            $table->string('email')->unique()->comment('電子信箱');
            $table->timestamp('email_verified_at')->nullable()->comment('電子信箱認證時間');
            $table->unsignedInteger('address_postal_id')->nullable()->comment('地址郵遞區號');
            $table->string('address')->nullable()->comment('地址');
            $table->boolean('valid_flag')->default(false)->index()->comment('有效狀態');
            $table->string('created_by', 255)->index();
            $table->string('updated_by', 255)->index();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
