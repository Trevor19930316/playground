<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefinitionCountriesCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('definition_countries_code', function (Blueprint $table) {
            $table->id();
            $table->foreignId('definition_country_id')
                ->comment('國家id')
                ->constrained('definition_countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('country_code', 20)->index()->comment('手機國碼');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('definition_countries_code');
    }
}
