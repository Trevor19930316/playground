<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBadmintonCourtChargesTable extends Migration
{
    /**
     * 羽球場特別收費方式
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badminton_court_charges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('badminton_courts_id')
                ->comment('羽球場id')
                ->constrained('badminton_courts')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->date('start_time')->comment('指定開始時間');
            $table->date('end_time')->comment('指定結束時間');
            $table->unsignedTinyInteger('charge_type')->comment('收費類型，離峰/尖峰/不限');
            $table->unsignedTinyInteger('time_type')->comment('計算類型，以每小時/以區段');
            $table->string('start_hour', 2)->comment('區段開始時間 - 小時');
            $table->string('end_hour', 2)->comment('區段結束時間 - 小時');
            $table->unsignedInteger('amount')->comment('收費金額');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badminton_court_charges');
    }
}
