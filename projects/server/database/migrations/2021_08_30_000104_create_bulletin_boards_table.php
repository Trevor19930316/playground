<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBulletinBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulletin_boards', function (Blueprint $table) {
            $table->id();
            $table->string("title", 100)->comment("公佈欄標題");
            $table->string("subtitle", 100)->nullable()->comment("公佈欄副標題");
            $table->dateTime("start_date")->nullable()->comment("開始時間");
            $table->dateTime("end_date")->nullable()->comment("結束時間");
            $table->string("image_url")->nullable()->comment("公佈欄圖片");
            $table->text("content")->comment("公佈欄文字內容");
            $table->boolean('valid_flag')->default(false)->index()->comment('有效狀態');
            $table->unsignedInteger('sort')->default(0)->index()->comment('排序');
            $table->string('created_by', 255)->index();
            $table->string('updated_by', 255)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulletin_boards');
    }
}
