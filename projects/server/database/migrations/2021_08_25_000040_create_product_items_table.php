<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')
                ->comment('商品id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('no')->unique()->comment('編號');
            $table->string('name', 100)->comment('名稱');
            $table->unsignedInteger('price')->comment('價格');
            $table->unsignedInteger('gp')->comment('GP');
            $table->boolean('valid_flag')->default(false)->comment('有效狀態');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_items');
    }
}
