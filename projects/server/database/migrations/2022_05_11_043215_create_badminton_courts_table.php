<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBadmintonCourtsTable extends Migration
{
    /**
     * 羽球場
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badminton_courts', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('名稱');
            $table->string('open_hour', 2)->comment('球場開放時間');
            $table->string('close_hour', 2)->comment('球場關閉時間');
            $table->string('start_rush_hour', 2)->comment('球場尖峰起始時間');
            $table->string('end_rush_hour', 2)->comment('球場尖峰結束時間');
            $table->unsignedInteger('peek_off_amount')->comment('離峰時間收費');
            $table->unsignedInteger('rush_hour_amount')->comment('尖峰時間收費');
            $table->unsignedTinyInteger('valid_flag')->index()->default(0)->comment('離峰時間收費');
            $table->string('created_by', 255)->index();
            $table->string('updated_by', 255)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badminton_courts');
    }
}
