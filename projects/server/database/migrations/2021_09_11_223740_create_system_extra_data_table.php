<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemExtraDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_extra_data', function (Blueprint $table) {
            $table->id();
            $table->string('type')->index()->comment('類型');
            $table->boolean('is_single')->default(false)->index()->comment('是否為單一資料');
            $table->string('title')->index()->comment('標題');
            $table->text('content')->comment('內容');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_extra_data');
    }
}
