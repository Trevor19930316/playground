<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiRecordLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_record_logs', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->comment(__('API 狀態'));
            $table->string('http_code', 30)->comment('API 回應 http code');
            $table->json('request_header')->nullable()->comment('API 請求 header');
            $table->json('request_content')->nullable()->comment('API 請求參數');
            $table->json('response_header')->nullable()->comment('API 回應 header');
            $table->json('response_content')->nullable()->comment('API 回應資料');
            $table->string('method', 10)->comment('API 呼叫方式');
            $table->string('api_url')->comment('API 網址');
            $table->string('route_name', 50)->comment('Route 名稱')->index();
            $table->ipAddress('request_ip')->comment('API 請求 ip')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_record_logs');
    }
}
