<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        Admin::insert([
            'account' => 'admin',
            'password' => password_hash(md5('111111'), PASSWORD_DEFAULT),
            'name' => config('system.administrator'),
            'valid_flag' => true,
            'is_super' => true,
            'created_by' => 'System',
            'updated_by' => 'System',
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
