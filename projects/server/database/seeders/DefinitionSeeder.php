<?php

namespace Database\Seeders;

use App\Models\DefinitionCountry;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefinitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        DB::table('definition_countries')->insert([
            'name' => '臺灣',
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table('definition_countries_code')->insert([
            'definition_country_id' => DefinitionCountry::getTaiwanId(),
            'country_code' => '886',
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
