<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        DB::table('user_levels')->insert([
            'no' => 'user',
            'name' => '一般會員',
            'sort' => 0,
            'valid_flag' => true,
            'created_by' => 'System',
            'updated_by' => 'System',
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('users')->insert([
            'user_level_id' => 1,
            'account' => 'user',
            'password' => password_hash(md5('111111'), PASSWORD_DEFAULT),
            'name' => config('system.administrator'),
            'phone_country' => 886,
            'phone_number' => '0975137790',
            'email' => 'abc@gmail.com',
            'address_postal_id' => 1,
            'address' => '測試地址',
            'valid_flag' => true,
            'created_by' => 'System',
            'updated_by' => 'System',
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
