<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            // 網站資訊
            WebsiteInfoSeeder::class,
            // 會員
            UserSeeder::class,
            // 管理者
            AdminSeeder::class,
            // 公佈欄
            BulletinBoardSeeder::class,
            // System Extra Data
            SystemExtraDataSeeder::class,
            // 定義資料
            DefinitionSeeder::class,
        ]);
    }
}
