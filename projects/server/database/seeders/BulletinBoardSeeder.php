<?php

namespace Database\Seeders;

use App\Models\BulletinBoard;
use Illuminate\Database\Seeder;

class BulletinBoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BulletinBoard::factory()->count(10)->create();
    }
}
