<?php

namespace Database\Seeders;

use App\Repositories\SystemExtraDataRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemExtraDataSeeder extends Seeder
{
    protected $repository;

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function run()
    {
        $this->repository = new SystemExtraDataRepository();

        $type_list = $this->repository->getTypeList();
        $now = now();
        foreach ($type_list as $type => $type_lang) {
            DB::table('system_extra_data')->insert([
                'type' => $type,
                'is_single' => true,
                'title' => $type_lang,
                'content' => $type_lang,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
