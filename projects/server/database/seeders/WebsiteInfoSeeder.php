<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WebsiteInfoSeeder extends Seeder
{
    protected $repository;

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function run()
    {
        $now = now();
        DB::table('website_infos')->insert([
            'name' => '',
            'website_logo_url' => '',
            'title' => '',
            'title_logo_url' => '',
            'phone' => '',
            'email' => '',
            'company_address' => '',
            'line' => '',
            'facebook' => '',
            'instagram' => '',
            'created_by' => 'System',
            'updated_by' => 'System',
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
