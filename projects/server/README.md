# initialize

### copy .env
```shell
cd projects/server
cp .env.local.example .env
```

### create cache folder
```shell
cd storage/
mkdir -p framework/{sessions,views,cache}
chmod -R 775 framework
```

# in docker

## gen laravel key
```text
php artisan key:generate
```

## initialize local data
```shell
bash bin/reset.sh
```
