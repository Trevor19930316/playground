#!/bin/bash
echo "Run Reset ? (Y/n)";
read runReset

if [ $runReset == "Y" ]; then
    echo "Run php artisan migrate:refresh"
    php artisan migrate:refresh

    echo "Run php artisan db:seed"
    php artisan db:seed

    echo "Run php artisan initialize:AddressPostalInitialize"
    php artisan initialize:AddressPostalInitialize

    echo "Run php artisan initialize:CacheInitialize"
    php artisan initialize:CacheInitialize
fi

