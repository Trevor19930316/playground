<?php

/* 後台相關 API */

use App\Http\Controllers\api\admin\AdminController;
use App\Http\Controllers\api\admin\AuthController;
use App\Http\Controllers\api\admin\BadmintonCourtOrderController;
use App\Http\Controllers\api\admin\BadmintonCourtOrderItemController;
use App\Http\Controllers\api\admin\BaseController;
use App\Http\Controllers\api\admin\BulletinBoardController;
use App\Http\Controllers\api\admin\CacheController;
use App\Http\Controllers\api\admin\DataExportController;
use App\Http\Controllers\api\admin\DataImportController;
use App\Http\Controllers\api\admin\FileImportLogController;
use App\Http\Controllers\api\admin\ProductController;
use App\Http\Controllers\api\admin\ProductItemController;
use App\Http\Controllers\api\admin\SystemExtraDataController;
use App\Http\Controllers\api\admin\SystemLogController;
use App\Http\Controllers\api\admin\UserController;
use App\Http\Controllers\api\admin\UserLevelController;
use App\Http\Controllers\api\admin\UserLevelRecordLogController;
use App\Http\Controllers\api\admin\WebsiteInfoController;
use Illuminate\Support\Facades\Route;

Route::group([

    //'domain' => config('app.api_url'),
    'middleware' => 'api',
    'prefix' => 'admin',

], function () {

    // phpinfo
    Route::get('phpinfo', [BaseController::class, 'getPhpinfo'])->name('api.admin.phpinfo');
    // test for developer
    Route::get('testForDeveloper', [BaseController::class, 'testForDeveloper'])->name('api.admin.testForDeveloper');

    // Auth
    Route::prefix('auth')->group(function () {
        // 登入
        Route::post('login', [AuthController::class, 'login'])->name('api.admin.auth.login');

        Route::middleware('jwt.verify')->group(function () {
            // 登出
            Route::post('logout', [AuthController::class, 'logout'])->name('api.admin.auth.logout');
            // Token 刷新
            Route::post('refresh', [AuthController::class, 'refresh'])->name('api.admin.auth.refresh');
            // 個人資料
            Route::get('user_profile', [AuthController::class, 'getUserProfile'])->name('api.admin.auth.getUserProfile');

        });
    });

    // admins api
    Route::middleware('jwt.verify')->group(function () {
        // 管理員
        Route::resource('admins', AdminController::class, ['as' => 'api.admin']);
        // 會員等級
        Route::resource('user_levels', UserLevelController::class, ['as' => 'api.admin']);
        // 會員等級異動紀錄
        Route::resource('user_level_record_logs', UserLevelRecordLogController::class, ['as' => 'api.admin']);
        // 會員
        Route::resource('users', UserController::class, ['as' => 'api.admin']);
        // 會員重置密碼
        Route::match(['put', 'patch'], 'users/resetPassword/{user}', [UserController::class, 'resetPassword'])
            ->name('api.admin.users.resetPassword');
        // 公佈欄
        Route::resource('bulletin_boards', BulletinBoardController::class, ['as' => 'api.admin']);
        // System Extra Data
        Route::resource('system_extra_data', SystemExtraDataController::class, ['as' => 'api.admin']);
        Route::post('system_extra_data/type_list', [SystemExtraDataController::class, 'getTypeList'])
            ->name('api.admin.system_extra_data.type_list');
        // 商品
        Route::resource('products', ProductController::class, ['as' => 'api.admin']);
        // 商品明細
        Route::resource('products.product_items', ProductItemController::class, ['as' => 'api.admin'])->shallow();
        // 訂單
        Route::resource('badminton_court_orders', BadmintonCourtOrderController::class, ['as' => 'api.admin']);
        // 訂單匯入
        Route::post('badminton_court_orders/import/{mode}', [BadmintonCourtOrderController::class, 'import'])->name('api.admin.badminton_court_orders.import');
        // 訂單匯入紀錄
        Route::get('badminton_court_orders/import/logs', [BadmintonCourtOrderController::class, 'import_logs'])->name('api.admin.badminton_court_orders.import.logs');
        // 訂單明細
        Route::resource('badminton_court_orders.badminton_court_order_items', BadmintonCourtOrderItemController::class, ['as' => 'api.admin'])->shallow();
        // 資料匯入
        Route::get('data_import', [DataImportController::class, 'index'])->name('api.admin.data_import.index');
        Route::post('data_import', [DataImportController::class, 'import'])->name('api.admin.data_import.import');
        // 資料匯出
        Route::post('data_export', [DataExportController::class, 'export'])->name('api.admin.data_import.export');
        // 網站基本資訊
        Route::get('website_info/{website_info}', [WebsiteInfoController::class, 'show'])->name('api.admin.website_info.show');
        Route::post('website_info/{website_info}', [WebsiteInfoController::class, 'update'])->name('api.admin.website_info.update');
        // 檔案匯入紀錄
        Route::resource('file_import_log', FileImportLogController::class, ['as' => 'api.admin']);
        // cache
        Route::get('cache/cacheCategoryList', [CacheController::class, 'getCacheCategoryList'])->name('api.admin.cache.cacheCategoryList');
        Route::get('cache/cacheKeyList', [CacheController::class, 'getCacheKeyList'])->name('api.admin.cache.cacheKeyList');
        Route::post('cache/singleCacheData', [CacheController::class, 'getSingleCacheKeyData'])->name('api.admin.cache.singleCacheData');
        Route::post('cache/multipleCacheData', [CacheController::class, 'getMultipleCacheKeyData'])->name('api.admin.cache.multipleCacheData');
        // system logs
        Route::get('system_log/index', [SystemLogController::class, 'index'])->name('api.admin.system_log.index');
        Route::post('system_log/show', [SystemLogController::class, 'show'])->name('api.admin.system_log.show');
    });

});
