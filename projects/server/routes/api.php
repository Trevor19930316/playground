<?php

use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\BadmintonCourtOrderController;
use App\Http\Controllers\api\BulletinBoardController;
use App\Http\Controllers\api\ProductController;
use App\Http\Controllers\api\ProductItemController;
use App\Http\Controllers\api\SystemExtraDataController;
use App\Http\Controllers\api\UserController;
use App\Http\Controllers\api\WebsiteInfoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* 前台相關 API */
Route::group([

    //'domain' => config('app.api_url'),
    'middleware' => 'api',

], function () {

    // 網站基本資訊
    Route::resource('website_info', WebsiteInfoController::class);

    // Auth
    Route::prefix('auth')->group(function () {
        // 登入
        Route::post('login', [AuthController::class, 'login'])->name('api.auth.login');
        // 註冊
        Route::post('register', [AuthController::class, 'register'])->name('api.auth.register');

        Route::middleware('jwt.verify')->group(function () {
            // 登出
            Route::post('logout', [AuthController::class, 'logout'])->name('api.auth.logout');
            // Token 刷新
            Route::post('refresh', [AuthController::class, 'refresh'])->name('api.auth.refresh');
            // 個人資料
            Route::get('user_profile', [AuthController::class, 'getUserProfile'])->name('api.auth.user_profile');
            // 修改個人資料
            Route::post('user_profile', [AuthController::class, 'updateUserProfile'])->name('api.auth.user_profile');
            // 變更密碼
            Route::post('change_password', [AuthController::class, 'changePassword'])->name('api.auth.change_password');
        });
    });

    // users api
    Route::middleware('jwt.verify')->group(function () {
        // 會員
        Route::prefix('users')->resource('users', UserController::class);
        // 公佈欄
        Route::resource('bulletin_boards', BulletinBoardController::class);
        // 授權書、權益書
        Route::resource('system_extra_data', SystemExtraDataController::class);
        Route::post('system_extra_data/type_list', [SystemExtraDataController::class, 'getTypeList'])
            ->name('api.system_extra_data.type_list');
        // 商品
        Route::resource('products', ProductController::class);
        // 商品明細
        Route::resource('products.product_items', ProductItemController::class)->shallow();
        // 訂單
        Route::get('badminton_court_orders', [BadmintonCourtOrderController::class, 'index'])->name('api.badminton_court_orders.index');
        // 訂單 - 明細
        Route::get('badminton_court_orders/{id}', [BadmintonCourtOrderController::class, 'show'])->name('api.badminton_court_orders.show');
        // 訂單 - 訂單規格
        Route::get('badminton_court_orders/{id}/badminton_court_order_items', [BadmintonCourtOrderController::class, 'badminton_court_order_items'])->name('api.badminton_court_orders.badminton_court_order_items');
    });
});

/* 後台相關 API */
require_once 'api_admin.php';

/* Definition API */
require_once 'api_definition.php';
