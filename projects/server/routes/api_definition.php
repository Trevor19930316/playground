<?php

/* Definition 相關 API */

use App\Http\Controllers\api\definition\DefinitionController;
use Illuminate\Support\Facades\Route;

Route::group([

    //'domain' => config('app.api_url'),
    'middleware' => 'api',
    'prefix' => 'definition',

], function () {

    // 國家
    Route::get('getCountries', [DefinitionController::class, 'getCountries'])
        ->name('api.definition.getCountries');

    // 地址
    Route::get('getAddress', [DefinitionController::class, 'getAddress'])
        ->name('api.definition.getAddress');

    // 手機國碼
    Route::get('getCountriesCode', [DefinitionController::class, 'getCountriesCode'])
        ->name('api.definition.getCountriesCode');
});
