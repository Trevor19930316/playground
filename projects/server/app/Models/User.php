<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_level_id',
        'account',
        'password',
        'name',
        'phone_country',
        'phone_number',
        'email',
        'address_postal_id',
        'address',
        'valid_flag',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @inheritDoc
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * belongs to UserLevel
     *
     * @return BelongsTo
     */
    public function userLevel(): BelongsTo
    {
        return $this->belongsTo(UserLevel::class);
    }

    /**
     * has many through UserLevel
     *
     * @return HasManyThrough
     */
    public function userLevelBonuses(): HasManyThrough
    {
        return $this->hasManyThrough(UserLevelBonus::class, UserLevel::class, 'id', 'user_level_id', 'id', 'id');
    }

    /**
     * has many UserSaleOrderLog
     *
     * @return HasMany
     */
    public function userSaleOrderLogs(): HasMany
    {
        return $this->hasMany(UserSaleOrderLog::class, 'user_id', 'id');
    }
}
