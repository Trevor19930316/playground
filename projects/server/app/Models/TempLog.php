<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TempLog extends Model
{
    use HasFactory;

    protected $table = 'temp_logs';

    protected $fillable = [
        'title',
        'content',
        'description',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    /**
     * 建立 temp log
     * @param string $title
     * @param string $content
     * @param string $description
     */
    public static function createTempLog($title = '', $content = '', $description = '')
    {
        self::create([
            'title' => $title,
            'content' => $content,
            'description' => $description,
        ]);
    }
}
