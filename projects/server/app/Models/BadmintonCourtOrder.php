<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BadmintonCourtOrder extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no',
        'user_id',
        'status',
        'is_pay',
        'amount',
        'order_day',
        'order_year',
        'order_month',
        'badminton_courts_id',
        'badminton_court_charges_id',
        'order_invoice_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    /**
     * has many OrderItem
     *
     * @return HasMany
     */
    public function badmintonCourtOrderItems(): HasMany
    {
        return $this->hasMany(BadmintonCourtOrderItem::class);
    }
}
