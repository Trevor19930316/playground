<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DefinitionAddressPostal extends Model
{
    use HasFactory;

    protected $table = 'definition_address_postal';

    protected $fillable = [
        'definition_country_id',
        'zip_code',
        'city',
        'district',
        'latitude',
        'longitude',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    /**
     * belongs to
     *
     * @return BelongsTo
     */
    public function definition_country()
    {
        return $this->belongsTo(DefinitionCountry::class);
    }
}
