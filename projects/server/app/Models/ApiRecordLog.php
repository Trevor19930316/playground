<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiRecordLog extends Model
{
    use HasFactory;

    protected $table = 'api_record_logs';

    protected $fillable = [
        'status',
        'http_code',
        'request_header',
        'request_content',
        'response_header',
        'response_content',
        'method',
        'api_url',
        'route_name',
        'request_ip',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    // TODO 研究 Repository Pattern
    // 把 function 拆出來的定義

    /**
     * 建立 API 呼叫 log
     * @param Response $response
     */
    public static function createApiRecordLog(Response $response)
    {
        $http_code = $response->getStatusCode();
        // 200, 201, 202 判定為成功
        $http_code_list = [Response::HTTP_OK, Response::HTTP_CREATED, Response::HTTP_ACCEPTED];

        $status = true;
        if (!in_array($http_code, $http_code_list)) {
            $status = false;
        }

        $request_header = is_array(request()->headers->all()) ? json_encode(request()->headers->all()) : "{}";
        $request_content = is_array(request()->all()) ? json_encode(request()->all()) : "{}";
        $response_header = is_array($response->headers->all()) ? json_encode($response->headers->all()) : "{}";
        $response_content = is_array($response->getContent()) ? json_encode($response->getContent()) : "{}";

        // TODO 檢查 header 為空時記 error log
        if ($request_header === "{}" || $response_header === "{}") {
        }

        self::create([
            'status' => $status,
            'http_code' => $http_code,
            'request_header' => $request_header,
            'request_content' => $request_content,
            'response_header' => $response_header,
            'response_content' => $response_content,
            'method' => request()->getMethod(),
            'api_url' => request()->fullUrl(),
            'route_name' => request()->route()->getName(),
            'request_ip' => request()->ip(),
        ]);
    }
}
