<?php

namespace App\Models;

use App\Libraries\Cache\DefinitionCache;
use App\Libraries\Cache\SystemCache;
use App\Libraries\Definition\CacheDefine;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class DefinitionCountry extends Model
{
    use HasFactory;

    protected $table = 'definition_countries';

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    /**
     * has many
     *
     * @return HasMany
     */
    public function definitionCountriesCode()
    {
        return $this->hasMany(DefinitionCountryCode::class);
    }

    /**
     * has many
     *
     * @return HasMany
     */
    public function definitionAddressPostal()
    {
        return $this->hasMany(DefinitionAddressPostal::class);
    }

    /**
     * 取得 國家資料陣列
     * @param string $key
     * @param string $value
     * @return mixed
     */
    public static function getCountriesArray($key = 'id', $value = 'name') {
        $cacheKey = CacheDefine::CACHE_CATEGORY_DEFINITION . CacheDefine::SEPARATOR . CacheDefine::CACHE_KEY_DEFINITION_COUNTRY;
        $countries_data = Redis::get($cacheKey);
        $countries_data = collect(json_decode($countries_data, true))->keyBy('id');
        return $countries_data;
    }

    /**
     * 取得 Taiwan id
     *
     * @return mixed
     */
    public static function getTaiwanId() {
        return self::select('id')->where('name', '=', '臺灣')->first()->id;
    }
}
