<?php

namespace App\Jobs;

use App\Imports\OrderImport;
use App\Imports\ProductImport;
use App\Libraries\Definition\FileImportDefine;
use App\Libraries\Definition\imports\OrderImportDefine;
use App\Libraries\Definition\imports\ProductImportDefine;
use App\Libraries\Handler\FileHandler;
use App\Models\FileUploadLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Util\Exception;
use Throwable;

class FileImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600;
    // 檔案上傳表格 id
    protected $uploadId;
    // 匯入檔案類型
    protected $importType;
    // 匯入模式
    protected $importMode;
    // 匯入檔案
    protected $importFile;

    /**
     * Create a new job instance.
     *
     * @param int $uploadId
     * @param string $importType
     * @param string $importMode
     */
    public function __construct(int $uploadId, string $importType, string $importMode)
    {
        $this->uploadId = $uploadId;
        $this->importType = $importType;
        $this->importMode = $importMode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $uploadData = FileUploadLog::find($this->uploadId);
        if (is_null($uploadData)) {
            throw new Exception("Upload data is null.");
        }

        if (!in_array($this->importType, FileImportDefine::IMPORT_TYPE_LIST)) {
            throw new Exception("Import type is invalid.");
        }

        if (!in_array($this->importMode, FileImportDefine::IMPORT_MODE_LIST)) {
            throw new Exception("Import type is invalid.");
        }

        // 讓 handler 處理
        // 包 try catch

        // 呼叫匯入 Job
        if ($this->importType === FileImportDefine::IMPORT_TYPE_ORDER) {
            $importColumns = OrderImportDefine::IMPORT_RULE[$this->importMode];
            Excel::import(new OrderImport($this->uploadId, $this->importType, $this->importMode, $importColumns), FileHandler::getStorageFilePath($uploadData->path));
        } elseif ($this->importType === FileImportDefine::IMPORT_TYPE_PRODUCT) {
            $importColumns = ProductImportDefine::IMPORT_RULE[$this->importMode];
            Excel::import(new ProductImport($this->uploadId, $this->importType, $this->importMode, $importColumns), FileHandler::getStorageFilePath($uploadData->path));
        } elseif ($this->importType === FileImportDefine::IMPORT_TYPE_USER) {
            // TODO: do something
        }

    }

    public function failed(Throwable $exception)
    {

    }
}
