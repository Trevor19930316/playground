<?php

namespace App\Imports;

use App\Libraries\Cache\OrderCache;
use App\Models\Order;
use App\Models\BadmintonCourtOrderItem;
use App\Models\UserSaleOrderLog;
use App\Repositories\UserLevelRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class OrderImport extends BaseImport
{
    /**
     * @inheritDoc
     */
    public function collection(Collection $collection)
    {
        parent::collection($collection);

        // TODO 啟動事務
        $totalOrders = [];
        $importOrders = [];
        $importOrderItems = [];

        // 會員資訊(計算獎金用)
        $users = [];

        if ($this->importMode == 'create') {

            foreach ($collection as $row) {

                // 必填欄位處理
                $orderNo = $row[$this->importColumns['order_no']];
                $orderName = $row[$this->importColumns['order_name']];
                $mobile = $row[$this->importColumns['mobile']];
                $productItemNo = $row[$this->importColumns['product_item_no']];
                $orderAmount = $row[$this->importColumns['order_amount']];
                $productItemPrice = $row[$this->importColumns['product_item_price']];
                $productItemQuantity = $row[$this->importColumns['product_item_quantity']];
                $productItemAmount = $row[$this->importColumns['product_item_amount']];
                $orderStatus = $row[$this->importColumns['order_status']];

                // 選填欄位判斷 & 預設值
                $userAccount = $row[$this->importColumns['user_account']];

                $orderDay = self::getOptionDefaultValue('order_day');
                if (isset($this->importColumns['order_day'])) {
                    $orderDay = $row[$this->importColumns['order_day']];
                }
                $orderDate = self::getOptionDefaultValue('order_date');
                if (isset($this->importColumns['order_date'])) {
                    $orderDate = $row[$this->importColumns['order_date']];
                }

                // TODO 找一下判斷沒有下一筆資料的處理方式
                if (is_null($orderNo)) {
                    break;
                }

                if (!in_array($orderNo, $totalOrders)) {
                    $totalOrders[] = $orderNo;
                }

                /*
                 * STEP 01. 檢查訂單資料正確性
                 */
                // 檢查訂單是否已存在
                if (self::checkOrderExists($orderNo)) {
                    self::setImportFailedLog($orderNo, '訂單已存在!');
                    continue;
                }
                // 檢查商品規格是否存在
                $productItem = self::getProductItemByNo($productItemNo);
                if (is_null($productItem)) {
                    self::setImportFailedLog($orderNo, '商品規格不存在!');
                    continue;
                }
                $productItem = json_decode($productItem, true);
                // 檢查會員是否存在
                $user = null;
                if (!is_null($userAccount)) {
                    $user = self::getUserByAccount($userAccount);
                    if (is_null($user)) {
                        self::setImportFailedLog($orderNo, '會員不存在!');
                        continue;
                    }
                    $user = json_decode($user, true);

                    $users[$user['id']] = $user;

                    // 檢查會員是否有效
                    if (!$user['valid_flag']) {
                        self::setImportFailedLog($orderNo, '會員帳號無效!');
                        continue;
                    }
                }
                // 檢查訂單狀態
                $orderStatus = self::checkOrderStatusExists($orderStatus);
                if (!$orderStatus) {
                    self::setImportFailedLog($orderNo, '訂單狀態不存在!');
                    continue;
                }

                /*
                 * STEP 02. 組成訂單與明細資料
                 */
                // 訂單資料組成
                if (!array_key_exists($orderNo, $importOrders)) {
                    $importOrders[$orderNo] = [
                        'no' => $orderNo,
                        'user_id' => !is_null($user) ? $user['id'] : null,
                        'amount' => $orderAmount,
                        'gp' => 0,
                        'order_day' => $orderDay,
                        'order_year' => Carbon::parse($orderDay)->format('Y'),
                        'order_month' => Carbon::parse($orderDay)->format('m'),
                        'status' => $orderStatus,
                        'order_invoice_id' => 0,
                        'user_billing_id' => null,
                    ];
                }

                // 訂單明細資料組成
                $importOrderItems[$orderNo][] = [
                    'order_id' => null,
                    'product_id' => $productItem['product_id'],
                    'product_no' => $productItem['product']['no'],
                    'product_name' => $productItem['product']['name'],
                    'product_item_id' => $productItem['id'],
                    'product_item_no' => $productItem['no'],
                    'product_item_name' => $productItem['name'],
                    'product_item_gp' => $productItem['gp'],
                    'price' => $productItemPrice,
                    'quantity' => $productItemQuantity,
                    'amount' => $productItemAmount,
                    'gp' => $productItem['gp'] * $productItemQuantity,
                ];
            }

        } elseif ($this->importMode == 'update') {
            // TODO:update
        }
        //dd($importOrders);
        //dd($importOrderItems);

        /*
         * STEP 03. 逐一建立訂單及明細 & 會員銷售紀錄
         */
        if (!empty($importOrders)) {

            if ($this->importMode == 'create') {

                foreach ($importOrders as $orderNo => $orderData) {

                    $order = Order::create($orderData);
                    $gp = 0;

                    foreach ($importOrderItems[$orderNo] as $orderItemsData) {
                        $orderItemsData['order_id'] = $order->id;
                        $gp += $orderItemsData['gp'];
                        BadmintonCourtOrderItem::create($orderItemsData);
                    }

                    $order->update([
                        'gp' => $gp,
                    ]);

                    OrderCache::cacheOrderById($order->id);
                    OrderCache::cacheOrderByNo($order->no);

                    // TODO: 計算會員銷售紀錄 (目前只有自身抽成)
                    if (!is_null($order['user_id'])) {

                        // STEP 01. 會員等級
                        $user_level_id = $users[$order['user_id']]['user_level_id'];

                        $UserLevelRepository = new UserLevelRepository();
                        $userLevel = $UserLevelRepository->withUserLevelBonuses()->where('id', $user_level_id)->first();

                        // 抽成類型
                        $bonus_type = $userLevel->bonus_type;
                        // 可抽成階數
                        $bonus_level = $userLevel->bonus_level;

                        // STEP 02. 會員抽成 - 自身抽成

                        $bonus_user_id = $order['user_id'];
                        $bonus_user_valid_flag = $users[$order['user_id']]['valid_flag'];
                        $bonus_from = 0;

                        // 會員等級抽成
                        $userLevelBonus = $userLevel->userLevelBonuses->where('bonus_from', $bonus_from)->first();
                        $bonus_percent = $userLevelBonus->bonus_percent;

                        // STEP 03. 建立 user_sale_order_logs

                        $bonus = 0;

                        if ($bonus_type == 'order_gp') {
                            $bonus = round($order['gp'] * $bonus_percent);
                        } else if ($bonus_type == 'order_amount') {
                            $bonus = round($order['amount'] * $bonus_percent);
                        }

                        $UserSaleOrderLogData = [
                            'user_id' => $order['user_id'],
                            'order_id' => $order['id'],
                            'sale_amount' => $order['amount'],
                            'sale_gp' => $order['gp'],
                            'bonus_user_id' => $bonus_user_id,
                            'bonus_user_valid_flag' => $bonus_user_valid_flag,
                            'bonus_from' => $bonus_from,
                            'bonus_percent' => $bonus_percent,
                            'bonus_type' => $bonus_type,
                            'bonus' => $bonus,
                        ];
                        UserSaleOrderLog::create($UserSaleOrderLogData);
                    }

                    $this->importSuccessOrders[$orderNo] = '訂單匯入成功!';
                }

            } elseif ($this->importMode == 'update') {
                // TODO:update
            }

            $this->importStatus = true;
            $this->successCount = count($importOrders);
        }

        $this->totalCount = count($totalOrders);

        self::writeImportLog();
    }
}
