<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\ProductItem;
use Illuminate\Support\Collection;

class ProductImport extends BaseImport
{
    /**
     * @inheritDoc
     */
    public function collection(Collection $collection)
    {
        parent::collection($collection);

        // TODO 啟動事務
        $totalProducts = [];
        $importProducts = [];
        $importProductItems = [];

        // 選填欄位預設值
        $productValidFlag = self::getOptionDefaultValue('product_valid_flag');
        $productItemValidFlag = self::getOptionDefaultValue('product_item_valid_flag');

        if ($this->importMode == 'create') {

            foreach ($collection as $row)
            {
                // 必填欄位處理
                $productNo = $row[$this->importColumns['product_no']];
                $productName = $row[$this->importColumns['product_name']];
                $productItemNo = $row[$this->importColumns['product_item_no']];
                $productItemName = $row[$this->importColumns['product_item_name']];
                $price = $row[$this->importColumns['price']];
                $gp = $row[$this->importColumns['gp']];

                if (is_null($productNo)) {
                    break;
                }

                // 選填欄位判斷
                if (isset($this->importColumns['product_valid_flag'])) {
                    $productValidFlag = $row[$this->importColumns['product_valid_flag']];
                }
                if (isset($this->importColumns['product_item_valid_flag'])) {
                    $productItemValidFlag = $row[$this->importColumns['product_item_valid_flag']];
                }

                if (!in_array($productNo, $totalProducts)) {
                    $totalProducts[] = $productNo;
                }

                /*
                 * STEP 01. 檢查資料正確性
                 */
                if (!is_null(self::getProductByNo($productNo))) {
                    self::setImportFailedLog($productNo, '商品已存在!');
                    continue;
                }
                if (!is_null(self::getProductItemByNo($productItemNo))) {
                    self::setImportFailedLog($productItemNo, '規格已存在!');
                    continue;
                }

                /*
                 * STEP 02. 組成商品與明細資料
                 */
                // 商品資料組成
                if (!array_key_exists($productNo, $importProducts)) {
                    $importProducts[$productNo] = [
                        'no' => $productNo,
                        'name' => $productName,
                        'valid_flag' => $productValidFlag,
                    ];
                    $importProductItems[$productNo] = [];
                }
                // 商品明細資料組成
                if (!array_key_exists($productItemNo, $importProductItems[$productNo])) {
                    $importProductItems[$productNo][$productItemNo] = [
                        'product_id' => null,
                        'no' => $productItemNo,
                        'name' => $productItemName,
                        'price' => $price,
                        'gp' => $gp,
                        'valid_flag' => $productItemValidFlag,
                    ];
                }
            }

        } elseif ($this->importMode == 'update') {

            foreach ($collection as $row)
            {
                // 必填欄位處理
                // 商品編號
                $productNo = $row[$this->importColumns['product_no']];
                $productName = $row[$this->importColumns['product_name']];
                $productItemNo = $row[$this->importColumns['product_item_no']];
                $productItemName = $row[$this->importColumns['product_item_name']];
                $price = $row[$this->importColumns['price']];
                $gp = $row[$this->importColumns['gp']];

                if (is_null($productNo)) {
                    break;
                }

                // 選填欄位判斷
                if (isset($this->importColumns['product_valid_flag'])) {
                    $productValidFlag = $row[$this->importColumns['product_valid_flag']];
                }
                if (isset($this->importColumns['product_item_valid_flag'])) {
                    $productItemValidFlag = $row[$this->importColumns['product_item_valid_flag']];
                }

                if (!in_array($productNo, $totalProducts)) {
                    $totalProducts[] = $productNo;
                }

                /*
                 * STEP 01. 檢查資料正確性
                 */
                $product = self::getProductByNo($productNo);
                if (is_null($product)) {
                    self::setImportFailedLog($productNo, '商品不存在!');
                    continue;
                }
                $product = json_decode($product);
                $productItem = self::getProductItemByNo($productItemNo);
                if (is_null($productItem)) {
                    self::setImportFailedLog($productItemNo, '規格不存在!');
                    continue;
                }
                $productItem = json_decode($productItem);

                /*
                 * STEP 02. 組成商品與明細資料
                 */
                // 商品資料組成
                if (!array_key_exists($product->id, $importProducts)) {
                    $importProducts[$product->id] = [
                        'no' => $productNo,
                        'name' => $productName,
                        'valid_flag' => $productValidFlag,
                    ];
                    $importProductItems[$product->id] = [];
                }
                // 商品明細資料組成
                if (!array_key_exists($productItem->id, $importProductItems[$product->id])) {
                    $importProductItems[$product->id][$productItem->id] = [
                        'no' => $productItemNo,
                        'name' => $productItemName,
                        'price' => $price,
                        'gp' => $gp,
                        'valid_flag' => $productItemValidFlag,
                    ];
                }
            }
        }

        //dd($importProducts);
        //dd($importProductItems);

        /*
         * STEP 03. 逐一建立/更新商品及明細
         */
        if (!empty($importProducts)) {

            if ($this->importMode == 'create') {

                foreach ($importProducts as $productNo => $productData) {

                    $product = Product::create($productData);

                    foreach ($importProductItems[$productNo] as $productItemData) {
                        $productItemData['product_id'] = $product->id;
                        ProductItem::create($productItemData);
                    }

                    $this->importSuccessOrders[$productNo] = '商品匯入新增成功!';
                }

            } elseif ($this->importMode == 'update') {

                foreach ($importProducts as $productId => $productData) {

                    Product::where('id', '=', $productId)->update($productData);

                    foreach ($importProductItems[$productId] as $productItemId => $productItemData) {
                        ProductItem::where('id', '=', $productItemId)->update($productItemData);
                    }

                    $this->importSuccessOrders[$productData['no']] = '商品匯入更新成功!';
                }
            }

            $this->importStatus = true;
            $this->successCount = count($importProducts);
        }


        $this->totalCount = count($totalProducts);

        self::writeImportLog();
    }
}
