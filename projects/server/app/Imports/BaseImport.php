<?php

namespace App\Imports;

use App\Libraries\Cache\OrderCache;
use App\Libraries\Cache\ProductCache;
use App\Libraries\Cache\ProductItemCache;
use App\Libraries\Cache\UserCache;
use App\Libraries\Definition\FileImportDefine;
use App\Libraries\Definition\status\orders\StatusDefine;
use App\Models\FileImportLog;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use PHPUnit\Util\Exception;

abstract class BaseImport implements ToCollection
{
    // 上傳檔案 id
    protected int $uploadId;
    // 匯入類型
    protected string $importType;
    // 匯入模式
    protected string $importMode;
    // 已定義匯入欄位
    protected array $defineImportColumns = [];
    // 匯入必填欄位
    protected array $defineImportRequiredColumns = [];
    // 匯入選填欄位
    protected array $defineImportOptionalColumns = [];
    // 所有匯入欄位
    protected array $importColumns = [];
    // 匯入狀態
    protected bool $importStatus = false;
    // 匯入紀錄內容(含成功與失敗)
    protected string $content = '';
    // 匯入總筆數
    protected int $totalCount = 0;
    // 匯入成功筆數
    protected int $successCount = 0;
    // 匯入失敗筆數
    protected int $failureCount = 0;

    protected array $importSuccessOrders = [];
    protected array $importFailOrders = [];

    public function __construct(int $upload_id, string $importType, string $importMode, array $defineImportColumns)
    {
        $this->uploadId = $upload_id;
        $this->importType = $importType;
        $this->importMode = $importMode;
        $this->defineImportColumns = $defineImportColumns;

        // 匯入類型有誤
        if (!in_array($this->importType, FileImportDefine::IMPORT_TYPE_LIST)) {
            throw new Exception("Import type ($this->importType) is invalid.");
        }

        // 匯入欄位為空
        if (empty($this->defineImportColumns)) {
            throw new Exception("Define import columns are empty.");
        }

        $this->setImportColumns();
    }

    /**
     * 設定 匯入欄位
     */
    protected function setImportColumns(): void
    {
        foreach ($this->defineImportColumns as $columnName => $columnExtraData) {

            $required = $columnExtraData['required'];

            if ($required) {
                // 必填欄位
                $this->defineImportRequiredColumns[$columnName] = $columnExtraData;
            } else {
                // 非必填欄位
                $this->defineImportOptionalColumns[$columnName] = $columnExtraData;
            }
        }
    }

    /**
     * Check import columns format
     *
     * @param array $importColumnsName
     */
    private function checkImportColumnsFormat(array $importColumnsName): void
    {
        // 檢查欄位對應是否存在
        $isNotExistsColumns = [];
        $defineImportColumns = array_keys($this->defineImportColumns);
        foreach ($importColumnsName as $columnName) {
            if (!in_array($columnName, $defineImportColumns, true)) {
                $isNotExistsColumns[] = $columnName;
            }
        }
        if (!empty($isNotExistsColumns)) {
            throw new Exception('Import columns sort is not match. Please check these [' . implode(',', $isNotExistsColumns) . ']');
        }

        // 檢查必填欄位是否完整
        $defineImportColumnsNames = array_keys($this->defineImportRequiredColumns);
        $checkDiff = array_diff($defineImportColumnsNames, $importColumnsName);
        if (count($checkDiff) > 0) {
            throw new Exception('Import columns required count not match. Please check these [' . implode(',', $checkDiff) . "]");
        }

        // key & value change
        $this->importColumns = array_flip($importColumnsName);
    }

    /**
     * Get optional column default value
     *
     * @param $columnName
     * @return mixed
     */
    protected function getOptionDefaultValue($columnName)
    {
        if (!isset($this->defineImportOptionalColumns[$columnName])) {
            throw new Exception("Optional column [$columnName] is not match.");
        }

        $dataType = $this->defineImportOptionalColumns[$columnName]['type'];
        $defaultValue = $this->defineImportOptionalColumns[$columnName]["default"];

        // 時間格式額外處理
        if ($dataType === 'time') {
            return self::getTimeDefaultValue($defaultValue);
        }

        return $defaultValue;
    }

    /**
     * Get default value when type is time
     *
     * @param $defaultValue
     * @return false|string
     */
    private static function getTimeDefaultValue($defaultValue)
    {
        return date($defaultValue);
    }

    /**
     * check order exists
     *
     * @param string $orderNo
     * @return bool
     */
    protected function checkOrderExists(string $orderNo): bool
    {
        return !is_null(OrderCache::getOrderByNo($orderNo));
    }

    /**
     * check order status define exists
     *
     * @param string $orderStatus
     * @return false|string
     */
    protected function checkOrderStatusExists(string $orderStatus)
    {
        // excel 訂單狀態
        $OrderStatusList = [
            '取消' => StatusDefine::CANCEL,
            '完成' => StatusDefine::FINISH,
        ];

        if (
            isset($OrderStatusList[$orderStatus]) &&
            in_array($OrderStatusList[$orderStatus], StatusDefine::ORDER_STATUS_LIST, true)
        ) {
            return $OrderStatusList[$orderStatus];
        }

        return false;
    }

    /**
     * get order by no
     *
     * @param string $orderNo
     * @return bool
     */
    protected function getOrderByNo(string $orderNo): bool
    {
        return OrderCache::getOrderByNo($orderNo);
    }

    /**
     * get product by no
     *
     * @param string $productNo
     * @return mixed
     */
    protected function getProductByNo(string $productNo)
    {
        return ProductCache::getProductByNo($productNo);
    }

    /**
     * get product item by no
     *
     * @param string $productItemNo
     * @return mixed
     */
    protected function getProductItemByNo(string $productItemNo)
    {
        return ProductItemCache::getProductItemByNo($productItemNo);
    }

    /**
     * get user by account
     *
     * @param string $account
     * @return mixed
     */
    protected function getUserByAccount(string $account)
    {
        return UserCache::getUserByAccount($account);
    }

    /**
     * 設定匯入錯誤訊息
     *
     * @param $key
     * @param $failMessage
     */
    protected function setImportFailedLog($key, $failMessage): void
    {
        if (!array_key_exists($key, $this->importFailOrders)) {
            $this->importFailOrders[$key][] = $failMessage;
        }
    }

    /**
     * 寫入匯入紀錄
     */
    protected function writeImportLog(): void
    {
        FileImportLog::create([
            'file_upload_logs_id' => $this->uploadId,
            'import_type' => $this->importType,
            'status' => $this->importStatus,
            'content' => json_encode($this->importSuccessOrders + $this->importFailOrders, JSON_THROW_ON_ERROR),
            'total_count' => $this->totalCount,
            'success_count' => $this->successCount,
            'fail_count' => $this->totalCount - $this->successCount,
        ]);
    }

    public function collection(Collection $collection)
    {
        // 匯入欄位標題
        $importColumnsTitle = $collection->get(0);
        // 匯入欄位名稱
        $importColumnsName = $collection->get(1)->toArray();

        // 移除前兩行 (欄位標題&欄位名稱)
        $collection->forget([0, 1]);

        // 檢查欄位
        $this->checkImportColumnsFormat($importColumnsName);

        // child do something
    }
}
