<?php


namespace App\Libraries\Definition;


abstract class ErrorMessageDefine
{
    // Handler
    public const FILE_PUT_FAILED = 'put_file_failed';                                  // 儲存檔案失敗
    public const IMPORT_TYPE_IS_NOT_LEGAL = 'import_type_is_not_legal';                // 匯入類型不合法
    public const FILE_EXTENSION_IS_NOT_LEGAL = 'file_extension_is_not_legal';          // 檔案副檔名不合法
    public const FILE_FOLDER_IS_EMPTY = 'file_folder_is_empty';                        // 檔案資料夾不得為空
    public const MODEL_NOT_EXISTS = 'model_not_exists';                                // Model 不存在
    public const IMAGE_VALIDATE_FAILED = 'image_validate_failed';                      // 圖片驗證失敗
}
