<?php

namespace App\Libraries\Definition\status\orders;

abstract class StatusDefine
{
    // badminton_court_orders.status
    public const BOOKED = 'booked';             // 已預定
    public const CANCELLED = 'cancelled';       // 已取消
    public const PAID = 'paid';                 // 已付款
    public const REFUND = 'refund';             // 待退款
    public const REFUNDED = 'refunded';         // 已退款
    public const FINISHED = 'finished';         // 已完成

    public const ORDER_STATUS_LIST = [
        self::BOOKED,
        self::CANCELLED,
        self::PAID,
        self::REFUND,
        self::REFUNDED,
        self::FINISHED,
    ];
}
