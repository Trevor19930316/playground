<?php


namespace App\Libraries\Definition;


class FileImportDefine
{
    // 檔案匯入副檔名清單
    public const IMPORT_FILE_EXTENSION_LIST = [
        'xlsx',
        'xls',
        'csv'
    ];

    // 檔案匯入類型
    public const IMPORT_TYPE_ORDER = 'order';
    public const IMPORT_TYPE_PRODUCT = 'product';
    public const IMPORT_TYPE_USER = 'user';

    // 檔案匯入類型清單
    public const IMPORT_TYPE_LIST = [
      self::IMPORT_TYPE_ORDER,
      self::IMPORT_TYPE_PRODUCT,
      self::IMPORT_TYPE_USER,
    ];

    // 檔案匯入模式
    public const IMPORT_MODE_CREATE = 'create';
    public const IMPORT_MODE_UPDATE = 'update';

    // 檔案匯入模式清單
    public const IMPORT_MODE_LIST = [
        self::IMPORT_MODE_CREATE,
        self::IMPORT_MODE_UPDATE,
    ];

    // 可支援匯入
    public const IMPORT_ALLOW_LIST = [
        self::IMPORT_TYPE_ORDER => [
            self::IMPORT_MODE_CREATE
        ],
        self::IMPORT_TYPE_PRODUCT => [
            self::IMPORT_MODE_CREATE,
            self::IMPORT_MODE_UPDATE,
        ],
        self::IMPORT_TYPE_USER => [
            self::IMPORT_MODE_CREATE,
            self::IMPORT_MODE_UPDATE,
        ],
    ];
}
