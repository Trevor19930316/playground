<?php


namespace App\Libraries\Definition\imports;


use App\Libraries\Definition\DataDefine;

class OrderImportDefine
{
    // 匯入規則
    public const IMPORT_RULE = [
        // 匯入新增
        'create' => [
            // 欄位名稱
            'order_no' => [
                // 類型 string, int, boolean
                'type' => 'string',
                // 是否為必要
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'user_account' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_FALSE,
            ],
            'order_name' => [
                'type' => 'boolean',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'mobile' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_no' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'order_amount' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_price' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_quantity' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_amount' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'order_status' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'order_day' => [
                'type' => 'time',
                'required' => DataDefine::BOOLEAN_FALSE,
                'default' => 'Y-m-d',
            ],
            'order_date' => [
                'type' => 'time',
                'required' => DataDefine::BOOLEAN_FALSE,
                'default' => 'Y-m-d H:i:s',
            ],
        ],
    ];
}
