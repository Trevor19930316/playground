<?php


namespace App\Libraries\Definition\imports;


use App\Libraries\Definition\DataDefine;

class ProductImportDefine
{
    // 匯入規則
    public const IMPORT_RULE = [
        // 匯入新增
        'create' => [
            // 欄位名稱
            'product_no' => [
                // 類型 string, int, boolean
                'type' => 'string',
                // 是否為必要
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_name' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_valid_flag' => [
                'type' => 'boolean',
                'required' => DataDefine::BOOLEAN_FALSE,
                'default' => DataDefine::BOOLEAN_FALSE_INT,
            ],
            'product_item_no' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_name' => [
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'price' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'gp' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_valid_flag' => [
                'type' => 'boolean',
                'required' => DataDefine::BOOLEAN_FALSE,
                'default' => DataDefine::BOOLEAN_FALSE_INT,
            ],
        ],
        // 匯入更新
        'update' => [
            // 欄位名稱
            'product_no' => [
                // 類型 string, int, boolean
                'type' => 'string',
                // 是否為必要
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_name' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_valid_flag' => [
                'type' => 'boolean',
                'required' => DataDefine::BOOLEAN_FALSE,
                'default' => DataDefine::BOOLEAN_FALSE_INT,
            ],
            'product_item_no' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_name' => [
                'type' => 'string',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'price' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'gp' => [
                'type' => 'int',
                'required' => DataDefine::BOOLEAN_TRUE,
            ],
            'product_item_valid_flag' => [
                'type' => 'boolean',
                'required' => DataDefine::BOOLEAN_FALSE,
                'default' => DataDefine::BOOLEAN_FALSE_INT,
            ],
        ],
    ];
}
