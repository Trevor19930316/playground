<?php


namespace App\Libraries\Definition;



abstract class SystemExtraDataDefine
{
    public const TYPE_POWER_OF_ATTORNEY = 'power_of_attorney';   // 授權書
    public const TYPE_USER_EQUITY = 'user_equity';               // 會員權益

    public const TYPE_LIST = [
        self::TYPE_POWER_OF_ATTORNEY,
        self::TYPE_USER_EQUITY,
    ];

    // 儲存單一筆資料
    public const SINGLE_TYPE_LIST = [
        self::TYPE_POWER_OF_ATTORNEY,
        self::TYPE_USER_EQUITY,
    ];

    // 儲存多筆資料
    public const MULTIPLE_TYPE_LIST = [

    ];
}
