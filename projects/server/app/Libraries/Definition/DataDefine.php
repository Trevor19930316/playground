<?php


namespace App\Libraries\Definition;


abstract class DataDefine
{
    // boolean
    public const BOOLEAN_FALSE = false;
    public const BOOLEAN_TRUE = true;
    public const BOOLEAN_DATA_LIST = [
        self::BOOLEAN_FALSE,
        self::BOOLEAN_TRUE,
    ];

    // boolean to int
    public const BOOLEAN_FALSE_INT = 0;
    public const BOOLEAN_TRUE_INT = 1;
    public const BOOLEAN_DATA_INT_LIST = [
        self::BOOLEAN_FALSE_INT,
        self::BOOLEAN_TRUE_INT,
    ];

    // update_type
    public const UPDATE_TYPE_SYSTEM = 'system';
    public const UPDATE_TYPE_ADMIN_USER = 'admin_user';
    public const UPDATE_TYPE_LIST = [
        self::UPDATE_TYPE_SYSTEM,
        self::UPDATE_TYPE_ADMIN_USER,
    ];
}
