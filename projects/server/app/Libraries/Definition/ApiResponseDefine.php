<?php


namespace App\Libraries\Definition;


abstract class ApiResponseDefine
{
    public const GET_TOKEN_SUCCESS = 'get_api_token_success';                         // 取得 API Token 成功
    public const GET_TOKEN_FAILURE = 'get_api_token_failure';                         // 取得 API Token 失敗

    public const QUERY_SUCCESS = 'query_success';                                     // 查詢成功
    public const QUERY_FAILURE = 'query_failure';                                     // 查詢失敗

    public const CREATE_SUCCESS = 'create_success';                                   // 建立成功
    public const CREATE_FAILURE = 'create_failure';                                   // 建立失敗
    public const CREATE_REPEAT_FAILURE = 'create_repeat_failure';                     // 此類型資料不可重複建立

    public const UPDATE_SUCCESS = 'update_success';                                   // 更新成功
    public const UPDATE_FAILURE = 'update_failure';                                   // 更新失敗

    public const DELETE_SUCCESS = 'delete_success';                                    // 刪除成功
    public const DELETE_FAILURE = 'delete_failure';                                    // 刪除失敗

    public const PERMISSION_DENIED = 'permission_denied';                              // 權限不足
    public const DATA_IS_NOT_EXISTS = 'data_is_not_exists';                            // 資料不存在

    public const IMPORT_SUCCESS = 'import_success';                                    // 匯入成功
    public const IMPORT_FAIL = 'import_fail';                                          // 匯入失敗
    public const EXPORT_SUCCESS = 'export_success';                                    // 匯出失敗
    public const EXPORT_FAIL = 'export_fail';                                          // 匯出失敗
}
