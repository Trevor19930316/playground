<?php

namespace App\Libraries\Definition\type\badminton_court_charges;

abstract class TimeTypeDefine
{
    public const HOUR = 0;
    public const SEGMENT = 1;

    public const TYPE_LIST = [
        self::HOUR,
        self::SEGMENT,
    ];
}
