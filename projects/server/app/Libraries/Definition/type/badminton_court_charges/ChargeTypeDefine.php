<?php

namespace App\Libraries\Definition\type\badminton_court_charges;

abstract class ChargeTypeDefine
{
    public const UNLIMITED = 0;
    public const PEEK_OFF = 1;
    public const RUSH_HOUR = 2;

    public const TYPE_LIST = [
        self::UNLIMITED,
        self::PEEK_OFF,
        self::RUSH_HOUR,
    ];
}
