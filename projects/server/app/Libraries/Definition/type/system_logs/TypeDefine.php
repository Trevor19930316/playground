<?php

namespace App\Libraries\Definition\type\system_logs;

abstract class TypeDefine
{
    public const LOG = 'log';
    public const DEBUG = 'debug';
    public const INFO = 'info';
    public const NOTICE = 'notice';
    public const WARRING = 'warring';
    public const ERROR = 'error';

    public const SYSTEM_LOGS_TYPE_LIST = [
        self::LOG,
        self::DEBUG,
        self::INFO,
        self::NOTICE,
        self::WARRING,
        self::ERROR,
    ];
}
