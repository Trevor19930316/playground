<?php


namespace App\Libraries\Definition;


abstract class CacheDefine
{
    // 快取分隔符號
    public const SEPARATOR = ':';

    /*
     * Cache Category
     * 快取分類
     */

    // Cache Category 快取分類
    public const CACHE_CATEGORY_SYSTEM = 'system';
    public const CACHE_CATEGORY_DEFINITION = 'definition';
    public const CACHE_CATEGORY_ORDER = 'order';
    public const CACHE_CATEGORY_PRODUCT = 'product';
    public const CACHE_CATEGORY_PRODUCT_ITEM = 'product_item';
    public const CACHE_CATEGORY_USER = 'user';

    /*
     * Cache Key
     * 快取 key
     */

    // System Cache Key
    public const CACHE_KEY_LANG_TABLE_FIELD_EN = 'lang_table_field_en';                                       // 資料表欄位英文語系
    public const CACHE_KEY_LANG_TABLE_FIELD_TW = 'lang_table_field_tw';                                       // 資料表欄位中文語系
    // Definition Cache Key
    public const CACHE_KEY_DEFINITION_COUNTRY = 'definition_country';                                         // 國家資料
    public const CACHE_KEY_DEFINITION_ADDRESS_POSTAL = 'definition_address_postal';                           // 地址資料
    public const CACHE_KEY_DEFINITION_COUNTRY_CODE = 'definition_country_code';                               // 國碼資料
    // Order Cache Key
    public const CACHE_KEY_ORDER_KEY_BY_ID = 'order_key_by_id';                                               // 訂單資料, key by id
    public const CACHE_KEY_ORDER_KEY_BY_NO = 'order_key_by_no';                                               // 訂單資料, key by no
    public const CACHE_KEY_ORDER_ITEM_KEY_BY_ORDER_ID = 'order_item_key_by_order_id';                         // 訂單明細資料, key by order id
    public const CACHE_KEY_ORDER_ITEM_KEY_BY_ORDER_NO = 'order_item_key_by_order_no';                         // 訂單明細資料, key by order
    // Product Cache Key
    public const CACHE_KEY_PRODUCT_KEY_BY_ID = 'product_key_by_id';                                           // 商品資料, key by id
    public const CACHE_KEY_PRODUCT_KEY_BY_NO = 'product_key_by_no';                                           // 商品資料, key by no
    // Product Item Cache Key
    public const CACHE_KEY_PRODUCT_ITEM_KEY_BY_ID = 'product_item_key_by_id';                                 // 商品明細資料, key by no
    public const CACHE_KEY_PRODUCT_ITEM_KEY_BY_NO = 'product_item_key_by_no';                                 // 商品明細資料, key by no
    // User Cache Key
    public const CACHE_KEY_USER_KEY_BY_ID = 'user_key_by_id';                                                 // 會員資料, key by id
    public const CACHE_KEY_USER_KEY_BY_ACCOUNT = 'user_key_by_account';                                       // 會員資料, key by account

    // Cache Category List 快取分類清單
    public const ALL_CACHE_CATEGORY_LIST = [
        self::CACHE_CATEGORY_SYSTEM => [
            self::CACHE_KEY_LANG_TABLE_FIELD_EN,
            self::CACHE_KEY_LANG_TABLE_FIELD_TW,
        ],
        self::CACHE_CATEGORY_DEFINITION => [
            self::CACHE_KEY_DEFINITION_COUNTRY,
            self::CACHE_KEY_DEFINITION_ADDRESS_POSTAL,
            self::CACHE_KEY_DEFINITION_COUNTRY_CODE,
        ],
        self::CACHE_CATEGORY_ORDER => [
            self::CACHE_KEY_ORDER_KEY_BY_ID,
            self::CACHE_KEY_ORDER_KEY_BY_NO,
            self::CACHE_KEY_ORDER_ITEM_KEY_BY_ORDER_ID,
            self::CACHE_KEY_ORDER_ITEM_KEY_BY_ORDER_NO,
        ],
        self::CACHE_CATEGORY_PRODUCT => [
            self::CACHE_KEY_PRODUCT_KEY_BY_ID,
            self::CACHE_KEY_PRODUCT_KEY_BY_NO,
        ],
        self::CACHE_CATEGORY_PRODUCT_ITEM => [
            self::CACHE_KEY_PRODUCT_ITEM_KEY_BY_ID,
            self::CACHE_KEY_PRODUCT_ITEM_KEY_BY_NO,
        ],
        self::CACHE_CATEGORY_USER => [
            self::CACHE_KEY_USER_KEY_BY_ID,
            self::CACHE_KEY_USER_KEY_BY_ACCOUNT,
        ],
    ];

    // Single Cache Key List 單一快取資料
    public const SINGLE_CACHE_KEY_LIST = [
        self::CACHE_KEY_LANG_TABLE_FIELD_EN,
        self::CACHE_KEY_LANG_TABLE_FIELD_TW,
        self::CACHE_KEY_DEFINITION_COUNTRY,
        self::CACHE_KEY_DEFINITION_ADDRESS_POSTAL,
        self::CACHE_KEY_DEFINITION_COUNTRY_CODE,
    ];

    // Multiple Cache Key List 多筆快取資料
    public const MULTIPLE_CACHE_KEY_LIST = [
        self::CACHE_KEY_ORDER_KEY_BY_ID,
        self::CACHE_KEY_ORDER_KEY_BY_NO,
        self::CACHE_KEY_ORDER_ITEM_KEY_BY_ORDER_ID,
        self::CACHE_KEY_ORDER_ITEM_KEY_BY_ORDER_NO,
        self::CACHE_KEY_PRODUCT_KEY_BY_ID,
        self::CACHE_KEY_PRODUCT_KEY_BY_NO,
        self::CACHE_KEY_PRODUCT_ITEM_KEY_BY_ID,
        self::CACHE_KEY_PRODUCT_ITEM_KEY_BY_NO,
        self::CACHE_KEY_USER_KEY_BY_ID,
        self::CACHE_KEY_USER_KEY_BY_ID,
    ];
}
