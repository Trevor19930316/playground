<?php


namespace App\Libraries\Validation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Util\Exception;

class ValidatorHandler
{
    // 驗證資料表
    protected string $table;
    // Validator 預設 errors ( 暫時沒用到，不過先存起來 )
    protected $errors = null;
    // 錯誤訊息
    protected array $errorsMessages = [];

    public function __construct($model)
    {
        if ($model instanceof Model) {
            $this->table = $model->getTable();
        } else {
            $model = new $model();
            if (!$model instanceof Model) {
                throw new Exception(__CLASS__ . " " . __FUNCTION__ . " parameter is not instanceof Model");
            }
            $this->table = $model->getTable();
        }
    }

    // ==== validate 系列 =============================================

    /**
     * 資料驗證
     * @param array $validationData 驗證資料
     * @param array $validationRules 驗證規則
     * @return bool
     */
    public function validate(array $validationData, array $validationRules)
    {
        // 依資料移除不需要的驗證規則
//        foreach ($validationRules as $key => $value) {
//            if (!array_key_exists($key, $validationData)) {
//                unset($validationRules[$key]);
//            }
//        }

        // 更新驗證資料及規則鍵名，讓 validation lang 對應到 attributes
        foreach ($validationData as $key => $value) {
            $validationData[$this->table . "_" . $key] = $value;
            unset($validationData[$key]);
        }
        foreach ($validationRules as $key => $value) {
            $validationRules[$this->table . "_" . $key] = $value;
            unset($validationRules[$key]);
        }

        $validation = Validator::make($validationData, $validationRules);

        // 驗證失敗
        if ($validation->fails()) {
            $this->errors = $validation->errors();
            // 額外處理錯誤資訊
            foreach ($this->errors->getMessages() as $key => $value) {
                $key = str_replace($this->table . "_", '', $key);
                $this->errorsMessages[$key] = $value;
            }
        }

        return empty($this->errorsMessages);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getErrorsMessages(): array
    {
        return $this->errorsMessages;
    }
}
