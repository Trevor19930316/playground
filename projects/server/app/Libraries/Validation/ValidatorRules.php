<?php

use App\Libraries\Validation\ValidatorHandler;

trait ValidatorRules
{
    /**
     * 設定 地址 驗證規則
     * @param \Illuminate\Http\Request $request
     * @param string $moduleName
     * @param boolean $isRequired
     */
    public function setAddressRules($request, $moduleName, $isRequired)
    {
        $postal_id = $moduleName . '_postal_id';
        $postal_code = $moduleName . '_postal_code';
        $address = $moduleName . '_address';

        $this->setData([
            $postal_id => $request->input($postal_id),
            $postal_code => $request->input($postal_code),
            $address => $request->input($address),
        ]);

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        $this->setRules([
            $postal_id => [$isRequiredText, 'integer'],
            $postal_code => [$isRequiredText, 'max:20'],
            $address => [$isRequiredText, 'max:255'],
        ]);

    }

    /**
     * 設定 銀行 驗證規則
     * @param \Illuminate\Http\Request $request
     * @param string $moduleName
     * @param boolean $isRequired
     */
    public function setBankRules($request, $moduleName, $isRequired)
    {
        $bank_id = $moduleName . '_bank_id';
        $account_no = $moduleName . '_account_no';
        $account_name = $moduleName . '_account_name';

        $this->setData([
            $bank_id => $request->input($bank_id),
            $account_no => $request->input($account_no),
            $account_name => $request->input($account_name),
        ]);

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        $this->setRules([
            $bank_id => [$isRequiredText, 'integer'],
            $account_no => [$isRequiredText, 'max:50'],
            $account_name => [$isRequiredText, 'max:50'],
        ]);
    }

    /**
     * 設定 Email 驗證規則
     * @param \Illuminate\Http\Request $request
     * @param string $inputName
     * @param boolean $isRequired
     */
    public function setEmailRules($request, $inputName, $isRequired)
    {
        $this->setData([
            $inputName => $request->input($inputName),
        ]);

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        $this->setRules([
            $inputName => [$isRequiredText, 'email', 'max:100']
        ]);
    }

    /**
     * 設定 台灣身分證 驗證規則
     * @param \Illuminate\Http\Request $request
     * @param string $inputName
     */
    public function setTaiwaneseIdentifyNoRules($request, $inputName)
    {
        $this->setRules([
            $inputName => ['regex:/^[A-Z]{1}[12ABCD]{1}[[:digit:]]{8}$/']
        ]);
    }

    /**
     * 設定 手機 驗證規則
     * @param \Illuminate\Http\Request $request
     * @param string $moduleName
     * @param boolean $isRequired
     */
    public function setMobileRules($request, $moduleName, $isRequired)
    {
        $country_code = $moduleName . '_country_code';

        $this->setData([
            $country_code => $request->input($country_code),
            $moduleName => $request->input($moduleName),
        ]);

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        $Systems_Locales = new Systems_Locales();
        $systemsLocalesIdNameLists = $Systems_Locales->getCountryCodes('id', 'country_code');

        $this->setRules([
            $country_code => [$isRequiredText, 'max:10', Rule::in($systemsLocalesIdNameLists)],
            $moduleName => [$isRequiredText, 'max:50'],
        ]);

    }

    /**
     * 設定 圖片上傳 驗證規則
     * @param \Illuminate\Http\Request $request 把 Controller 的 $request 丟過來
     * @param string $inputName input 名稱 通常是 資料庫欄位名稱
     * @param string $uploadFileType 參考 config('code-yield.uploadFileType') 的 key
     * @param boolean $isRequired
     * @param null|string $tableColumn
     * @return $this
     */
    public function setUploadFileRules($request, $inputName, $uploadFileType, $isRequired = false, $tableColumn = null)
    {
        // $uploadFileType
        $validUploadFileTypes = config('code-yield.uploadFileType');

        if (!array_key_exists($uploadFileType, $validUploadFileTypes)) {
            dd('HelperValidation setUploadFileRules() invalid $uploadFileType ( ' . $uploadFileType . ' )');
        }

        $validUploadFileExtensions = $validUploadFileTypes[$uploadFileType];

        $imageExtensions = join(',', $validUploadFileExtensions);
        $imageUrlExtensions = 'regex:/\.(?:' . join('|', $validUploadFileExtensions) . ')+$/';

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        // $FileSizeKb 檔案上傳最大 kb 數
        $validationFileSize = $request->input($inputName . '_validationFileSize');
        $FileSizeKb = is_null($validationFileSize) ? config('code-yield.uploadFileLimitFileSizeKb') : $validationFileSize;

        $tableColumn = is_null($tableColumn) ? $inputName : $tableColumn;

        if (!is_null($request->file($inputName))) {

            // 圖片檔案防呆
            $this->setData([$tableColumn => $request->file($inputName)]);
            $this->setRules([
                $tableColumn => [$isRequiredText, 'file', 'max:' . $FileSizeKb, 'mimes:' . $imageExtensions],
            ]);

        } else {

            // 圖片網址防呆
            $this->setData([$tableColumn => $request->input($inputName)]);
            $this->setRules([
                $tableColumn => [$isRequiredText, 'url', 'max:255', 'starts_with:https', $imageUrlExtensions],
            ]);
        }

        return $this;
    }

    /**
     * 設定 圖片網址 驗證規則
     * ex. 匯入檔案驗證用
     * @param string $inputName input 名稱
     * @param string $inputValue input 值
     * @param boolean $isRequired
     */
    public function setImageUrlRules($inputName, $inputValue, $isRequired = false)
    {
        $validUploadFileExtensions = config('code-yield.uploadFileType.image');

        $imageUrlExtensions = 'regex:/\.(?:' . join('|', $validUploadFileExtensions) . ')+$/';

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        // 圖片網址防呆
        $this->setData([$inputName => $inputValue]);
        $this->setRules([
            $inputName => [$isRequiredText, 'url', 'max:255', 'starts_with:https', $imageUrlExtensions],
        ]);
    }

    /**
     * 設定 color_code 驗證規則
     * @param \Illuminate\Http\Request $request
     * @param string $name
     * @param boolean $isRequired
     */
    public function setColorCodeRules($request, $name, $isRequired)
    {
        $this->setData([
            $name => str_replace(' ', '', $request->input($name)),
        ]);

        // $isRequired
        $isRequiredText = $isRequired ? 'required' : 'nullable';

        $this->setRules([
            $name => [
                $isRequiredText,
                'regex:/#[a-zA-Z0-9]{6}|rgb\((?:\s*\d+\s*,){2}\s*[\d]+\)|rgba\((\s*\d+\s*,){3}[\d\.]+\)|hsl\(\s*\d+\s*(\s*\,\s*\d+\%){2}\)|hsla\(\s*\d+(\s*,\s*\d+\s*\%){2}\s*\,\s*[\d\.]+\)/'
            ],
        ]);

    }
}
