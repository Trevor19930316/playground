<?php


namespace App\Libraries\Handler;


use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CurlHandler
{
    /**
     * @param string $url
     * @param array $header
     * @param int $timeout
     * @return bool|string
     */
    public static function get(string $url, array $header = [], int $timeout = 3)
    {
        $ch = curl_init();

        if (env('APP_ENV') === 'local') {
            // 跳過憑證檢查
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        } elseif (substr($url, 0, 5) == 'https') {
            // 檢查網址
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳過憑證檢查
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 從憑證中檢查SSL加密演算法是否存在
        }
        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if(curl_exec($ch) === false) {
            dump("CURL ERROR : " . curl_error($ch));
        }

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * @param $url
     * @param array $post_data
     * @param array $header
     * @param int $timeout
     * @return mixed
     */
    public static function post($url, array $post_data = [], array $header = [], int $timeout = 3)
    {
        $jsonStr = json_encode($post_data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
        if (empty($header)) {
            $header = [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonStr),
            ];
        }

        $ch = curl_init($url);

        if (env('APP_ENV') === 'local') {
            // 跳過憑證檢查
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        } elseif (substr($url, 0, 5) == 'https') {
            // 檢查網址
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳過憑證檢查
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 從憑證中檢查SSL加密演算法是否存在
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }
}
