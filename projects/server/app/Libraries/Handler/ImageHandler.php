<?php


namespace App\Libraries\Handler;

use App\Libraries\Definition\ErrorMessageDefine;
use App\Libraries\Validation\ValidatorHandler;
use Illuminate\Database\Eloquent\Collection;

class ImageHandler extends FileHandler
{
    // image 合法檔案類型
    public const image_file_extensions = ['jpg', 'jpeg', 'png', 'gif'];
    // image 大小限制
    public const image_kb_size = '1024kb';
    // image 驗證條件
    public const validate_rule = [
        'image',
        'max:' . self::image_kb_size,
        'mimes:jpg,jpeg,png,gif'
    ];

    // validate errormessage
    protected static array $validateErrorMessages = [];

    /**
     * check file extension
     *
     * @param $file_extension
     * @return bool
     */
    private static function checkImageFileExtension($file_extension): bool
    {
        return in_array($file_extension, self::image_file_extensions);
    }

    /**
     * get validate error messages
     *
     * @return array
     */
    public static function getValidateErrorMessages(): array
    {
        return self::$validateErrorMessages;
    }

    /**
     * 驗證圖檔
     *
     * @param $file
     * @param $model
     * @param string $field
     * @return bool
     */
    public static function validateImage($file, $model, string $field): bool
    {
        // 驗證檔案
        $ValidatorHandler = new ValidatorHandler($model);
        $valid_status = $ValidatorHandler->validate([$field => $file], self::validate_rule);
        if (!$valid_status) {
            self::$errorMessage = ErrorMessageDefine::IMAGE_VALIDATE_FAILED;
            self::$validateErrorMessages += $ValidatorHandler->getErrorsMessages();
            return false;
        }

        return true;
    }

    /**
     * 上傳圖片
     *
     * @param \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[] $file
     * @param string $folder
     * @param string $return_field
     * @return mixed
     */
    public static function uploadImage($file, string $folder, string $return_field = 'path')
    {
        // 檢查副檔名
        if (!self::checkImageFileExtension($file->extension())) {
            return false;
        }

        // folder = $folder/20211117
        $folder = $folder . "/" . date('Ymd');
        $put_status = self::putFile($file, $folder, $return_field);
        if (!$put_status) {
            return false;
        }

        return $put_status;
    }

    /**
     * 上傳圖片(多張)
     *
     * @param array $files
     * @param string $folder
     * @param string $return_field
     * @return mixed
     */
    public static function uploadImages(array $files, string $folder, string $return_field = 'path')
    {
        $upload_return = self::putFiles($files, $folder, $return_field);
        if (!$upload_return) {
            return false;
        }

        return $upload_return;
    }

    /**
     * 圖片存放位置轉換成 url
     *
     * @param array|Collection $data
     * @param array $keys
     * @return mixed
     */
    public static function convertImageToUrl($data, array $keys)
    {
        if ($data instanceof Collection) {
            // collection 處理
            foreach ($data as $model) {
                foreach ($model as $key => &$value) {
                    $str_prefix = substr($value, 0, 4);
                    if (!empty($value) && $str_prefix !== 'http' && in_array($key, $keys, true)) {
                        $value = config('app.file_url') . '/' . $value;
                    }
                }
            }
        } else {
            // array 處理
            foreach ($data as $key => &$value) {
                $str_prefix = substr($value, 0, 4);
                if (!empty($value) && $str_prefix !== 'http' && in_array($key, $keys, true)) {
                    $value = config('app.file_url') . '/' . $value;
                }
            }
        }

        return $data;
    }

    /**
     * 驗證圖檔(多張)
     *
     * @param array $files
     * @param $model
     * @return bool
     */
    public static function validateImages($files, $model)
    {
        // 空陣列回傳 true
        if (empty($files)) {
            return true;
        }

        // 逐一驗證
        foreach ($files as $input_key => $image_file) {
            if (!self::validateImage($files, $model, $input_key)) {
                return false;
            }
        }

        return true;
    }
}
