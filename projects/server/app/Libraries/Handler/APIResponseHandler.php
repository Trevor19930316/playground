<?php


namespace App\Libraries\Handler;


use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class APIResponseHandler
{
    public int $_HTTP_CODE = Response::HTTP_OK;
    public bool $_STATUS = DataDefine::BOOLEAN_TRUE;
    public string $_MESSAGE = '';
    public array $_DATA = [];
    public array $_VALIDATION_ERROR_MESSAGE = [];

    /**
     * @param int $http_code
     * @return APIResponseHandler
     */
    public function setHttpStatus(int $http_code = Response::HTTP_OK): APIResponseHandler
    {
        $this->_HTTP_CODE = $http_code;
        return $this;
    }

    /**
     * @return APIResponseHandler
     */
    public function isStatusFalse(): APIResponseHandler
    {
        $this->_STATUS = false;
        return $this;
    }

    /**
     * @return APIResponseHandler
     */
    public function isStatusTrue(): APIResponseHandler
    {
        $this->_STATUS = true;
        return $this;
    }

    /**
     * @param string $message
     * @return APIResponseHandler
     */
    public function setMessage(string $message): APIResponseHandler
    {
        $this->_MESSAGE = $message;
        return $this;
    }

    /**
     * @param string $lang_key
     * @return APIResponseHandler
     */
    public function setMessageLang(string $lang_key): APIResponseHandler
    {
        $this->_MESSAGE = StringHandler::getDefineLang(ApiResponseDefine::class, $lang_key);
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data): APIResponseHandler
    {
        $this->_DATA = $data;
        return $this;
    }

    /**
     * @param array $message
     * @return APIResponseHandler
     */
    public function setValidationErrorMessage(array $message): APIResponseHandler
    {
        $this->_VALIDATION_ERROR_MESSAGE = $message;
        return $this;
    }

    /**
     * 取得 Response array
     * @return array
     */
    public function getResponseData(): array
    {
        return [
            'status' => $this->_STATUS,
            'message' => $this->_MESSAGE,
            'data' => $this->_DATA,
            'validation_error_message' => $this->_VALIDATION_ERROR_MESSAGE,
        ];
    }

    /**
     * 取得 自訂 Response
     * @return JsonResponse
     */
    public function getResponse(): JsonResponse
    {
        return response()->json($this->getResponseData(), $this->_HTTP_CODE);
    }

    /**
     * 取得 Token Response
     *
     * @param $token
     * @param $expires_in
     * @return JsonResponse
     */
    public function getTokenResponse($token, $expires_in): JsonResponse
    {
        $this->_MESSAGE = StringHandler::getDefineLang(ApiResponseDefine::class, ApiResponseDefine::GET_TOKEN_SUCCESS);
        $this->_DATA = [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expires_in,
        ];

        return response()->json($this->getResponseData());
    }

    /**
     * 取得 API 成功 Response
     *
     * @param mixed $data
     * @param string $message
     * @return JsonResponse
     */
    public function getSuccessResponse($data, string $message = ''): JsonResponse
    {
        $this->_DATA = $data;

        if (!empty($message)) {
            $this->_MESSAGE = StringHandler::getDefineLang(ApiResponseDefine::class, $message);
        }

        $this->_STATUS = DataDefine::BOOLEAN_TRUE;
        $this->_VALIDATION_ERROR_MESSAGE = [];

        return response()->json($this->getResponseData());
    }

    /**
     * 取得 API 失敗 Response
     *
     * @param array $data
     * @param string $message
     * @param array $validation_error_message
     * @return JsonResponse
     */
    public function getFailureResponse(array $data = [], string $message = '', array $validation_error_message = []): JsonResponse
    {
        $this->_DATA = $data;

        if (!empty($message)) {
            $this->_MESSAGE = StringHandler::getDefineLang(ApiResponseDefine::class, $message);
        }

        if (!empty($validation_error_message)) {
            $this->_VALIDATION_ERROR_MESSAGE = $validation_error_message;
        }

        $this->_STATUS = DataDefine::BOOLEAN_FALSE;

        return response()->json($this->getResponseData());
    }
}
