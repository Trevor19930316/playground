<?php


namespace App\Libraries\Handler;

/**
 * 系統 Cache
 * TODO Check > 跑排程或Artisan
 */

use App\Libraries\Cache\BaseCache;
use App\Libraries\Cache\DefinitionCache;
use App\Libraries\Cache\ProductCache;
use App\Libraries\Cache\ProductItemCache;
use App\Libraries\Cache\SystemCache;
use App\Libraries\Cache\UserCache;
use App\Libraries\Definition\CacheDefine;
use App\Models\Product;
use App\Models\ProductItem;
use App\Models\User;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Facades\Redis;

class CacheHandler
{
    /**
     * get cache Category list
     *
     * @return array
     */
    public static function getCacheCategoryList(): array
    {
        return StringHandler::getDefineLang(CacheDefine::class, array_keys(CacheDefine::ALL_CACHE_CATEGORY_LIST));
    }

    /**
     * get cache key list
     *
     * @return array
     */
    public static function getCacheKeyList(): array
    {
        $list = [];
        foreach (CacheDefine::ALL_CACHE_CATEGORY_LIST as $category => $cacheKeys) {
            foreach ($cacheKeys as $cacheKey) {
                $keyStr = StringHandler::getDefineLang(CacheDefine::class, $cacheKey);
                if (in_array($cacheKey, CacheDefine::SINGLE_CACHE_KEY_LIST)) {
                    $list['single'][$category][$cacheKey] = $keyStr;
                } elseif (in_array($cacheKey, CacheDefine::MULTIPLE_CACHE_KEY_LIST)) {
                    $list['multiple'][$category][$cacheKey] = $keyStr;
                }
            }
        }
        return $list;
    }

    /**
     * get single cache key data
     *
     * @param $category
     * @param $key
     * @return mixed
     */
    public static function getSingleCacheKeyData($category, $key)
    {
        $cacheKey = $category . CacheDefine::SEPARATOR . $key;
        return BaseCache::getCacheData($cacheKey);
    }

    /**
     * get multiple cache key data
     *
     * @param $category
     * @param $key
     * @return mixed
     */
    public static function getMultipleCacheKeyData($category, $key)
    {
        $list = [];
        $childKeys = Redis::keys($category . CacheDefine::SEPARATOR . $key . '*');
        if (!empty($childKeys)) {
            foreach ($childKeys as $key) {
                $list[$key] = BaseCache::getCacheData($key);
            }
        }
        return $list;
    }

    /**
     * Cache 資料庫表格欄位翻譯
     *
     * @return mixed
     */
    public static function cacheTablesColumnsLang(): array
    {
        return SystemCache::cacheTablesColumnsLang();
    }

    /**
     * Cache 國家
     *
     * @return mixed
     */
    public static function cacheDefinitionCountries()
    {
        return DefinitionCache::cacheCountries();
    }

    /**
     * Cache 地址
     *
     * @return mixed
     */
    public static function cacheDefinitionAddressPostal()
    {
        return DefinitionCache::cacheAddressPostal();
    }

    /**
     * Cache 手機國碼
     *
     * @return mixed
     */
    public static function cacheDefinitionCountriesCode()
    {
        return DefinitionCache::cacheCountriesCode();
    }

    /**
     * TODO 考量大量作快取
     * Cache products and items
     *
     * @return void
     */
    public static function cacheAllProductsAndProductItems(): void
    {
        // 商品
        $products = Product::select('id', 'no')->pluck('no', 'id')->toArray();
        foreach ($products as $product_id => $product_no) {
            ProductCache::cacheProductById($product_id);
            ProductCache::cacheProductByNo($product_no);
        }

        // 商品明細
        $productsItems = ProductItem::select('id', 'no')->pluck('no', 'id')->toArray();
        foreach ($productsItems as $product_item_id => $product_item_no) {
            ProductItemCache::cacheProductItemById($product_item_id);
            ProductItemCache::cacheProductItemByNo($product_item_no);
        }
    }

    /**
     * TODO 考量大量作快取
     *
     * Cache users
     */
    public static function cacheAllUsers(): void
    {
        $user_ids = User::select('id', 'account')->pluck('account', 'id')->toArray();
        foreach ($user_ids as $user_id => $user_account) {
            UserCache::cacheUserById($user_id);
            UserCache::cacheUserByAccount($user_account);
        }
    }
}
