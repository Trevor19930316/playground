<?php


namespace App\Libraries\Handler;

use App\Jobs\FileImport;
use App\Libraries\Definition\ErrorMessageDefine;
use App\Libraries\Definition\FileImportDefine;

class FileImportHandler extends FileHandler
{
    protected static $upload_id;

    /**
     * check import type
     *
     * @param $import_type
     * @return bool
     */
    private static function checkImportType($import_type): bool
    {
        return in_array($import_type, FileImportDefine::IMPORT_TYPE_LIST, true);
    }

    /**
     * check file extension
     *
     * @param $file_extension
     * @return bool
     */
    private static function checkImportFileExtension($file_extension): bool
    {
        return in_array($file_extension, FileImportDefine::IMPORT_FILE_EXTENSION_LIST, true);
    }

    /**
     * 匯入執行 (入口)
     *
     * @param string $importType
     * @param string $importMode
     * @param \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[] $importFile
     * @return bool
     */
    public static function import(string $importType, string $importMode, $importFile): bool
    {
        // 匯入類型不合法
        if (!self::checkImportType($importType)) {
            self::$errorMessage = ErrorMessageDefine::IMPORT_TYPE_IS_NOT_LEGAL;
            return false;
        }

        // 匯入檔案副檔名不合法
        if (!self::checkImportFileExtension($importFile->extension())) {
            self::$errorMessage = ErrorMessageDefine::FILE_EXTENSION_IS_NOT_LEGAL;
            return false;
        }

        // folder = $importType/20211117
        $folder = $importType . "/" . date('Ymd');
        self::$upload_id = self::putFile($importFile, $folder, 'id');
        if (!self::$upload_id) {
            return false;
        }

        // 執行 Job
        $importJob = (new FileImport(self::$upload_id, $importType, $importMode))->onQueue('file_import');
        dispatch($importJob);

        return true;
    }
}
