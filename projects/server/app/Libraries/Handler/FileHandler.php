<?php


namespace App\Libraries\Handler;

use App\Libraries\Definition\ErrorMessageDefine;
use App\Models\FileUploadLog;
use Illuminate\Support\Facades\Storage;

class FileHandler
{
    // 存放檔案位置
    public const upload_disk = 'files';

    // 錯誤訊息
    protected static $errorMessage = null;

    /**
     * 存放單一檔案
     *
     * @param \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[] $file
     * @param string $folder
     * @param string $return_field
     * @return mixed
     */
    protected static function putFile($file, string $folder, string $return_field = 'path')
    {
        // 檔案類型(副檔名)
        $file_extension = $file->extension();
        // 檔名
        $file_name = time() . ".$file_extension";
        // 存放檔案
        $result = Storage::disk(self::upload_disk)->putFileAs($folder, $file, $file_name);

        if (!$result) {
            self::$errorMessage = ErrorMessageDefine::FILE_PUT_FAILED;
            return false;
        }

        // disk 底下路徑
        $file_disk_path = self::upload_disk . "/$folder/$file_name";

        return self::writeUploadLog($file, $file_name, $file_disk_path)->$return_field;
    }

    /**
     * 存放多個檔案
     *
     * @param array $files
     * @param string $folder
     * @param string $return_field
     * @return false|array
     */
    protected static function putFiles(array $files, string $folder, string $return_field = 'path')
    {
        $return_data = [];
        foreach ($files as $key => $file) {
            // 確認上傳狀態
            $put_return = self::putFile($file, $folder, $return_field);
            if (!$put_return) {
                self::$errorMessage = ErrorMessageDefine::FILE_PUT_FAILED;
                // 多張上傳只要一張沒過就全部刪除
                self::deleteFile($put_return, $return_field);
                return false;
            }
            // 把回傳資料塞進陣列
            $return_data[$key] = $put_return;
        }

        return $return_data;
    }

    /**
     * 刪除檔案
     *
     * @param array|string $delete_data
     * @param string $delete_by
     */
    public static function deleteFile($delete_data, string $delete_by = 'path'): void
    {
        $path = [];
        if ($delete_by === 'path') {
            // by 路徑刪除
            $path = $delete_data;
        } elseif ($delete_by === 'id') {
            // by id 刪除
            $path = [];
            if (is_array($delete_data)) {
                $path = FileUploadLog::whereIn($id)->pluck('path')->toArray();
            } else {
                $upload = FileUploadLog::find($id);
                if (!is_null($upload)) {
                    $path = $upload->path;
                }
            }
        }

        Storage::delete($path);
    }

    /**
     * 寫入紀錄
     *
     * @param \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[] $file
     * @param string $upload_name
     * @param string $path
     * @return
     */
    private static function writeUploadLog($file, string $upload_name, string $path)
    {
        return FileUploadLog::create([
            'extension' => $file->extension(),
            'original_name' => $file->getClientOriginalName(),
            'upload_name' => $upload_name,
            'path' => $path,
            'created_by' => auth()->user()->name,
            'updated_by' =>  auth()->user()->name,
        ]);
    }

    /**
     * get error message
     *
     * @return string
     */
    public static function getErrorMessage(): string
    {
        return StringHandler::getDefineLang(ErrorMessageDefine::class, self::$errorMessage);
    }

    /**
     * get storage public file path
     * @param string $filePath
     * @param string|null $disk
     * @return mixed
     */
    public static function getStorageFilePath(string $filePath, string $disk = null)
    {
        return Storage::disk($disk)->path($filePath);
    }
}
