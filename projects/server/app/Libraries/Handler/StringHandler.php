<?php


namespace App\Libraries\Handler;


use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Support\Facades\Log;
use ReflectionClass;
use ReflectionException;

class StringHandler
{
    // App\Libraries\Definition
    private static string $define_lang_file_path = 'App\\Libraries\\';

    /**
     * 下底線轉小駝峰
     *
     * @param $str
     * @return string
     */
    public static function underScoreCaseToCamelize($str): string
    {
        $str = '_' . str_replace('_', ' ', strtolower($str));
        return ltrim(str_replace(' ', '', ucwords($str)), '_');
    }

    /**
     * 下底線轉大駝峰
     *
     * @param $str
     * @return string
     */
    public static function underScoreCaseToCamelizeAndFirstWord($str): string
    {
        $str = '_' . str_replace('_', ' ', strtolower($str));
        return ucfirst(ltrim(str_replace(' ', '', ucwords($str)), '_'));
    }

    /**
     * 駝峰轉下底線
     *
     * @param $str
     * @return string
     */
    public static function camelizeToUnderScoreCase($str): string
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1' . "_" . '$2', $str));
    }

    /**
     * @param $class
     * @param array|string $keys
     * @return array|string
     */
    public static function getDefineLang($class, $keys)
    {
        try {

            $ReflectionClass = new ReflectionClass($class);

            $langPath = str_replace(self::$define_lang_file_path, '', $ReflectionClass->getName());
            $langPath = self::camelizeToUnderScoreCase($langPath);
            $langPath = str_replace("\\", "/", $langPath);

            if (is_array($keys)) {

                $langList = [];

                foreach ($keys as $key) {
                    $lang = __($langPath . ".$key");
                    $langList[$key] = !is_null($lang) ? $lang : $key;
                }

                return $langList;

            } elseif (is_string($keys)) {

                $lang = __($langPath . ".$keys");

                return !is_null($lang) ? $lang : $keys;
            }

        } catch (ReflectionException $e) {
            Debugbar::warning($e->getFile());
            Debugbar::warning($e->getLine());
            Log::warning($e->getFile());
            Log::warning($e->getLine());
            SystemLogHandler::warring('file name', $e->getFile());
            SystemLogHandler::warring('file line', $e->getLine());
        }

        return $keys;
    }

    /**
     * TODO Cache
     *
     * @param string $table_name
     * @param string $column_name
     * @param array|string $keys
     * @return array|string
     */
    public static function getDefineTypeLang(string $table_name, string $column_name, $keys)
    {
        $langPath = 'definition.type.' . $table_name . $column_name . '_define';

        return self::getDefineTableColumnLang($langPath, $keys);
    }

    /**
     * TODO Cache
     *
     * @param string $table_name
     * @param string $column_name
     * @param array|string $keys
     * @return array|string
     */
    public static function getDefineStatusLang(string $table_name, string $column_name, $keys)
    {
        $langPath = 'definition.status.' . $table_name . $column_name . '_define';

        return self::getDefineTableColumnLang($langPath, $keys);
    }

    /**
     * @param $langPath
     * @param $keys
     * @return array|string
     */
    private static function getDefineTableColumnLang($langPath, $keys)
    {
        if (is_array($keys)) {

            $langList = [];
            foreach ($keys as $key) {
                $lang = __($langPath . ".$key");
                $langList[$key] = !is_null($lang) ? $lang : $key;
            }

            return $langList;
        }

        if (is_string($keys)) {
            $lang = __($langPath . ".$keys");
            return !is_null($lang) ? $lang : $keys;
        }

        return null;
    }
}
