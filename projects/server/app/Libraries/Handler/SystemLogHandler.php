<?php


namespace App\Libraries\Handler;


use App\Libraries\Definition\type\system_logs\TypeDefine;
use App\Models\SystemLog;

class SystemLogHandler
{
    protected static string $model = SystemLog::class;
    protected static string $defaultType = TypeDefine::LOG;

    /**
     * 取得建立 log 的資料格式
     *
     * @param string $type
     * @param string $title
     * @param string $content
     * @return array
     */
    private static function getCreateFormatData(string $type, string $title, string $content = ''): array
    {
        return [
            'type' => $type,
            'title' => $title,
            'content' => $content,
            'created_time' => now(),
        ];
    }

    /**
     * create default log
     *
     * @param $title
     * @param $content
     */
    public static function log($title, $content): void
    {
        self::$model->create(self::getCreateFormatData(self::$defaultType, $title, $content));
    }

    /**
     * create info log
     *
     * @param $title
     * @param $content
     */
    public static function info($title, $content): void
    {
        self::$model->create(self::getCreateFormatData(TypeDefine::INFO, $title, $content));
    }

    /**
     * create debug log
     *
     * @param $title
     * @param $content
     */
    public static function debug($title, $content): void
    {
        self::$model->create(self::getCreateFormatData(TypeDefine::DEBUG, $title, $content));
    }

    /**
     * create notice log
     *
     * @param $title
     * @param $content
     */
    public static function notice($title, $content): void
    {
        self::$model->create(self::getCreateFormatData(TypeDefine::NOTICE, $title, $content));
    }

    /**
     * create warring log
     *
     * @param $title
     * @param $content
     */
    public static function warring($title, $content): void
    {
        self::$model->create(self::getCreateFormatData(TypeDefine::WARRING, $title, $content));
    }

    /**
     * create error log
     *
     * @param $title
     * @param $content
     */
    public static function error($title, $content): void
    {
        self::$model->create(self::getCreateFormatData(TypeDefine::ERROR, $title, $content));
    }
}
