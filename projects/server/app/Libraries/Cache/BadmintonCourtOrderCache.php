<?php


namespace App\Libraries\Cache;


use App\Libraries\Definition\CacheDefine;
use App\Models\BadmintonCourtOrder;

class BadmintonCourtOrderCache extends BaseCache
{
    public static function setCacheCategory(): string
    {
        return CacheDefine::CACHE_CATEGORY_ORDER;
    }

    /**
     * get cache order data by id
     *
     * @param int $order_id
     * @return array|false|mixed
     */
    public static function getOrderByID(int $order_id)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_ORDER_KEY_BY_ID,
            $order_id
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * get cache order data by no
     *
     * @param string $order_no
     * @return array|false|mixed
     */
    public static function getOrderByNo(string $order_no)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_ORDER_KEY_BY_NO,
            $order_no
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * set cache order data by id
     *
     * @param int $order_id
     * @param array $order_data
     * @return bool
     * @throws \JsonException
     */
    public static function cacheOrderById(int $order_id, array $order_data = []): bool
    {
        if (empty($order_data)) {
            $order_data = BadmintonCourtOrder::with('orderItems')->find($order_id);
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_ORDER_KEY_BY_ID,
            $order_id
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($order_data, JSON_THROW_ON_ERROR));
    }

    /**
     * set cache order data by no
     *
     * @param string $order_no
     * @param array $order_data
     * @return bool
     * @throws \JsonException
     */
    public static function cacheOrderByNo(string $order_no, array $order_data = []): bool
    {
        if (empty($order_data)) {
            $order_data = BadmintonCourtOrder::with('orderItems')->where('no', '=', $order_no)->first();
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_ORDER_KEY_BY_NO,
            $order_no
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($order_data, JSON_THROW_ON_ERROR));
    }

    /**
     * delete cache order data by id
     *
     * @param $order_id
     */
    public static function deleteCacheOrderById($order_id): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_ORDER_KEY_BY_ID,
            $order_id
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }

    /**
     * delete cache order data by no
     *
     * @param $order_no
     */
    public static function deleteCacheOrderByNo($order_no): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_ORDER_KEY_BY_NO,
            $order_no
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }
}
