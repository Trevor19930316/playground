<?php


namespace App\Libraries\Cache;


use App\Libraries\Definition\CacheDefine;
use App\Models\DefinitionAddressPostal;
use App\Models\DefinitionCountry;
use App\Models\DefinitionCountryCode;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class DefinitionCache extends BaseCache
{
    protected static function setCacheCategory(): string
    {
        return CacheDefine::CACHE_CATEGORY_DEFINITION;
    }

    /**
     * Cache 國家
     *
     * @return mixed
     */
    public static function cacheCountries(): array
    {
        $definitionCountries = DefinitionCountry::all();
        $cacheData = [];
        foreach ($definitionCountries as $country) {
            $cacheData[] = [
                'id' => $country->id,
                'name' => $country->name,
            ];
        }

        self::setSingleCacheData(CacheDefine::CACHE_KEY_DEFINITION_COUNTRY, json_encode($cacheData, JSON_THROW_ON_ERROR));

        return $cacheData;
    }

    /**
     * Cache 地址
     *
     * @return mixed
     */
    public static function cacheAddressPostal(): array
    {
        $taiwan_id = DefinitionCountry::getTaiwanId();
        $countries_data = DefinitionCountry::getCountriesArray();
        $definition_address_postal = DefinitionAddressPostal::where('definition_country_id', '=', $taiwan_id)->get();

        $cacheData = [];

        foreach ($definition_address_postal as $address) {
            $cacheData[] = [
                'id' => $address->id,
                'country_id' => $address->definition_country_id,
                'country_name' => $countries_data[$address->definition_country_id]['name'],
                'zip_code' => $address->zip_code,
                'city' => $address->city,
                'district' => $address->district,
                'latitude' => $address->latitude,
                'longitude' => $address->longitude,
            ];
        }

        self::setSingleCacheData(CacheDefine::CACHE_KEY_DEFINITION_ADDRESS_POSTAL, json_encode($cacheData, JSON_THROW_ON_ERROR));

        return $cacheData;
    }

    /**
     * Cache 手機國碼
     *
     * @return mixed
     */
    public static function cacheCountriesCode(): array
    {
        $countries_data = DefinitionCountry::getCountriesArray();
        $definition_countries_code = DefinitionCountryCode::all();

        $cacheData = [];

        foreach ($definition_countries_code as $country_code) {
            $cacheData[] = [
                'country_id' => $country_code->definition_country_id,
                'country_name' => $countries_data[$country_code->definition_country_id]['name'],
                'country_code' => $country_code->country_code,
            ];
        }

        self::setSingleCacheData(CacheDefine::CACHE_KEY_DEFINITION_COUNTRY_CODE, json_encode($cacheData, JSON_THROW_ON_ERROR));

        return $cacheData;
    }
}
