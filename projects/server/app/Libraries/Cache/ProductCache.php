<?php


namespace App\Libraries\Cache;


use App\Libraries\Definition\CacheDefine;
use App\Models\Product;

class ProductCache extends BaseCache
{
    protected static function setCacheCategory(): string
    {
        return CacheDefine::CACHE_CATEGORY_PRODUCT;
    }

    /**
     * get cache product data by id
     *
     * @param int $product_id
     * @return array|false|mixed
     */
    public static function getProductByID(int $product_id)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_KEY_BY_ID,
            $product_id
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * get cache product data by no
     *
     * @param string $product_no
     * @return array|false|mixed
     */
    public static function getProductByNo(string $product_no)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_KEY_BY_NO,
            $product_no
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * set cache product data by id
     *
     * @param int $product_id
     * @param array $product_data
     * @return bool
     * @throws \JsonException
     */
    public static function cacheProductById(int $product_id, array $product_data = []): bool
    {
        if (empty($product_data)) {
            $product_data = Product::with('productItems')->find($product_id);
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_KEY_BY_ID,
            $product_id
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($product_data, JSON_THROW_ON_ERROR));
    }

    /**
     * set cache product data by no
     *
     * @param string $product_no
     * @param array $product_data
     * @return bool
     * @throws \JsonException
     */
    public static function cacheProductByNo(string $product_no, array $product_data = []): bool
    {
        if (empty($product_data)) {
            $product_data = Product::with('productItems')->where('no', '=', $product_no)->first();
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_KEY_BY_NO,
            $product_no
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($product_data, JSON_THROW_ON_ERROR));
    }

    /**
     * delete cache product data by id
     *
     * @param $product_id
     */
    public static function deleteCacheProductById($product_id): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_KEY_BY_ID,
            $product_id
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }

    /**
     * delete cache product data by no
     *
     * @param $product_no
     */
    public static function deleteCacheProductByNo($product_no): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_KEY_BY_NO,
            $product_no
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }
}
