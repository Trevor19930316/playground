<?php


namespace App\Libraries\Cache;


use App\Libraries\Definition\CacheDefine;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class SystemCache extends BaseCache
{
    protected static function setCacheCategory(): string
    {
        return CacheDefine::CACHE_CATEGORY_SYSTEM;
    }

    /**
     * @throws \JsonException
     */
    public static function cacheTablesColumnsLang(): array
    {
        // 目前沒有多語系，僅支援中文，故寫死
        //$locale = !is_null(app()->getLocale()) ? app()->getLocale() : config("app.locale");

        $tables = DB::select('SHOW TABLES');
        $tableList = [];
        // 取得 database 使用軟體 mysql,sqlite...
        $databaseSoftware = config('database.default');
        // 取得 database scheme 名稱
        $database_var = "Tables_in_" . config('database.connections.' . $databaseSoftware . '.database');
        foreach ($tables as $table) {
            $tableList[] = $table->$database_var;
        }

        $cacheTablesColumnsLang = [];

        foreach ($tableList as $table) {

            $langList = __("tables/$table/columns");
            $langList = is_array($langList) ? $langList : [];

            $columns_lang_list = [];
            foreach ($langList as $key => $lang) {
                $columns_lang_list[$table . "_" . $key] = $lang;
            }
            if (!empty($columns_lang_list)) {
                $cacheTablesColumnsLang[] = $columns_lang_list;
            }
        }

        self::setSingleCacheData(CacheDefine::CACHE_KEY_LANG_TABLE_FIELD_TW, json_encode($cacheTablesColumnsLang, JSON_THROW_ON_ERROR));

        return $cacheTablesColumnsLang;
    }
}
