<?php


namespace App\Libraries\Cache;

use App\Libraries\Definition\CacheDefine;
use Illuminate\Support\Facades\Redis;

abstract class BaseCache
{
    /**
     * Set cache Category
     *
     * @return string
     */
    abstract protected static function setCacheCategory(): string;

    /**
     * Get cache key begin
     *
     * @return string
     */
    protected static function getCacheKeyBegin(): string
    {
        return static::setCacheCategory() . CacheDefine::SEPARATOR;
    }

    /**
     * Get cache key
     *
     * @param $cache_key
     * @return string
     */
    protected static function getSingleCacheKey($cache_key): string
    {
        return static::getCacheKeyBegin() . $cache_key;
    }

    /**
     * Get multiple cache key
     *
     * @param array $combine_cache_keys
     * @return string
     */
    protected static function getMultipleCacheKey(array $combine_cache_keys): string
    {
        $cacheKey = static::getCacheKeyBegin();
        if (is_array($combine_cache_keys) && count($combine_cache_keys) > 1) {
            foreach ($combine_cache_keys as $cache_key) {
                $cacheKey .= $cache_key . CacheDefine::SEPARATOR;
            }
            return substr($cacheKey, 0, -1);
        }
        return $cacheKey;
    }

    /**
     * Check cache key exists
     *
     * @param $cache_key
     * @return bool
     */
    protected static function checkKeyIsExists($cache_key): bool
    {
        return Redis::exists($cache_key);
    }

    /**
     * Check single cache key exists
     *
     * @param $cache_key
     * @return bool
     */
    protected static function checkSingleKeyIsExists($cache_key): bool
    {
        return self::checkKeyIsExists(self::getSingleCacheKey($cache_key));
    }

    /**
     * Check single cache key exists
     *
     * @param array $combine_cache_keys
     * @return bool
     */
    protected static function checkMultipleKeyIsExists(array $combine_cache_keys): bool
    {
        return self::checkKeyIsExists(self::getMultipleCacheKey($combine_cache_keys));
    }

    /**
     * Get cache data
     *
     * @param $cache_key
     * @return mixed
     */
    public static function getCacheData($cache_key)
    {
        return Redis::get($cache_key);
    }

    /**
     * Get single cache data
     *
     * @param $cache_key
     * @return mixed
     */
    public static function getSingleCacheData($cache_key)
    {
        return self::getCacheData(self::getSingleCacheKey($cache_key));
    }

    /**
     * Get multiple cache data
     *
     * @param array $combine_cache_keys
     * @return mixed
     */
    public static function getMultipleCacheData(array $combine_cache_keys)
    {
        return self::getCacheData(self::getMultipleCacheKey($combine_cache_keys));
    }

    /**
     * Set cache data
     *
     * @param $cache_key
     * @param $data
     * @return bool
     */
    public static function setCacheData($cache_key, $data): bool
    {
        Redis::set($cache_key, $data);

        return true;
    }

    /**
     * Set single cache data
     *
     * @param $cache_key
     * @param $data
     * @return bool
     */
    public static function setSingleCacheData($cache_key, $data): bool
    {
        return self::setCacheData(self::getSingleCacheKey($cache_key), $data);
    }

    /**
     * Set multiple cache data
     *
     * @param array $combine_cache_keys
     * @param $data
     * @return bool
     */
    public static function setMultipleCacheData(array $combine_cache_keys, $data): bool
    {
        return self::setCacheData(self::getMultipleCacheKey($combine_cache_keys), $data);
    }

    /**
     * Delete cache data
     *
     * @param $cache_key
     * @return bool
     */
    public static function delCacheData($cache_key): bool
    {
        Redis::del($cache_key);

        return true;
    }

    /**
     * Delete single cache data
     *
     * @param $cache_key
     * @return bool
     */
    public static function delSingleCacheData($cache_key): bool
    {
        return self::delCacheData(self::getSingleCacheKey($cache_key));
    }

    /**
     * Delete multiple cache data
     *
     * @param array $combine_cache_keys
     * @return bool
     */
    public static function delMultipleCacheData(array $combine_cache_keys): bool
    {
        return self::delCacheData(self::getMultipleCacheKey($combine_cache_keys));
    }
}
