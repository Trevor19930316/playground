<?php


namespace App\Libraries\Cache;


use App\Libraries\Definition\CacheDefine;
use App\Models\ProductItem;
use Illuminate\Support\Facades\Redis;

class ProductItemCache extends BaseCache
{
    protected static function setCacheCategory(): string
    {
        return CacheDefine::CACHE_CATEGORY_PRODUCT_ITEM;
    }

    /**
     * get cache product data by id
     *
     * @param int $product_item_id
     * @return array|false|mixed
     */
    public static function getProductItemByID(int $product_item_id)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_ITEM_KEY_BY_ID,
            $product_item_id
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * get cache product data by no
     *
     * @param string $product_item_no
     * @return array|false|mixed
     */
    public static function getProductItemByNo(string $product_item_no)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_ITEM_KEY_BY_NO,
            $product_item_no
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * set cache product data by id
     *
     * @param int $product_item_id
     * @param array $product_data
     * @return bool
     */
    public static function cacheProductItemById(int $product_item_id, array $product_data = []): bool
    {
        if (empty($product_data)) {
            $product_data = ProductItem::with('product')->find($product_item_id);
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_ITEM_KEY_BY_ID,
            $product_item_id
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($product_data, JSON_THROW_ON_ERROR));
    }

    /**
     * set cache product data by no
     *
     * @param string $product_item_no
     * @param array $product_data
     * @return bool
     */
    public static function cacheProductItemByNo(string $product_item_no, array $product_data = []): bool
    {
        if (empty($product_data)) {
            $product_data = ProductItem::with('product')->where('no', '=', $product_item_no)->first();
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_ITEM_KEY_BY_NO,
            $product_item_no
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($product_data, JSON_THROW_ON_ERROR));
    }

    /**
     * delete cache product data by id
     *
     * @param $product_item_id
     */
    public static function deleteCacheProductItemById($product_item_id): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_ITEM_KEY_BY_ID,
            $product_item_id
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }

    /**
     * delete cache product data by no
     *
     * @param $product_item_no
     */
    public static function deleteCacheProductItemByNo($product_item_no): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_PRODUCT_ITEM_KEY_BY_NO,
            $product_item_no
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }
}
