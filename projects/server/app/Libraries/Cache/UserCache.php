<?php


namespace App\Libraries\Cache;


use App\Libraries\Definition\CacheDefine;
use App\Models\User;

class UserCache extends BaseCache
{
    protected static function setCacheCategory(): string
    {
        return CacheDefine::CACHE_CATEGORY_USER;
    }

    /**
     * get cache user data by id
     *
     * @param int $user_id
     * @return mixed|null
     */
    public static function getUserByID(int $user_id)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_USER_KEY_BY_ID,
            $user_id
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * get cache user data by account
     *
     * @param string $user_account
     * @return mixed|null
     */
    public static function getUserByAccount(string $user_account)
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_USER_KEY_BY_ACCOUNT,
            $user_account
        ];
        $cacheKey = self::getMultipleCacheKey($cacheCombineKeys);

        // key 不存在
        if (!self::checkKeyIsExists($cacheKey)) {
            return null;
        }

        return self::getCacheData($cacheKey);
    }

    /**
     * set cache user data by id
     *
     * @param int $user_id
     * @param array $user_data
     * @return bool
     * @throws \JsonException
     */
    public static function cacheUserById(int $user_id, array $user_data = []): bool
    {
        if (empty($user_data)) {
            $user_data = User::find($user_id);
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_USER_KEY_BY_ID,
            $user_id
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($user_data, JSON_THROW_ON_ERROR));
    }

    /**
     * set cache user data by account
     *
     * @param string $user_account
     * @param array $user_data
     * @return bool
     * @throws \JsonException
     */
    public static function cacheUserByAccount(string $user_account, array $user_data = []): bool
    {
        if (empty($user_data)) {
            $user_data = User::where('account', '=', $user_account)->first();
        }

        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_USER_KEY_BY_ACCOUNT,
            $user_account
        ];

        return self::setMultipleCacheData($cacheCombineKeys, json_encode($user_data, JSON_THROW_ON_ERROR));
    }

    /**
     * delete cache user data by id
     *
     * @param $user_id
     */
    public static function deleteCacheUserById($user_id): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_USER_KEY_BY_ID,
            $user_id
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }

    /**
     * delete cache user data by account
     *
     * @param $user_account
     */
    public static function deleteCacheUserByAccount($user_account): void
    {
        $cacheCombineKeys = [
            CacheDefine::CACHE_KEY_USER_KEY_BY_ACCOUNT,
            $user_account
        ];

        self::delMultipleCacheData($cacheCombineKeys);
    }
}
