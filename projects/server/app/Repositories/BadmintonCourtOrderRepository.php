<?php


namespace App\Repositories;


use App\Libraries\Definition\ApiResponseDefine;
use App\Models\BadmintonCourtOrder;
use App\Repositories\Contracts\BadmintonCourtOrderRepositoryInterface;

class BadmintonCourtOrderRepository extends Repository implements BadmintonCourtOrderRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return BadmintonCourtOrder::class;
    }

    /**
     * @inheritdoc
     */
    public function showChild($no)
    {
        $data = $this->getByNo($no);

        if (is_null($data)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @inheritDoc
     */
    public function deleteData($id)
    {
        $order = $this->model->where('id', '=', $id)->first();

        if (is_null($order)) {
            return $this->APIResponseHandler->getFailureResponse([], APIResponseDefine::DELETE_FAILURE);
        }

        return parent::deleteData($id);
    }

    /**
     * @inheritDoc
     */
    public function withOrderItems()
    {
        return $this->model->with('orderItems');
    }

    /**
     * @inheritDoc
     */
    public function whereUserId($user_id)
    {
        return $this->model->where('user_id', '=', $user_id);
    }

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function whereByUserIdAndOrderId(int $user_id, int $order_id)
    {
        return $this->whereUserId($user_id)->where('id', $order_id);
    }

    /**
     * @inheritDoc
     */
    public function whereNo($no)
    {
        return $this->model->where('no', '=', $no);
    }

    /**
     * @inheritDoc
     */
    public function whereOrderDay(string $symbols, string $date)
    {
        return $this->model->where('order_day', $symbols, $date);
    }

    /**
     * @inheritDoc
     */
    public function getByUserId($user_id)
    {
        return $this->whereUserId($user_id)->get();
    }

    /**
     * @inheritDoc
     */
    public function getByNo($no)
    {
        return $this->whereNo($no)->first();
    }

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function getByUserIdAndOrderId(int $user_id, int $order_id)
    {
        return $this->whereByUserIdAndOrderId($user_id, $order_id)->first();
    }

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function getByUserIdAndOrderIdWithItems(int $user_id, int $order_id)
    {
        return $this->whereByUserIdAndOrderId($user_id, $order_id)
            ->with(['orderItems'])
            ->first();
    }

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function getByUserIdAndOrderIdWithUserSaleOrderLogs(int $user_id, int $order_id)
    {
        return $this->whereByUserIdAndOrderId($user_id, $order_id)
            ->with(['userSaleOrderLogs' => function ($query) {
                $query->with(['user' => function ($subQuery) {
                    $subQuery->select('id', 'name', 'account', 'phone_country', 'phone_number', 'email');
                }]);
                $query->with(['bonusUser' => function ($subQuery) {
                    $subQuery->select('id', 'name', 'account', 'phone_country', 'phone_number', 'email');
                }]);
            }])
            ->first();
    }
}
