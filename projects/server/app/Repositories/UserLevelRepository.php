<?php


namespace App\Repositories;


use App\Libraries\Definition\ApiResponseDefine;
use App\Models\UserLevel;
use App\Repositories\Contracts\UserLevelRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class UserLevelRepository extends Repository implements UserLevelRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return UserLevel::class;
    }

    /**
     * @inheritDoc
     */
    public function create($create_data)
    {
        $adminUser = Auth::guard()->user();

        $create_data['created_by'] = $adminUser->name;
        $create_data['updated_by'] = $adminUser->name;

        return parent::create($create_data);
    }

    /**
     * @inheritDoc
     */
    public function update($id, $update_data)
    {
        $adminUser = Auth::guard()->user();

        $update_data['updated_by'] = $adminUser->name;

        return parent::update($id, $update_data);
    }

    public function indexChild()
    {
        $data = $this->withUserLevelBonuses()->get();
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    public function showChild($id)
    {
        $data = $this->withUserLevelBonuses()->find($id);
        if (is_null($data)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @inheritDoc
     */
    public function withUserLevelBonuses()
    {
        return $this->model->with('userLevelBonuses');
    }

    /**
     * @inheritDoc
     */
    public function whereNo($no)
    {
        return $this->model->where('no', '=', $no);
    }

    /**
     * @inheritDoc
     */
    public function whereName($name)
    {
        return $this->model->where('name', '=', $name);
    }

    /**
     * @inheritDoc
     */
    public function isValid()
    {
        return $this->model->where('valid_flag', true);
    }

    /**
     * @inheritDoc
     */
    public function isInvalid()
    {
        return $this->model->where('valid_flag', false);
    }

    /**
     * @inheritDoc
     */
    public function getBonusLevelById($id)
    {
        return $this->getById($id)->getAttribute('bonus_level');
    }

    /**
     * @inheritDoc
     */
    public function getBonusLevelByNo($no)
    {
        return $this->whereNo($no)->select('bonus_level')->getAttribute('bonus_level');
    }

    /**
     * @inheritDoc
     */
    public function getIdList(array $valid_flag = [0, 1])
    {
        return $this->model->whereIn('valid_flag', $valid_flag)->pluck('id')->toArray();
    }
}
