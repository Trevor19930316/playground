<?php


namespace App\Repositories;


use App\Models\WebsiteInfo;
use App\Repositories\Contracts\WebsiteInfoRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class WebsiteInfoRepository extends Repository implements WebsiteInfoRepositoryInterface
{
    protected $convert_image_field = ['website_logo_url', 'title_logo_url'];

    /**
     * @inheritDoc
     */
    public function model()
    {
        return WebsiteInfo::class;
    }

    /**
     * @inheritDoc
     */
    public function create($create_data)
    {
        $adminUser = Auth::guard()->user();

        $create_data['created_by'] = $adminUser->name;
        $create_data['updated_by'] = $adminUser->name;

        return parent::create($create_data);
    }

    /**
     * @inheritDoc
     */
    public function update($id, $update_data)
    {
        $adminUser = Auth::guard()->user();

        $update_data['updated_by'] = $adminUser->name;

        return parent::update($id, $update_data);
    }
}
