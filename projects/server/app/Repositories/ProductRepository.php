<?php


namespace App\Repositories;


use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryInterface;

class ProductRepository extends Repository implements ProductRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * @inheritDoc
     */
    public function withProductItems()
    {
        return $this->model->with('productItems');
    }

    /**
     * @inheritDoc
     */
    public function whereNo($no)
    {
        return $this->model->where('no', $no);
    }

    /**
     * @inheritDoc
     */
    public function whereName($name)
    {
        return $this->model->where('name', $name);
    }

    /**
     * @inheritDoc
     */
    public function isValid()
    {
        return $this->model->where('valid_flag', true);
    }

    /**
     * @inheritDoc
     */
    public function isInvalid()
    {
        return $this->model->where('valid_flag', false);
    }

    /**
     * 取得 有效商品 關聯 有效規格
     * @inheritDoc
     */
    public function getWithProductItemsIsValid()
    {
        return $this->isValid()
            ->with(['productItems' => function ($query) {
                $query->where('valid_flag', true);
            }])
            ->get();
    }
}
