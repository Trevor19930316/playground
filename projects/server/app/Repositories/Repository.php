<?php


namespace App\Repositories;


use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Handler\APIResponseHandler;
use App\Libraries\Handler\ImageHandler;
use App\Libraries\Validation\ValidatorHandler;
use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Contracts\Services\ServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

abstract class Repository implements RepositoryInterface, ServiceInterface
{
    /**
     * @var Model
     */
    protected $model;

    protected $APIResponseHandler;

    /**
     * @var array
     */
    protected $validate_message;

    protected $convert_image_field = [];

    /**
     * @return string
     */
    abstract public function model();

    /**
     * Instantiate a new repository instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = app($this->model());
        $this->APIResponseHandler = new APIResponseHandler();
    }

    /**
     * 取得所有資料
     *
     * @return Collection|Model[]
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * 取得單一資料
     *
     * @param int $id
     * @return Model
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * @inheritDoc
     */
    public function whereId($id)
    {
        return $this->model->where('id', '=', $id);
    }

    /**
     * 驗證資料
     *
     * @param array $data
     * @param array $rule
     * @return array|mixed
     */
    public function validate($data, $rule)
    {
        $ValidatorHandler = new ValidatorHandler($this->model);
        $valid_status = $ValidatorHandler->validate($data, $rule);
        $this->validate_message = $ValidatorHandler->getErrorsMessages();

        return $valid_status;
    }

    /**
     * 取得資料驗證錯誤訊息
     *
     * @return mixed|void
     */
    public function getValidErrorsMessages()
    {
        return $this->validate_message;
    }

    /**
     * 資料顯示 for API (母包子，請參考 Product)
     *
     * @return JsonResponse
     */
    public function indexChild()
    {
        $data = $this->getAll();
        if (!empty($this->convert_image_field)) {
            $data = ImageHandler::convertImageToUrl($data, $this->convert_image_field);
            return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
        }
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * 資料新增 for API
     *
     * @param Request $request
     * @param array $validation_rule
     * @param bool $to_json_response
     * @return JsonResponse
     */
    public function createData($request, $validation_rule, $to_json_response = true)
    {
        $request_data = $request->all();

        // 驗證圖片
        if (!empty($request->allFiles())) {
            if (!ImageHandler::validateImages($request->allFiles(), $this->model)) {
                return $this->APIResponseHandler->getFailureResponse([], ImageHandler::getErrorMessage(), ImageHandler::getValidateErrorMessages());
            }
        }

        // 驗證資料
        if (!$this->validate($request_data, $validation_rule)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::CREATE_FAILURE, $this->validate_message);
        }

        // 上傳圖片
        if (!empty($request->allFiles())) {
            $upload_paths = ImageHandler::uploadImages($request->allFiles(), $this->model->getTable(), true);
            if (!$upload_paths) {
                return $this->APIResponseHandler->getFailureResponse([], ImageHandler::getErrorMessage(), ImageHandler::getValidateErrorMessages());
            }

            // 把圖片路徑取出來
            foreach ($upload_paths as $input_key => $path) {
                $request_data[$input_key] = $path;
            }
        }

        $data = $this->create($request_data);

        if (!empty($this->convert_image_field)) {
            $data = ImageHandler::convertImageToUrl($data->toArray(), $this->convert_image_field);
        }

        return !$to_json_response ? $data :
            $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::CREATE_SUCCESS);
    }

    /**
     * 新增資料
     *
     * @param $create_data
     * @return mixed
     */
    public function create($create_data)
    {
        return $this->model->create($create_data);
    }

    /**
     * 資料顯示 for API
     *
     * @param $id
     * @return JsonResponse
     */
    public function showChild($id)
    {
        $data = $this->getById($id);

        if (!empty($this->convert_image_field)) {
            $data = ImageHandler::convertImageToUrl($data->toArray(), $this->convert_image_field);
        }

        if (is_null($data)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * 資料更新 for API
     *
     * @param $id
     * @param Request $request
     * @param array $validation_rule
     * @param bool $to_json_response
     * @return JsonResponse
     */
    public function updateData($id, $request, $validation_rule, $to_json_response = true)
    {
        $request_data = $request->all();

        // 取出資料
        if (is_null($this->getById($id))) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        // 驗證圖片
        if (!empty($request->allFiles())) {
            if (!ImageHandler::validateImages($request->allFiles(), $this->model)) {
                return $this->APIResponseHandler->getFailureResponse([], ImageHandler::getErrorMessage(), ImageHandler::getValidateErrorMessages());
            }
        }

        // 驗證資料
        if (!$this->validate($request_data, $validation_rule)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::UPDATE_FAILURE, $this->validate_message);
        }

        // 上傳圖片
        if (!empty($request->allFiles())) {

            // 刪除舊有圖片
            $filedNames = array_keys($request->allFiles());
            foreach ($filedNames as $filedName) {
                $old_data = $this->getById($id);
                ImageHandler::deleteFile($old_data->$filedName);
            }

            $upload_paths = ImageHandler::uploadImages($request->allFiles(), $this->model->getTable(), true);
            if (!$upload_paths) {
                return $this->APIResponseHandler->getFailureResponse([], ImageHandler::getErrorMessage(), ImageHandler::getValidateErrorMessages());
            }

            // 把圖片路徑取出來
            foreach ($upload_paths as $input_key => $path) {
                $request_data[$input_key] = $path;
            }
        }

        $data = $this->update($id, $request_data);

        if (!empty($this->convert_image_field)) {
            $data = ImageHandler::convertImageToUrl($data->toArray(), $this->convert_image_field);
        }

        return !$to_json_response ? $data :
            $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::UPDATE_SUCCESS);
    }

    /**
     * 資料更新
     *
     * @param $id
     * @param $update_data
     * @return bool|mixed
     */
    public function update($id, $update_data)
    {
        $data = $this->getById($id);
        $data->update($update_data);

        return $data;
    }

    /**
     * 資料刪除 for API
     *
     * @param $id
     * @return JsonResponse
     */
    public function deleteData($id)
    {
        if (is_null($this->getById($id))) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        $this->delete($id);

        return $this->APIResponseHandler->getSuccessResponse([], ApiResponseDefine::DELETE_SUCCESS);
    }

    /**
     * 資料刪除
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete($id);
    }
}
