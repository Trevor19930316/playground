<?php


namespace App\Repositories;

use App\Libraries\Definition\SystemExtraDataDefine;
use App\Libraries\Handler\StringHandler;
use App\Models\SystemExtraData;
use App\Repositories\Contracts\SystemExtraDataRepositoryInterface;
use ReflectionClass;

class SystemExtraDataRepository extends Repository implements SystemExtraDataRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return SystemExtraData::class;
    }

    /**
     * @inheritDoc
     */
    public function whereType($type)
    {
        return $this->model->where('type', '=', $type);
    }

    /**
     * @inheritDoc
     */
    public function isSingle()
    {
        return $this->model->where('is_single', true);
    }

    /**
     * @inheritDoc
     */
    public function isNotSingle()
    {
        return $this->model->where('is_single', false);
    }

    /**
     * @param array $type_list
     * @return array
     */
    private static function getTypeListLang(array $type_list)
    {
        // TODO 從快取拿
        foreach ($type_list as $key => $value) {
            try {
                $type_list[$key] = StringHandler::getDefineLang(SystemExtraDataDefine::class, $key);
            } catch (\Exception $e) {
                // TODO 寫 log
            }
        }
        return $type_list;
    }

    /**
     * 取得所有類型清單
     *
     * @param $with_lang
     * @param bool $exclude_single_exist
     * @return array
     */
    public function getTypeList($with_lang = true, $exclude_single_exist = true)
    {
       $type_list = SystemExtraDataDefine::TYPE_LIST;
        if ($exclude_single_exist) {
            $exist_single_type_list = $this->isSingle()->groupBy('type')->pluck('type','type')->toArray();
            $type_list = array_diff($type_list, $exist_single_type_list);
        }

        if ($with_lang) {
            return StringHandler::getDefineLang(SystemExtraDataDefine::class, $type_list);
        }
        return $type_list;
    }

    /**
     * 取得只能儲存一筆的類型清單
     *
     * @param bool $with_lang
     * @return array
     */
    public static function getIsSingleTypeList($with_lang = true)
    {
        if ($with_lang) {
            return StringHandler::getDefineLang(SystemExtraDataDefine::class, SystemExtraDataDefine::SINGLE_TYPE_LIST);
        }
        return SystemExtraDataDefine::SINGLE_TYPE_LIST;
    }
}
