<?php


namespace App\Repositories;


use App\Models\BulletinBoard;
use App\Repositories\Contracts\BulletinBoardRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class BulletinBoardRepository extends Repository implements BulletinBoardRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return BulletinBoard::class;
    }

    /**
     * @inheritDoc
     */
    public function create($create_data)
    {
        $adminUser = Auth::guard()->user();

        $create_data['created_by'] = $adminUser->name;
        $create_data['updated_by'] = $adminUser->name;

        return parent::create($create_data);
    }

    /**
     * @inheritDoc
     */
    public function update($id, $update_data)
    {
        $adminUser = Auth::guard()->user();

        $update_data['updated_by'] = $adminUser->name;

        return parent::update($id, $update_data);
    }

    /**
     * @inheritDoc
     */
    public function orderBySort($sortBy = 'asc')
    {
        return $this->model->orderBy('sort', $sortBy);
    }

    /**
     * @inheritDoc
     */
    public function orderBySortDesc()
    {
        return $this->model->orderByDesc('sort');
    }

    /**
     * @inheritDoc
     */
    public function isValid()
    {
        return $this->model->where('valid_flag', true);
    }

    /**
     * @inheritDoc
     */
    public function isInvalid()
    {
        return $this->model->where('valid_flag', false);
    }

    /**
     * @inheritDoc
     */
    public function isValidAndDate($date = null)
    {
        $date = $date ?? now();
        return $this->isValid()->where('start_date', '<=', $date)->where('end_date', '>=', $date);
    }
}
