<?php


namespace App\Repositories;


use App\Models\Admin;
use App\Repositories\Contracts\AdminRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class AdminRepository extends Repository implements AdminRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return Admin::class;
    }

    /**
     * @inheritDoc
     */
    public function create($create_data)
    {
        $adminUser = Auth::guard()->user();

        $create_data['created_by'] = $adminUser->name;
        $create_data['updated_by'] = $adminUser->name;

        return parent::create($create_data);
    }

    /**
     * @inheritDoc
     */
    public function update($id, $update_data)
    {
        $adminUser = Auth::guard()->user();

        $update_data['updated_by'] = $adminUser->name;

        return parent::update($id, $update_data);
    }

    /**
     * @inheritDoc
     */
    public function whereAccount($account)
    {
        return $this->model->where('account', '=', $account);
    }

    /**
     * @inheritDoc
     */
    public function isSuper()
    {
        return $this->model->where('is_super', true);
    }

    /**
     * @inheritDoc
     */
    public function isNotSuper()
    {
        return $this->model->where('is_super', false);
    }

    /**
     * @inheritDoc
     */
    public function isValid()
    {
        return $this->model->where('valid_flag', true);
    }

    /**
     * @inheritDoc
     */
    public function isInvalid()
    {
        return $this->model->where('valid_flag', false);
    }

    /**
     * @inheritDoc
     */
    public function getByAccount($account)
    {
        return $this->whereAccount($account)->firstOrFail();
    }
}
