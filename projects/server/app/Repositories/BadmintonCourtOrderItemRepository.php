<?php


namespace App\Repositories;


use App\Models\BadmintonCourtOrderItem;
use App\Repositories\Contracts\BadmintonCourtOrderItemRepositoryInterface;

class BadmintonCourtOrderItemRepository extends Repository implements BadmintonCourtOrderItemRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return BadmintonCourtOrderItem::class;
    }

    /**
     * @inheritDoc
     */
    public function whereOrderId($order_id)
    {
        return $this->model->where('order_id', $order_id);
    }

    /**
     * @inheritDoc
     */
    public function withOrder()
    {
        return $this->model->with('order');
    }
}
