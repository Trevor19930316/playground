<?php


namespace App\Repositories;


use App\Libraries\Definition\ApiResponseDefine;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class UserRepository extends Repository implements UserRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @inheritDoc
     */
    public function create($create_data)
    {
        $adminUser = Auth::guard()->user();

        $create_data['created_by'] = $adminUser->name;
        $create_data['updated_by'] = $adminUser->name;

        return parent::create($create_data);
    }

    /**
     * @inheritDoc
     */
    public function update($id, $update_data)
    {
        $adminUser = Auth::guard()->user();

        $update_data['updated_by'] = $adminUser->name;

        return parent::update($id, $update_data);
    }

    public function indexChild()
    {
        $data = $this->withUserLevel()->get();
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    public function showChild($id)
    {
        $data = $this->withUserLevel()->find($id);
        if (is_null($data)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @inheritDoc
     */
    public function withUserLevel()
    {
        return $this->model->with('userLevel');
    }

    /**
     * @inheritDoc
     */
    public function withUserLevelBonuses()
    {
        return $this->model->with('userLevelBonuses');
    }

    /**
     * @inheritDoc
     */
    public function withUserLevelAndBonuses()
    {
        return $this->model->with(['userLevel', 'userLevelBonuses']);
    }

    /**
     * @inheritDoc
     */
    public function whereAccount($account)
    {
        return $this->model->where('account', $account);
    }

    /**
     * @inheritDoc
     */
    public function wherePhoneNumber($phone_number)
    {
        return $this->model->where('phone_number', $phone_number);
    }

    /**
     * @inheritDoc
     */
    public function getByAccount($account)
    {
        return $this->whereAccount($account)->first();
    }

    /**
     * @inheritDoc
     */
    public function getByPhoneNumber($phone_number)
    {
        return $this->wherePhoneNumber($phone_number)->first();
    }
}
