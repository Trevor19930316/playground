<?php


namespace App\Repositories;


use App\Models\DefinitionAddressPostal;
use App\Repositories\Contracts\DefinitionAddressPostalRepositoryInterface;

class DefinitionAddressPostalRepository extends Repository implements DefinitionAddressPostalRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return DefinitionAddressPostal::class;
    }

    /**
     * @inheritDoc
     */
    public function getIdList()
    {
        return $this->model->pluck('id')->toArray();
    }
}
