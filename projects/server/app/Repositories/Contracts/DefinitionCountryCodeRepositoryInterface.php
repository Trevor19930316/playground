<?php


namespace App\Repositories\Contracts;


interface DefinitionCountryCodeRepositoryInterface
{
    /**
     * 取得 id array
     *
     * @return mixed
     */
    public function getCountryCodeList();
}
