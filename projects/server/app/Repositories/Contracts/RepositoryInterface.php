<?php


namespace App\Repositories\Contracts;


use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * get data by id
     *
     * @param $id
     * @return Model
     */
    public function getById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function whereId($id);
}
