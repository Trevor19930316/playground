<?php


namespace App\Repositories\Contracts;


interface ProductRepositoryInterface
{
    /**
     * @return mixed
     */
    public function withProductItems();

    /**
     * @param string $no
     * @return mixed
     */
    public function whereNo($no);

    /**
     * @param string $name
     * @return mixed
     */
    public function whereName($name);

    /**
     * @return mixed
     */
    public function isValid();

    /**
     * @return mixed
     */
    public function isInvalid();

    /**
     * @return mixed
     */
    public function getWithProductItemsIsValid();
}
