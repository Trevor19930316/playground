<?php


namespace App\Repositories\Contracts;


use App\Models\BadmintonCourtOrder;

interface BadmintonCourtOrderRepositoryInterface
{
    /**
     * @return mixed
     */
    public function withOrderItems();

    /**
     * @param int $user_id
     * @return mixed
     */
    public function whereUserId(int $user_id);

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function whereByUserIdAndOrderId(int $user_id, int $order_id);

    /**
     * @param string $no
     * @return mixed
     */
    public function whereNo(string $no);

    /**
     * @param string $symbols
     * @param string $date
     * @return mixed
     */
    public function whereOrderDay(string $symbols, string $date);

    /**
     * @param int $user_id
     * @return mixed
     */
    public function getByUserId(int $user_id);

    /**
     * @param string $no
     * @return mixed
     */
    public function getByNo(string $no);

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function getByUserIdAndOrderId(int $user_id, int $order_id);

    /**
     * @param int $user_id
     * @param int $order_id
     * @return mixed
     */
    public function getByUserIdAndOrderIdWithItems(int $user_id, int $order_id);
}
