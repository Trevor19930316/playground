<?php


namespace App\Repositories\Contracts;


interface UserLevelRecordLogRepositoryInterface
{
    /**
     * @param int $user_id
     * @return mixed
     */
    public function whereUserId(int $user_id);

    /**
     * get data by account
     *
     * @param int $user_id
     * @return mixed
     */
    public function getByUserId(int $user_id);

}
