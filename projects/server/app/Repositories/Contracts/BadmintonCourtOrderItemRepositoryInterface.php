<?php


namespace App\Repositories\Contracts;


use App\Models\BadmintonCourtOrder;

interface BadmintonCourtOrderItemRepositoryInterface
{
    /**
     * @param int $order_id
     * @return mixed
     */
    public function whereOrderId(int $order_id);

    /**
     * @return mixed
     */
    public function withOrder();
}
