<?php


namespace App\Repositories\Contracts;


interface BulletinBoardRepositoryInterface
{
    /**
     * @param $sortBy
     * @return mixed
     */
    public function orderBySort($sortBy);

    /**
     * @return mixed
     */
    public function orderBySortDesc();

    /**
     * @return mixed
     */
    public function isValid();

    /**
     * @return mixed
     */
    public function isInvalid();

    /**
     * @param $date
     * @return mixed
     */
    public function isValidAndDate($date);
}
