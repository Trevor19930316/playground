<?php


namespace App\Repositories\Contracts;


interface UserRepositoryInterface
{
    /**
     * @return mixed
     */
    public function withUserLevel();

    /**
     * @return mixed
     */
    public function withUserLevelBonuses();

    /**
     * @return mixed
     */
    public function withUserLevelAndBonuses();

    /**
     * get data by account
     *
     * @param $account
     * @return mixed
     */
    public function getByAccount($account);

    /**
     * get data by phone number
     *
     * @param $phone_number
     * @return mixed
     */
    public function getByPhoneNumber($phone_number);

    /**
     * @param $account
     * @return mixed
     */
    public function whereAccount($account);

    /**
     * @param $phone_number
     * @return mixed
     */
    public function wherePhoneNumber($phone_number);
}
