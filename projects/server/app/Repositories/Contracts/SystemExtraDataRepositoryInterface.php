<?php


namespace App\Repositories\Contracts;


interface SystemExtraDataRepositoryInterface
{
    /**
     * @param $type
     * @return mixed
     */
    public function whereType($type);

    /**
     * @return mixed
     */
    public function isSingle();

    /**
     * @return mixed
     */
    public function isNotSingle();
}
