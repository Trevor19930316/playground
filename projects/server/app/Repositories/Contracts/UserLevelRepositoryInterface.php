<?php


namespace App\Repositories\Contracts;


interface UserLevelRepositoryInterface
{
    /**
     * @return mixed
     */
    public function withUserLevelBonuses();

    /**
     * @param $no
     * @return mixed
     */
    public function whereNo($no);

    /**
     * @param $name
     * @return mixed
     */
    public function whereName($name);

    /**
     * @return mixed
     */
    public function isValid();

    /**
     * @return mixed
     */
    public function isInvalid();

    /**
     * get data by account
     *
     * @param $id
     * @return mixed
     */
    public function getBonusLevelById($id);

    /**
     * @param $no
     * @return mixed
     */
    public function getBonusLevelByNo($no);

    /**
     * 取得會員等級 id
     *
     * @param array $valid_flag
     * @return mixed
     */
    public function getIdList(array $valid_flag = [0,1]);
}
