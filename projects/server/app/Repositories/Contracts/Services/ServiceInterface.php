<?php


namespace App\Repositories\Contracts\Services;


interface ServiceInterface
{
    /**
     * @param $data
     * @param $rule
     * @return mixed
     */
    public function validate($data, $rule);

    /**
     * @return mixed
     */
    public function getValidErrorsMessages();

    /**
     * create logic
     *
     * @param $data
     * @param $validation_rule
     * @return mixed
     */
    public function createData($data, $validation_rule);

    /**
     * create
     *
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * index child
     *
     * @return mixed
     */
    public function indexChild();

    /**
     * show child
     *
     * @param $id
     * @return mixed
     */
    public function showChild($id);

    /**
     * update logic
     *
     * @param $id
     * @param $data
     * @param $validation_rule
     * @return mixed
     */
    public function updateData($id, $data, $validation_rule);

    /**
     * update
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data);

    /**
     * delete logic
     *
     * @param $id
     * @return mixed
     */
    public function deleteData($id);

    /**
     * delete
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
