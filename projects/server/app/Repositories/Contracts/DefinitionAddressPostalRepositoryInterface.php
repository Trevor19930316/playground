<?php


namespace App\Repositories\Contracts;


interface DefinitionAddressPostalRepositoryInterface
{
    /**
     * 取得 id array
     *
     * @return mixed
     */
    public function getIdList();
}
