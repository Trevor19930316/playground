<?php


namespace App\Repositories\Contracts;


interface AdminRepositoryInterface
{
    /**
     * @param $account
     * @return mixed
     */
    public function whereAccount($account);

    /**
     * @return mixed
     */
    public function isSuper();

    /**
     * @return mixed
     */
    public function isNotSuper();

    /**
     * @return mixed
     */
    public function isValid();

    /**
     * @return mixed
     */
    public function isInvalid();

    /**
     * get data by account
     *
     * @param $account
     * @return mixed
     */
    public function getByAccount($account);
}
