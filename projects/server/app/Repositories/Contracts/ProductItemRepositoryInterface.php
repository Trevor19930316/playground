<?php


namespace App\Repositories\Contracts;


interface ProductItemRepositoryInterface
{
    /**
     * @return mixed
     */
    public function withProduct();

    /**
     * @param string $no
     * @return mixed
     */
    public function whereNo(string $no);

    /**
     * @param int $product_id
     * @return mixed
     */
    public function whereProductId(int $product_id);

    /**
     * @param string $name
     * @return mixed
     */
    public function whereName(string $name);

    /**
     * @return mixed
     */
    public function isValid();

    /**
     * @return mixed
     */
    public function isInvalid();

    /**
     * @param $no
     * @return mixed
     */
    public function getByNoWithProduct($no);

    /**
     * @param $no
     * @return mixed
     */
    public function getByNo($no);
}
