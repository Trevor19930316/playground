<?php


namespace App\Repositories;


use App\Models\DefinitionCountryCode;
use App\Repositories\Contracts\DefinitionCountryCodeRepositoryInterface;

class DefinitionCountryCodeRepository extends Repository implements DefinitionCountryCodeRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return DefinitionCountryCode::class;
    }

    /**
     * @inheritDoc
     */
    public function getCountryCodeList()
    {
        return $this->model->pluck('country_code')->toArray();
    }
}
