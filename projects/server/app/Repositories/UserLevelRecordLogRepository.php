<?php


namespace App\Repositories;


use App\Models\UserLevelRecordLog;
use App\Repositories\Contracts\UserLevelRecordLogRepositoryInterface;

class UserLevelRecordLogRepository extends Repository implements UserLevelRecordLogRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return UserLevelRecordLog::class;
    }

    /**
     * @inheritDoc
     */
    public function whereUserId($user_id)
    {
        return $this->model->where('user_id', '=', $user_id);
    }

    /**
     * @inheritDoc
     */
    public function getByUserId($user_id)
    {
        $this->model->whereUserId($user_id)->get();
    }
}
