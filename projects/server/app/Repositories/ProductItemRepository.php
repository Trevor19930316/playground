<?php


namespace App\Repositories;


use App\Libraries\Definition\ApiResponseDefine;
use App\Models\ProductItem;
use App\Repositories\Contracts\ProductItemRepositoryInterface;
use Illuminate\Http\JsonResponse;

class ProductItemRepository extends Repository implements ProductItemRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return ProductItem::class;
    }

    /**
     * @inheritDoc
     */
    public function withProduct()
    {
        return $this->model->with('product');
    }

    /**
     * @inheritDoc
     */
    public function whereNo($no)
    {
        return $this->model->where('no', $no);
    }

    /**
     * @inheritDoc
     */
    public function whereProductId($product_id)
    {
        return $this->model->where('product_id', $product_id);
    }

    /**
     * @inheritDoc
     */
    public function whereName($name)
    {
        return $this->model->where('name', $name);
    }

    /**
     * @inheritDoc
     */
    public function isValid()
    {
        return $this->model->where('valid_flag', true);
    }

    /**
     * @inheritDoc
     */
    public function isInvalid()
    {
        return $this->model->where('valid_flag', false);
    }

    /**
     * @inheritDoc
     */
    public function getByNoWithProduct($no)
    {
        return $this->withProduct()->whereNo($no)->first();
    }

    /**
     * @inheritDoc
     */
    public function getByNo($no)
    {
        return $this->whereNo($no)->first();
    }
}
