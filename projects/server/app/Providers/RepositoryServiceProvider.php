<?php

namespace App\Providers;

use App\Repositories\AdminRepository;
use App\Repositories\BulletinBoardRepository;
use App\Repositories\Contracts\AdminRepositoryInterface;
use App\Repositories\Contracts\BulletinBoardRepositoryInterface;
use App\Repositories\Contracts\DefinitionAddressPostalRepositoryInterface;
use App\Repositories\Contracts\DefinitionCountryCodeRepositoryInterface;
use App\Repositories\Contracts\OrderItemRepositoryInterface;
use App\Repositories\Contracts\OrderRepositoryInterface;
use App\Repositories\Contracts\ProductItemRepositoryInterface;
use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\Contracts\SystemExtraDataRepositoryInterface;
use App\Repositories\Contracts\UserLevelRecordLogRepositoryInterface;
use App\Repositories\Contracts\UserLevelRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\DefinitionAddressPostalRepository;
use App\Repositories\DefinitionCountryCodeRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ProductItemRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SystemExtraDataRepository;
use App\Repositories\UserLevelRecordLogRepository;
use App\Repositories\UserLevelRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->bind(BulletinBoardRepositoryInterface::class, BulletinBoardRepository::class);
        $this->app->bind(DefinitionAddressPostalRepositoryInterface::class, DefinitionAddressPostalRepository::class);
        $this->app->bind(DefinitionCountryCodeRepositoryInterface::class, DefinitionCountryCodeRepository::class);
        $this->app->bind(OrderItemRepositoryInterface::class, OrderItemRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(ProductItemRepositoryInterface::class, ProductItemRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(SystemExtraDataRepositoryInterface::class, SystemExtraDataRepository::class);
        $this->app->bind(UserLevelRecordLogRepositoryInterface::class, UserLevelRecordLogRepository::class);
        $this->app->bind(UserLevelRepositoryInterface::class, UserLevelRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            AdminRepositoryInterface::class,
            BulletinBoardRepositoryInterface::class,
            DefinitionAddressPostalRepositoryInterface::class,
            DefinitionCountryCodeRepositoryInterface::class,
            OrderItemRepositoryInterface::class,
            ProductItemRepositoryInterface::class,
            ProductRepositoryInterface::class,
            SystemExtraDataRepositoryInterface::class,
            UserLevelRecordLogRepositoryInterface::class,
            UserLevelRepositoryInterface::class,
            UserRepositoryInterface::class,
        ];
    }
}
