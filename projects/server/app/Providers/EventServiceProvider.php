<?php

namespace App\Providers;

use App\Models\BadmintonCourtOrder;
use App\Models\Product;
use App\Models\ProductItem;
use App\Models\User;
use App\Models\UserLevel;
use App\Observers\ProductItemObserver;
use App\Observers\ProductObserver;
use App\Observers\UserLevelObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        BadmintonCourtOrder::observe(BadmintonCourtOrder::class);           // 訂單 Observer
        User::observe(UserObserver::class);                                 // 會員 Observer
        UserLevel::observe(UserLevelObserver::class);                       // 會員等級 Observer
        Product::observe(ProductObserver::class);                           // 商品 Observer
        ProductItem::observe(ProductItemObserver::class);                   // 商品明細 Observer
    }
}
