<?php

namespace App\Observers;

use App\Libraries\Cache\ProductCache;
use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param \App\Models\Product $product
     * @return void
     * @throws \JsonException
     */
    public function created(Product $product)
    {
        // create cache
        ProductCache::cacheProductById($product->id);
        ProductCache::cacheProductByNo($product->no);
    }

    /**
     * Handle the Product "updated" event.
     *
     * @param \App\Models\Product $product
     * @return void
     * @throws \JsonException
     */
    public function updated(Product $product)
    {
        // update cache
        ProductCache::cacheProductById($product->id);
        ProductCache::cacheProductByNo($product->no);
    }

    /**
     * Handle the Product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        // delete cache
        ProductCache::deleteCacheProductById($product->id);
        ProductCache::deleteCacheProductByNo($product->no);
    }

    /**
     * Handle the Product "restored" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function restored(Product $product): void
    {
        //
    }

    /**
     * Handle the Product "force deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product): void
    {
        // delete cache
        ProductCache::deleteCacheProductById($product->id);
        ProductCache::deleteCacheProductByNo($product->no);
    }
}
