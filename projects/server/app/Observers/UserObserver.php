<?php

namespace App\Observers;

use App\Libraries\Cache\UserCache;
use App\Models\User;
use App\Repositories\UserLevelRecordLogRepository;
use App\Repositories\UserLevelRepository;

class UserObserver
{
    protected $userLevelRepository;
    protected $userLevelRecordLogRepository;
    protected $userLevelRecordLog;

    public function __construct(UserLevelRepository $userLevelRepository, UserLevelRecordLogRepository $userLevelRecordLogRepository)
    {
        $this->userLevelRepository = $userLevelRepository;
        $this->userLevelRecordLogRepository = $userLevelRecordLogRepository;
    }

    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $data = [
            'user_id' => $user->id,
            'old_user_level_id' => null,
            'new_user_levels_id' => $user->user_level_id,
            'created_by' => $user->created_by,
            'updated_by' => $user->created_by,
        ];
        $this->userLevelRecordLogRepository->create($data);

        // cache
        UserCache::cacheUserById($user->id, $user);
        UserCache::cacheUserByAccount($user->account, $user);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        if($user->isDirty('user_level_id')){

            $new_user_level_id = $user->user_level_id;
            $old_user_level_id = $user->getOriginal('user_level_id');

            $data = [
                'user_id' => $user->id,
                'old_user_level_id' => $old_user_level_id,
                'new_user_levels_id' => $new_user_level_id,
                'created_by' => $user->updated_by,
                'updated_by' => $user->updated_by,
            ];
            $this->userLevelRecordLog = $this->userLevelRecordLogRepository->create($data);
        }

        // cache
        UserCache::cacheUserById($user->id, $user);
        UserCache::cacheUserByAccount($user->account, $user);
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        // delete cache
        UserCache::deleteCacheUserById($user->id);
        UserCache::deleteCacheUserByAccount($user->account);
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        // delete cache
        UserCache::deleteCacheUserById($user->id);
        UserCache::deleteCacheUserByAccount($user->account);
    }
}
