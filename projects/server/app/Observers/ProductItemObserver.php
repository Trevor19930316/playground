<?php

namespace App\Observers;

use App\Libraries\Cache\ProductCache;
use App\Libraries\Cache\ProductItemCache;
use App\Models\Product;
use App\Models\ProductItem;

class ProductItemObserver
{
    /**
     * Handle the ProductItem "created" event.
     *
     * @param \App\Models\ProductItem $productItem
     * @return void
     * @throws \JsonException
     */
    public function created(ProductItem $productItem)
    {
        // create cache
        $product = Product::find($productItem->product_id);
        ProductCache::cacheProductById($product->id);
        ProductCache::cacheProductByNo($product->no);
        ProductItemCache::cacheProductItemById($productItem->id);
        ProductItemCache::cacheProductItemByNo($productItem->no);
    }

    /**
     * Handle the ProductItem "updated" event.
     *
     * @param \App\Models\ProductItem $productItem
     * @return void
     * @throws \JsonException
     */
    public function updated(ProductItem $productItem)
    {
        // update cache
        $product = Product::find($productItem->product_id);
        ProductCache::cacheProductById($product->id);
        ProductCache::cacheProductByNo($product->no);
        ProductItemCache::cacheProductItemById($productItem->id);
        ProductItemCache::cacheProductItemByNo($productItem->no);
    }

    /**
     * Handle the ProductItem "deleted" event.
     *
     * @param  \App\Models\ProductItem  $productItem
     * @return void
     */
    public function deleted(ProductItem $productItem): void
    {
        // delete cache
        $product = Product::find($productItem->product_id);
        ProductCache::deleteCacheProductById($product->id);
        ProductCache::deleteCacheProductByNo($product->no);
        ProductItemCache::deleteCacheProductItemById($productItem->id);
        ProductItemCache::deleteCacheProductItemByNo($productItem->no);

    }

    /**
     * Handle the ProductItem "restored" event.
     *
     * @param  \App\Models\ProductItem  $productItem
     * @return void
     */
    public function restored(ProductItem $productItem): void
    {
        //
    }

    /**
     * Handle the ProductItem "force deleted" event.
     *
     * @param  \App\Models\ProductItem  $productItem
     * @return void
     */
    public function forceDeleted(ProductItem $productItem): void
    {
        // delete cache
        $product = Product::find($productItem->product_id);
        ProductCache::deleteCacheProductById($product->id);
        ProductCache::deleteCacheProductByNo($product->no);
        ProductItemCache::deleteCacheProductItemById($productItem->id);
        ProductItemCache::deleteCacheProductItemByNo($productItem->no);
    }
}
