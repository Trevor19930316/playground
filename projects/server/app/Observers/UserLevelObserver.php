<?php

namespace App\Observers;

use App\Models\UserLevel;
use App\Repositories\UserLevelBonusRepository;
use App\Repositories\UserLevelRepository;

class UserLevelObserver
{
    protected $userLevelBonusesRepository;

    public function __construct(UserLevelBonusRepository $userLevelBonusRepository)
    {
        $this->userLevelBonusesRepository = $userLevelBonusRepository;
    }

    /**
     * Handle the UserLevel "created" event.
     *
     * @param  \App\Models\UserLevel  $userLevel
     * @return void
     */
    public function created(UserLevel $userLevel)
    {
        $this->userLevelBonusesRepository->createDefaultUserLevelBonus($userLevel->id);
    }

    /**
     * Handle the UserLevel "updated" event.
     *
     * @param  \App\Models\UserLevel  $userLevel
     * @return void
     */
    public function updated(UserLevel $userLevel)
    {
        //
    }

    /**
     * Handle the UserLevel "deleted" event.
     *
     * @param  \App\Models\UserLevel  $userLevel
     * @return void
     */
    public function deleted(UserLevel $userLevel)
    {
        //
    }

    /**
     * Handle the UserLevel "restored" event.
     *
     * @param  \App\Models\UserLevel  $userLevel
     * @return void
     */
    public function restored(UserLevel $userLevel)
    {
        //
    }

    /**
     * Handle the UserLevel "force deleted" event.
     *
     * @param  \App\Models\UserLevel  $userLevel
     * @return void
     */
    public function forceDeleted(UserLevel $userLevel)
    {
        //
    }
}
