<?php

namespace App\Observers;

use App\Libraries\Cache\BadmintonCourtOrderCache;
use App\Models\BadmintonCourtOrder;

class BadmintonCourtOrderObserver
{
    public function __construct()
    {
    }

    /**
     * Handle the User "created" event.
     *
     * @param BadmintonCourtOrder $order
     * @return void
     */
    public function created(BadmintonCourtOrder $order)
    {
    }

    /**
     * Handle the User "updated" event.
     *
     * @param BadmintonCourtOrder $order
     * @return void
     */
    public function updated(BadmintonCourtOrder $order)
    {
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param BadmintonCourtOrder $order
     * @return void
     */
    public function deleted(BadmintonCourtOrder $order)
    {
        // delete cache
        BadmintonCourtOrderCache::deleteCacheOrderById($order->id);
        BadmintonCourtOrderCache::deleteCacheOrderByNo($order->no);
    }

    /**
     * Handle the User "restored" event.
     *
     * @param BadmintonCourtOrder $order
     * @return void
     */
    public function restored(BadmintonCourtOrder $order)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param BadmintonCourtOrder $order
     * @return void
     */
    public function forceDeleted(BadmintonCourtOrder $order)
    {
        // delete cache
        BadmintonCourtOrderCache::deleteCacheOrderById($order->id);
        BadmintonCourtOrderCache::deleteCacheOrderByNo($order->no);
    }
}
