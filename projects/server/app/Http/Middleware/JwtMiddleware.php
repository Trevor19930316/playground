<?php

namespace App\Http\Middleware;

use App\Models\ApiRecordLog;
use Auth;
use Closure;
use Illuminate\Http\Request;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // 檢查請求是否包含 Token
        $this->checkForToken($request);

        try {
            // Token 驗證成功
            if ($this->auth->parseToken()->authenticate()) {
                return $next($request);
            };
        } catch (Exception $e) {

            if ($e instanceof TokenInvalidException) {

                $message = 'Token is Invalid';

            } else if ($e instanceof TokenExpiredException) {

                try {

                    // 取得新 Token 塞在 body 回傳
                    $token = $this->auth->refresh();
                    $response_data = [
                        'status' => true,
                        'message' => '',
                        'data' => [
                            'token' => $token,
                            'token_type' => 'bearer',
                            'expires_in' => auth()->factory()->getTTL() * 60,
                        ]
                    ];
                    return response()->json($response_data);
                    // 使用一次性登入保證此次請求成功 > 該換成 cache
                    //Auth::guard('api')->onceUsingId($this->auth->manager()->getPayloadFactory()->buildClaimsCollection()->toPlainArray()['sub']);
                    /*
                    $gracePeriod = $this->auth->manager()->getBlacklist()->getGracePeriod();
                    $expiresAt = Carbon::now()->addSeconds($gracePeriod);
                    Cache::put($key, $token, $expiresAt);
                    */
                    //$response = $this->setAuthenticationHeader($next($request), $token);
                    //return $response;

                } catch (TokenExpiredException $e) {
                    // Token 失效
                    $message = 'Token is Expired';
                }

            } elseif ($e instanceof TokenBlacklistedException) {

                // Token 失效
                $message = 'Token is Expired';

            } else {

                // Token 不存在
                $message = 'Authorization Token not found';
            }

            $response_data = [
                'status' => false,
                'message' => $message,
                'data' => [],
            ];
            return response()->json($response_data, Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
