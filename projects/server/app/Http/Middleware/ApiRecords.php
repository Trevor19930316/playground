<?php

namespace App\Http\Middleware;

use App\Models\ApiRecordLog;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiRecords
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        ApiRecordLog::createApiRecordLog($response);
        return $response;
    }
}
