<?php

namespace App\Http\Controllers\api;

use App\Libraries\Definition\ApiResponseDefine;
use App\Repositories\ProductItemRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductItemController extends BaseController
{
    private $repository;

    public function __construct(ProductItemRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $product_id
     * @return JsonResponse
     */
    public function index($product_id)
    {
        return $this->APIResponseHandler->getSuccessResponse($this->repository->whereProductId($product_id)->get(), ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $product_id
     * @return JsonResponse
     */
    public function store(Request $request, $product_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->repository->showChild($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //
    }
}
