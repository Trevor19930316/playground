<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Validation\ValidatorHandler;
use App\Models\ProductItem;
use App\Repositories\ProductItemRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductItemGPController extends BaseController
{
    private $repository;
    private $productItemRepository;

    public function __construct(ProductRepository $repository, ProductItemRepository $productItemRepository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->productItemRepository = $productItemRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $productWithItems = $this->repository->withProductItems()->get();

        $productsData = [];
        foreach ($productWithItems as $productWithItem) {
            $thisProductsData = [
                'id' => $productWithItem->id,
                'no' => $productWithItem->no,
                'name' => $productWithItem->name,
                'valid_flag' => $productWithItem->valid_flag,
                'created_at' => $productWithItem->created_at,
                'updated_at' => $productWithItem->updated_at,
            ];
            foreach ($productWithItem->productItems as $productItem) {
                $thisProductsData['product_items'][] = [
                    'id' => $productItem->id,
                    'no' => $productItem->no,
                    'name' => $productItem->name,
                    'gp' => $productItem->gp,
                    'valid_flag' => $productItem->valid_flag,
                    'created_at' => $productItem->created_at,
                    'updated_at' => $productItem->updated_at,
                ];
            }
            $productsData[] = $thisProductsData;
        }
        return $this->responseApiSuccess($productsData, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $product_id
     * @return JsonResponse
     */
    public function store(Request $request, $product_id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $product_item_id
     * @return JsonResponse
     */
    public function update(Request $request, int $product_item_id): JsonResponse
    {
        $productItem = $this->productItemRepository->getById($product_item_id);
        // 資料不存在
        if (is_null($productItem)) {
            return $this->responseApiFailure(ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        // 驗證規則
        $valid_rules = [
            'gp' => ['required', 'integer'],
        ];

        // 驗證資料
        $ValidatorHandler = new ValidatorHandler(ProductItem::class);
        $valid_status = $ValidatorHandler->validate($request->only('gp'), $valid_rules);
        if (!$valid_status) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::UPDATE_FAILURE, $ValidatorHandler->getErrorsMessages());
        }

        $productItem->update($request->only('gp'));
        return $this->responseApiSuccess($productItem, ApiResponseDefine::UPDATE_SUCCESS);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {

    }
}
