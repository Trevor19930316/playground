<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Models\FileUploadLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FileUploadLogController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $import_type = $request->input('type');

        $FileUploadLog = new FileUploadLog();
        $sql = $FileUploadLog->with(['fileImportLog' => function ($query) use ($import_type) {
            if ($import_type) {
                $query->where('import_type', $import_type);
            }
        }]);
        $data = $sql->get();

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $data = FileUploadLog::find($id);
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }
}
