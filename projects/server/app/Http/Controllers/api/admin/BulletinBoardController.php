<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use App\Repositories\BulletinBoardRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BulletinBoardController extends BaseController
{
    private $repository;

    public function __construct(BulletinBoardRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $order_by_sort = 'asc';
        if ($request->has('order_by_sort')) {
            $order_by_sort = $request->input('order_by_sort');
        }
        $data = $this->repository->orderBySort($order_by_sort)->get();
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        // 驗證規則
        $valid_rules = [
            'title' => ['required', 'max:100'],
            'subtitle' => ['required', 'max:100'],
            'start_date' => ['nullable', 'date_format:Y-m-d H:00:00'],
            'end_date' => ['nullable', 'date_format:Y-m-d H:00:00', 'after:' . $request->input('start_date')],
            'content' => ['required'],
            'valid_flag' => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
            'sort' => ['required', 'numeric', 'min:0'],
        ];

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        // 驗證規則
        $valid_rules = [
            'title' => ['max:100'],
            'subtitle' => ['max:100'],
            'start_date' => ['date_format:Y-m-d H:00:00'],
            'end_date' => ['date_format:Y-m-d H:00:00', 'after:' . $request->input('start_date')],
            'valid_flag' => [Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
            'sort' => ['numeric', 'min:0'],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        return $this->repository->deleteData($id);
    }
}
