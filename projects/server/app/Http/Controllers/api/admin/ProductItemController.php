<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use App\Repositories\ProductItemRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductItemController extends BaseController
{
    private $repository;

    public function __construct(ProductItemRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $product_id
     * @return JsonResponse
     */
    public function index($product_id): JsonResponse
    {
        return $this->APIResponseHandler->getSuccessResponse($this->repository->whereProductId($product_id)->get(), ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $product_id
     * @return JsonResponse
     */
    public function store(Request $request, $product_id): JsonResponse
    {
        // request 資料額外處理
        $request->merge([
            // 塞入商品id
            'product_id' => $product_id,
        ]);

        // 驗證規則
        $valid_rules = [
            'product_id' => ['required', 'integer'],
            'no' => ['required', 'unique:product_items,no', 'max:255'],
            'name' => ['required', 'max:100'],
            'price' => ['required', 'integer'],
            'gp' => ['required', 'integer'],
            'valid_flag' => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->repository->showChild($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            'no' => ['unique:product_items,no,' . $id, 'max:255'],
            'name' => ['max:100'],
            'price' => ['integer'],
            'gp' => ['integer'],
            'valid_flag' => [Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->repository->deleteData($id);
    }
}
