<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\DataDefine;
use App\Libraries\Definition\type\UserLevelBonusTypeDefine;
use App\Repositories\UserLevelRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UserLevelController extends BaseController
{
    private $repository;

    public function __construct(UserLevelRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            'no' => ['required', 'unique:user_levels,no', 'min:4', 'max:30'],
            'name' => ['required', 'max:30'],
            'bonus_type' => ['required', Rule::in(UserLevelBonusTypeDefine::USER_LEVEL_BONUS_TYPE_LIST)],
            'sort' => ['required', 'integer'],
            'valid_flag' => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            "no" => ["unique:user_levels,no,$id", 'min:4', 'max:30'],
            "name" => ['max:30'],
            'bonus_type' => ['required', Rule::in(UserLevelBonusTypeDefine::USER_LEVEL_BONUS_TYPE_LIST)],
            "sort" => ['integer'],
            "valid_flag" => [Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->repository->deleteData($id);
    }
}
