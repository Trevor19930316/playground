<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use App\Models\Admin;
use App\Repositories\AdminRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminController extends BaseController
{
    private $repository;

    public function __construct(AdminRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        /** @var Admin $adminUser */
        $adminUser = $request->user();

        // request 資料額外處理
        $request->merge([
            // 密碼加密
            "password" => password_hash($request->get("password"), PASSWORD_DEFAULT),
        ]);

        // 驗證規則
        $valid_rules = [
            "account" => ['required', 'unique:admins,account', 'min:4', 'max:30'],
            "password" => ['required'],
            "name" => ['required', 'max:30'],
            "valid_flag" => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        // 檢查系統管理者權限
        if ($request->has('is_super')) {

            if ((int)$adminUser->is_super !== 1) {
                $message = ApiResponseDefine::CREATE_FAILURE . __('punctuation.comma') . ApiResponseDefine::PERMISSION_DENIED;
                return self::responseApiFailure($message);
            }

            // 添加系統管理者規則
            $valid_rules['is_super'] = ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)];
        }

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        /** @var Admin $adminUser */
        $adminUser = $request->user();

        // 驗證規則
        $valid_rules = [
            "account" => ["unique:admins,account,$id", 'min:4', 'max:30'],
            "name" => ['required', 'max:30'],
            "valid_flag" => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        // request 有密碼
        if ($request->has('password')) {
            $request->merge([
                // 密碼加密
                'password' => password_hash($request->get('password'), PASSWORD_DEFAULT),
            ]);
        }

        // 檢查系統管理者權限 TODO 模組化
        if ($request->has('is_super')) {
            if ((int)$adminUser->is_super !== 1) {
                $message = ApiResponseDefine::UPDATE_FAILURE . __('punctuation.comma') . ApiResponseDefine::PERMISSION_DENIED;
                return self::responseApiFailure($message);
            }

            $valid_rules['is_super'] = ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)];
        }

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        /** @var Admin $adminUser */
        $adminUser = request()->user();

        // id 為 1 不可刪除
        if ((int)$id === 1) {
            if ((int)$adminUser->is_super !== DataDefine::BOOLEAN_TRUE_INT) {
                $message = __('tables/admins/extras.system_account_cannot_delete');
                return self::responseApiFailure($message);
            }
        }

        // 刪除資料
        return $this->repository->deleteData($id);
    }
}
