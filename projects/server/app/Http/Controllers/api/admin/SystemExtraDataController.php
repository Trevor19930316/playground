<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use App\Libraries\Validation\ValidatorHandler;
use App\Models\SystemExtraData;
use App\Repositories\SystemExtraDataRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class SystemExtraDataController extends BaseController
{
    private $repository;

    public function __construct(SystemExtraDataRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \ReflectionException
     */
    public function store(Request $request)
    {
        // 驗證規則
        $valid_rules = [
            "type" => ["required", Rule::in($this->repository->getTypeList(false))],
            "title" => ["required", "max:255"],
            "content" => ["required"],
        ];

        $request_data = $request->all();

        // 檢查是否為只能建立一次的資料
        $request_data['is_single'] = false;
        $single_type_list = $this->repository->getIsSingleTypeList(false);
        if (in_array($request_data['type'], $single_type_list)) {
            $request_data["is_single"] = true;
            $check_exists = $this->repository->whereType($request_data['type'])->first();
            if (!is_null($check_exists)) {
                return self::responseApiFailure(ApiResponseDefine::CREATE_REPEAT_FAILURE);
            }
        }

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws \ReflectionException
     */
    public function update(Request $request, $id)
    {
        // 驗證規則
        $valid_rules = [
            "type" => [Rule::in($this->repository->getTypeList(false, false))],
            "is_single" => [Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
            "title" => ['max:255'],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        return $this->repository->deleteData($id);
    }

    /**
     * 查詢 只能儲存一筆的類型清單
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTypeList(Request $request)
    {
        $request_data = $request->only('with_lang', 'exclude_single_exist');
        // 驗證規則
        $valid_rules = [
            "with_lang" => ["required", Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
            "exclude_single_exist" => ["required", Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];
        // 資料驗證
        $ValidatorHandler = new ValidatorHandler(SystemExtraData::class);
        $valid_status = $ValidatorHandler->validate($request_data, $valid_rules);
        if (!$valid_status) {
            return self::responseApiFailure(ApiResponseDefine::QUERY_FAILURE, $ValidatorHandler->getErrorsMessages());
        }

        $type_list = $this->repository->getTypeList($request_data["with_lang"], $request_data["exclude_single_exist"]);

        return self::responseApiSuccess($type_list, ApiResponseDefine::QUERY_SUCCESS);
    }
}
