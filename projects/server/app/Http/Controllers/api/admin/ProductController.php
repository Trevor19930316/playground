<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\DataDefine;
use App\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductController extends BaseController
{
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            'no' => ['required', 'unique:products,no', 'max:255'],
            'name' => ['required', 'max:100'],
            'valid_flag' => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            'no' => ['unique:products,no,' . $id, 'max:255'],
            'name' => ['max:100'],
            'valid_flag' => [Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->repository->deleteData($id);
    }
}
