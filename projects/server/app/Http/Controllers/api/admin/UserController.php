<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use App\Libraries\Validation\ValidatorHandler;
use App\Models\SystemExtraData;
use App\Repositories\DefinitionAddressPostalRepository;
use App\Repositories\DefinitionCountryCodeRepository;
use App\Repositories\UserLevelRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UserController extends BaseController
{
    private $repository;
    private $userLevelRepository;
    private $definitionAddressPostalRepository;
    private $definitionCountryCodeRepository;

    public function __construct(
        UserRepository                    $repository, UserLevelRepository $userLevelRepository,
        DefinitionAddressPostalRepository $definitionAddressPostalRepository, DefinitionCountryCodeRepository $definitionCountryCodeRepository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->userLevelRepository = $userLevelRepository;
        $this->definitionAddressPostalRepository = $definitionAddressPostalRepository;
        $this->definitionCountryCodeRepository = $definitionCountryCodeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        // request 資料額外處理
        $request->merge([
            // 密碼加密
            'password' => password_hash($request->get('password'), PASSWORD_DEFAULT),
        ]);

        // 驗證規則
        $valid_rules = [
            'user_level_id' => ['required', Rule::in($this->userLevelRepository->getIdList())],
            'account' => ['required', 'unique:users,account', 'min:4', 'max:30'],
            'password' => ['required'],
            'name' => ['required', 'max:30'],
            'phone_country' => ['required', Rule::in($this->definitionCountryCodeRepository->getCountryCodeList())],
            'phone_number' => ['required', 'numeric', 'unique:users,phone_number'],
            'email' => ['required', 'max:100', 'unique:users,email', 'email:rfc,dns'],
            'address_postal_id' => ['required', Rule::in($this->definitionAddressPostalRepository->getIdList())],
            'address' => ['required', 'max:255'],
            'valid_flag' => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            'user_level_id' => [Rule::in($this->userLevelRepository->getIdList())],
            'name' => ['max:30'],
            'phone_country' => [Rule::in($this->definitionCountryCodeRepository->getCountryCodeList())],
            'phone_number' => ['numeric', "unique:users,phone_number,$id"],
            'email' => ['max:100', "unique:users,email,$id", 'email:rfc,dns'],
            'address_postal_id' => [Rule::in($this->definitionAddressPostalRepository->getIdList())],
            'address' => ['max:255'],
            'valid_flag' => [Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->repository->deleteData($id);
    }

    /**
     * Reset User Password.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function resetPassword(int $id): JsonResponse
    {
        $user = $this->repository->getById($id);
        $new_password = password_hash(md5(substr($user->phone_number, -6)), PASSWORD_DEFAULT);

        // 驗證規則
        $valid_rules = ['password' => ['required'],];
        $update_data = ['password' => $new_password,];

        if (is_null($this->repository->getById($id))) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        // 資料驗證
        $ValidatorHandler = new ValidatorHandler(SystemExtraData::class);
        $valid_status = $ValidatorHandler->validate($valid_rules, $valid_rules);
        if (!$valid_status) {
            return self::responseApiFailure(ApiResponseDefine::QUERY_FAILURE, $ValidatorHandler->getErrorsMessages());
        }

        $this->repository->update($id, $update_data);

        return $this->APIResponseHandler->getSuccessResponse([], ApiResponseDefine::UPDATE_SUCCESS);
    }
}
