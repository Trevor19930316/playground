<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Controllers\api\ApiController;
use App\Models\Admin;

class BaseController extends ApiController
{
    /**
     * 定義 Auth Model
     * BaseController constructor.
     */
    public function __construct()
    {
        self::$_auth_model = Admin::class;
        parent::__construct();
    }

    /**
     * get phpinfo
     */
    public function getPhpinfo()
    {
        echo phpinfo();
    }
}
