<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Handler\CacheHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CacheController extends BaseController
{
    /**
     * @return JsonResponse
     */
    public function getCacheCategoryList(): JsonResponse
    {
        return $this->responseApiSuccess(CacheHandler::getCacheCategoryList(), ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @return JsonResponse
     */
    public function getCacheKeyList(): JsonResponse
    {
        return $this->responseApiSuccess(CacheHandler::getCacheKeyList(), ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSingleCacheKeyData(Request $request): JsonResponse
    {
        $category = $request->input('category');
        $key = $request->input('key');

        return $this->responseApiSuccess(CacheHandler::getSingleCacheKeyData($category, $key), ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getMultipleCacheKeyData(Request $request): JsonResponse
    {
        $category = $request->input('category');
        $key = $request->input('key');

        return $this->responseApiSuccess(CacheHandler::getMultipleCacheKeyData($category, $key), ApiResponseDefine::QUERY_SUCCESS);
    }
}
