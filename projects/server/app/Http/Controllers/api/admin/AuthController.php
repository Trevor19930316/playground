<?php

namespace App\Http\Controllers\api\admin;

use App\Repositories\AdminRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

// JWT
// https://www.positronx.io/laravel-jwt-authentication-tutorial-user-login-signup-api/

class AuthController extends BaseController
{
    private $repository;

    public function __construct(AdminRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * 管理者登入
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('account', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'account' => ['required', 'string', 'min:4', 'max:30'],
            'password' => ['required', 'string', 'min:4'],
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            $this->responseApiFailure(__('auth.login_failed'), $validator->message());
        }

        // Request is validated
        // Create token
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $message = __('auth.login_failed') .  __('punctuation.period');
                return self::responseApiFailure($message, [], Response::HTTP_UNAUTHORIZED);
            }
        } catch (JWTException $e) {
            $message = __('auth.login_failed') . __('punctuation.comma') . __('auth.please_contact_customer_service') . __('punctuation.period');
            return self::responseApiFailure($message, [], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // 檢查 valid_flag
        $adminUser = Auth::guard()->user();
        if ((int)$adminUser->valid_flag !== 1) {
            $message = __('auth.login_failed') . __('punctuation.comma') . __('auth.user_is_invalid') .  __('punctuation.period');
            return self::responseApiFailure($message);
        }

        //Token created, return with success response and jwt token
        return self::responseApiToken($token);
    }

    /**
     * 使用者登出
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        auth()->logout();
        $message = __('auth.logout_succeed') .  __('punctuation.period');
        return self::responseApiSuccess([], $message);
    }

    /**
     * 取得個人資料
     *
     * @return JsonResponse
     */
    public function getUserProfile()
    {
        $adminUser = Auth::guard()->user();
        if (is_null($adminUser)) {
            return self::responseApiFailure(__('auth.user_is_not_exists'));
        }
        return self::responseApiSuccess($adminUser);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return self::responseApiToken(auth()->refresh());
    }
}
