<?php

namespace App\Http\Controllers\api\admin;

use App\Repositories\WebsiteInfoRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WebsiteInfoController extends BaseController
{
    private $repository;

    public function __construct(WebsiteInfoRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        // 驗證規則
        $valid_rules = [
            'name' => ['max:30'],
            //'website_logo_url' => ['max:255'],
            'title' => ['max:30'],
            //'title_logo_url' => ['max:255'],
            'phone' => ['max:30'],
            'email' => ['max:100', 'email:rfc,dns'],
            'company_address' => ['max:100'],
            'line' => ['max:255'],
            'facebook' => ['max:255'],
            'instagram' => ['max:255'],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {

    }
}
