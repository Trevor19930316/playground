<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Models\FileImportLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FileImportLogController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $import_type = $request->input('type');

        $FileImportLog = new FileImportLog();
        $sql = $FileImportLog->with(['fileUploadLog']);
        if ($import_type) {
            $sql->where('import_type', $import_type);
        }
        $data = $sql->get();

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $data = FileImportLog::find($id);
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }
}
