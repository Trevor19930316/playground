<?php

namespace App\Http\Controllers\api\admin;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\FileImportDefine;
use App\Libraries\Handler\FileImportHandler;
use App\Libraries\Handler\StringHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DataImportController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $importAllowList = FileImportDefine::IMPORT_ALLOW_LIST;
        $importTypeList = StringHandler::getDefineLang(FileImportDefine::class, FileImportDefine::IMPORT_TYPE_LIST);
        $importModeList = StringHandler::getDefineLang(FileImportDefine::class,FileImportDefine::IMPORT_MODE_LIST);

        $data = [];

        foreach ($importAllowList as $importType => $importModeData) {
            $data['import_allow_list'][] = [
                'type' => $importType,
                'mode' => $importModeData,
            ];
        }

        foreach ($importTypeList as $importType => $text) {
            $data['import_type_list'][] = [
                'type' => $importType,
                'text' => $text,
            ];
        }

        foreach ($importModeList as $importMode => $text) {
            $data['import_mode_list'][] = [
                'mode' => $importMode,
                'text' => $text,
            ];
        }

        return self::responseApiSuccess($data, ApiResponseDefine::IMPORT_SUCCESS);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function import(Request $request): JsonResponse
    {
        $importType = $request->input('type');
        $importMode = $request->input('mode');
        $importFile = $request->file('file');

        $status = FileImportHandler::import($importType, $importMode, $importFile);

        if (!$status) {
            return self::responseApiFailure(ApiResponseDefine::IMPORT_FAIL);
        }

        return self::responseApiSuccess([], ApiResponseDefine::IMPORT_SUCCESS);
    }
}
