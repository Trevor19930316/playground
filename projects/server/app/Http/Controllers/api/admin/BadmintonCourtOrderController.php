<?php

namespace App\Http\Controllers\api\admin;

use App\Repositories\BadmintonCourtOrderRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BadmintonCourtOrderController extends BaseController
{
    private BadmintonCourtOrderRepository $repository;

    public function __construct(BadmintonCourtOrderRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param string $no
     * @return JsonResponse
     */
    public function show($no)
    {
        return $this->repository->showChild($no);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        return $this->repository->deleteData($id);
    }

    /**
     * @param Request $request
     * @param $mode
     * @return JsonResponse
     */
    public function import(Request $request, $mode): JsonResponse
    {
        $request->merge([
            'type' => 'order',
            'mode' => $mode,
        ]);

        return (new DataImportController())->import($request);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function import_logs(Request $request): JsonResponse
    {
        $request->merge([
            'type' => 'order',
        ]);

        return (new FileUploadLogController())->index($request);
    }
}
