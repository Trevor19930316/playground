<?php

namespace App\Http\Controllers\api;

use App\Models\User;

class BaseController extends ApiController
{
    /**
     * 定義 Auth Model
     * BaseController constructor.
     */
    public function __construct()
    {
        self::$_auth_model = User::class;
        parent::__construct();
    }
}
