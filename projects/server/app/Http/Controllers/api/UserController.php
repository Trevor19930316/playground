<?php

namespace App\Http\Controllers\api;

use App\Repositories\DefinitionAddressPostalRepository;
use App\Repositories\DefinitionCountryCodeRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UserController extends BaseController
{
    private $repository;
    private $definitionAddressPostalRepository;
    private $definitionCountryCodeRepository;

    public function __construct(UserRepository $repository, DefinitionAddressPostalRepository $definitionAddressPostalRepository, DefinitionCountryCodeRepository $definitionCountryCodeRepository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->definitionAddressPostalRepository = $definitionAddressPostalRepository;
        $this->definitionCountryCodeRepository = $definitionCountryCodeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        if (!is_null($request->get('password'))) {
            // request 資料額外處理
            $request->merge([
                // 密碼加密
                'password' => password_hash($request->get('password'), PASSWORD_DEFAULT),
            ]);
        }

        // 驗證規則
        $valid_rules = [
            'password' => ['max:255'],
            'name' => ['max:30'],
            'phone_country' => [Rule::in($this->definitionCountryCodeRepository->getCountryCodeList())],
            'phone_number' => ['numeric', "unique:users,phone_number,$id"],
            'email' => ['max:100', "unique:users,email,$id",'email:rfc,dns'],
            'address_postal_id' => [Rule::in($this->definitionAddressPostalRepository->getIdList())],
            'address' => ['max:255'],
        ];

        return $this->repository->updateData($id, $request, $valid_rules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
