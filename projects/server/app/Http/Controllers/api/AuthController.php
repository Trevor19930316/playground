<?php

namespace App\Http\Controllers\api;

use App\Libraries\Definition\DataDefine;
use App\Repositories\DefinitionAddressPostalRepository;
use App\Repositories\DefinitionCountryCodeRepository;
use App\Repositories\UserLevelRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use JWTAuth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

// JWT
// https://www.positronx.io/laravel-jwt-authentication-tutorial-user-login-signup-api/

class AuthController extends BaseController
{
    private $repository;
    private $userLevelRepository;
    private $definitionAddressPostalRepository;
    private $definitionCountryCodeRepository;

    public function __construct(
        UserRepository $repository, UserLevelRepository $userLevelRepository,
        DefinitionAddressPostalRepository $definitionAddressPostalRepository, DefinitionCountryCodeRepository $definitionCountryCodeRepository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->userLevelRepository = $userLevelRepository;
        $this->definitionAddressPostalRepository = $definitionAddressPostalRepository;
        $this->definitionCountryCodeRepository = $definitionCountryCodeRepository;
    }

    /**
     * 使用者註冊
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        // request 資料額外處理
        $request->merge([
            // 密碼加密
            'password' => $request->get('password'),
        ]);

        // 驗證規則
        $valid_rules = [
            'user_level_id' => ['required', Rule::in($this->userLevelRepository->getIdList())],
            'account' => ['required', 'unique:users,account', 'min:4', 'max:30'],
            'password' => ['required'],
            'name' => ['required', 'max:30'],
            'phone_country' => ['required', Rule::in($this->definitionCountryCodeRepository->getCountryCodeList())],
            'phone_number' => ['required', 'numeric', 'unique:users,phone_number'],
            'email' => ['required', 'max:100', 'unique:users,email', 'email:rfc,dns'],
            'address_postal_id' => ['required', Rule::in($this->definitionAddressPostalRepository->getIdList())],
            'address' => ['required', 'max:255'],
            'valid_flag' => ['required', Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];

        return $this->repository->createData($request, $valid_rules);
    }

    /**
     * 使用者登入
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('account', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'account' => ['required', 'string', 'min:4', 'max:30'],
            'password' => ['required', 'string', 'min:4'],
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return self::responseApiFailure(__('auth.login_failed'), $validator->getMessageBag()->toArray());
        }

        // Request is validated
        // Create token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                $message = __('auth.login_failed') .  __('punctuation.period');
                return self::responseApiFailure($message, [], Response::HTTP_UNAUTHORIZED);
            }
        } catch (JWTException $e) {
            $message = __('auth.login_failed') . __('punctuation.comma') . __('auth.please_contact_customer_service') . __('punctuation.period');
            return self::responseApiFailure($message, [], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // 檢查 valid_flag
        $user = Auth::guard()->user();
        if ((int)$user->valid_flag !== 1) {
            $message = __('auth.login_failed') . __('punctuation.comma') . __('auth.user_is_invalid') .  __('punctuation.period');
            return self::responseApiFailure($message);
        }

        //Token created, return with success response and jwt token
        return self::responseApiToken($token);
    }

    /**
     * 使用者登出
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        auth()->logout();
        $message = __('auth.logout_succeed') .  __('punctuation.period');
        return self::responseApiSuccess(null, $message);
    }

    /**
     * 取得個人資料
     *
     * @return JsonResponse
     */
    public function getUserProfile()
    {
        $user = Auth::guard()->user();
        if (is_null($user)) {
            return self::responseApiFailure(__('auth.user_is_not_exists'));
        }

        $userWithLevel = $this->repository->withUserLevel()->find($user->id);

        return self::responseApiSuccess($userWithLevel);
    }

    /**
     * 修改個人資料
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserProfile(Request $request)
    {
        $user = Auth::guard()->user();
        if (is_null($user)) {
            return self::responseApiFailure(__('auth.user_is_not_exists'));
        }

        // 驗證規則
        $valid_rules = [
            'name' => ['max:30'],
            'phone_country' => [ Rule::in($this->definitionCountryCodeRepository->getCountryCodeList())],
            'phone_number' => ['numeric', "unique:users,phone_number,$user->id"],
            'email' => ['max:100', "unique:users,email,$user->id",'email:rfc,dns'],
            'address_postal_id' => [Rule::in($this->definitionAddressPostalRepository->getIdList())],
            'address' => ['max:255'],
        ];

        return $this->repository->updateData($user->id, $request, $valid_rules);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return self::responseApiToken(auth()->refresh());
    }

    public function changePassword(Request $request)
    {
        $old_password = $request->input(['old_password']);
        $new_password = $request->input(['new_password']);

        if ($old_password === $new_password) {
            return self::responseApiFailure(__('auth.old_password_cannot_same_as_new_password'));
        }

        $user = Auth::guard()->user();
        if (is_null($user)) {
            return self::responseApiFailure(__('auth.user_is_not_exists'));
        }

        if (!Hash::check($old_password, $user->getAuthPassword())) {
            return self::responseApiFailure(__('auth.old_password_error'));
        }

        $UserRepository = new UserRepository();
        $UserRepository->update($user->id, ['password' => password_hash($new_password, PASSWORD_DEFAULT)]);

        return self::responseApiSuccess([], __('auth.change_password_success'));
    }
}
