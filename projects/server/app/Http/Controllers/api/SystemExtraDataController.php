<?php

namespace App\Http\Controllers\api;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Definition\DataDefine;
use App\Libraries\Validation\ValidatorHandler;
use App\Models\SystemExtraData;
use App\Repositories\SystemExtraDataRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class SystemExtraDataController extends BaseController
{
    private $repository;

    public function __construct(SystemExtraDataRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->repository->indexChild();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->repository->showChild($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //
    }

    /**
     * 查詢 只能儲存一筆的類型清單
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTypeList(Request $request)
    {
        $request_data = $request->only('with_lang', 'exclude_single_exist');
        // 驗證規則
        $valid_rules = [
            "with_lang" => ["required", Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
            "exclude_single_exist" => ["required", Rule::in(DataDefine::BOOLEAN_DATA_INT_LIST)],
        ];
        // 資料驗證
        $ValidatorHandler = new ValidatorHandler(SystemExtraData::class);
        $valid_status = $ValidatorHandler->validate($request_data, $valid_rules);
        if (!$valid_status) {
            return self::responseApiFailure(ApiResponseDefine::QUERY_FAILURE, $ValidatorHandler->getErrorsMessages());
        }

        $type_list = $this->repository->getTypeList($request_data["with_lang"], $request_data["exclude_single_exist"]);
        return self::responseApiSuccess($type_list, ApiResponseDefine::QUERY_SUCCESS);
    }
}
