<?php

namespace App\Http\Controllers\api;

use App\Libraries\Definition\ApiResponseDefine;
use App\Models\BadmintonCourtOrder;
use App\Repositories\BadmintonCourtOrderRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BadmintonCourtOrderController extends BaseController
{
    private BadmintonCourtOrderRepository $repository;
    private $user;

    public function __construct(BadmintonCourtOrderRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->user = Auth::guard()->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $data = $this->repository->getByUserId($this->user->id);
        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $data = $this->repository->getByUserIdAndOrderId($this->user->id, $id);

        if (is_null($data)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function badminton_court_order_items($id)
    {
        $data = $this->repository->getByUserIdAndOrderIdWithItems($this->user->id, $id);

        if (is_null($data)) {
            return $this->APIResponseHandler->getFailureResponse([], ApiResponseDefine::DATA_IS_NOT_EXISTS);
        }

        return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::QUERY_SUCCESS);
    }
}
