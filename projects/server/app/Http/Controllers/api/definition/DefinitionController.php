<?php

namespace App\Http\Controllers\api\definition;

use App\Http\Controllers\api\ApiController;
use App\Libraries\Cache\DefinitionCache;
use App\Libraries\Definition\CacheDefine;
use App\Models\DefinitionCountry;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class DefinitionController extends ApiController
{
    public function __construct()
    {
        self::$_auth_model = User::class;
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function getCountries(): JsonResponse
    {
        $data = json_decode(DefinitionCache::getSingleCacheData(CacheDefine::CACHE_KEY_DEFINITION_COUNTRY), true);
        return self::responseApiSuccess($data);
    }

    /**
     * @return JsonResponse
     */
    public function getAddress(): JsonResponse
    {
        $data = json_decode(DefinitionCache::getSingleCacheData(CacheDefine::CACHE_KEY_DEFINITION_ADDRESS_POSTAL), true);
        return self::responseApiSuccess($data);
    }

    /**
     * @return JsonResponse
     */
    public function getCountriesCode(): JsonResponse
    {
        $data = json_decode(DefinitionCache::getSingleCacheData(CacheDefine::CACHE_KEY_DEFINITION_COUNTRY_CODE), true);
        return self::responseApiSuccess($data);
    }
}
