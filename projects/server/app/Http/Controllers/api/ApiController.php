<?php

namespace App\Http\Controllers\api;

use App\Libraries\Definition\ApiResponseDefine;
use App\Libraries\Handler\APIResponseHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;
use JWTAuth;
use App\Http\Controllers\Controller;
use PHPUnit\Util\Exception;

abstract class ApiController extends Controller
{
    protected static $_auth_model = null;
    protected $APIResponseHandler;

    public function __construct()
    {
        $this->APIResponseHandler = new APIResponseHandler();

        if (is_null(self::$_auth_model)) {
            throw new Exception("Auth model is null.");
        }
        Config::set('jwt.user', self::$_auth_model);
        Config::set('auth.providers', [
            'users' => [
                'driver' => 'eloquent',
                'model' => self::$_auth_model,
            ]
        ]);
    }

    /**
     * API 成功回應
     *
     * @param $data
     * @param $message
     * @return JsonResponse
     */
    protected function responseApiSuccess($data, $message = '')
    {
        return $this->APIResponseHandler->setData($data)->setMessageLang($message)->getResponse();
    }

    /**
     * API 失敗回應
     *
     * @param string $message
     * @param $http_status
     * @param array $validation_error_message
     * @return JsonResponse
     */
    protected function responseApiFailure($message = '', array $validation_error_message = [], $http_status = Response::HTTP_OK)
    {
        if (!is_null($http_status)) {
            return $this->APIResponseHandler->isStatusFalse()->setMessage($message)->setHttpStatus($http_status)->setValidationErrorMessage($validation_error_message)->getResponse();
        }
        return $this->APIResponseHandler->getFailureResponse([], $message);
    }

    /**
     * API 成功回應 with Token
     *
     * @param $token
     * @return JsonResponse
     */
    protected function responseApiToken($token)
    {
        $data = [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ];
       return $this->APIResponseHandler->getSuccessResponse($data, ApiResponseDefine::GET_TOKEN_SUCCESS);
    }
}
