<?php

namespace App\Console\Commands;

use App\Libraries\Definition\CacheDefine;
use App\Libraries\Handler\CacheHandler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class CacheInitialize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initialize:CacheInitialize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize Cache';

    /**
     * 執行 function 清單
     *
     * @var array
     */
    protected $function_list = [
        "cacheTablesColumnsLang",
        "cacheDefinitionCountries",
        "cacheDefinitionAddressPostal",
        "cacheDefinitionCountriesCode",
        "cacheAllProductsAndProductItems",
        "cacheAllUsers",
    ];

    /**
     * function 使用到的 Cache key
     *
     * @var array
     */
    protected $function_use_cache_key = [
        "cacheTablesColumnsLang" => [
            CacheDefine::CACHE_KEY_LANG_TABLE_FIELD_TW,
            CacheDefine::CACHE_KEY_LANG_TABLE_FIELD_EN
        ],
        "cacheDefinitionCountries" => [
            CacheDefine::CACHE_KEY_DEFINITION_COUNTRY
        ],
        "cacheDefinitionAddressPostal" => [
            CacheDefine::CACHE_KEY_DEFINITION_ADDRESS_POSTAL
        ],
        "cacheDefinitionCountriesCode" => [
            CacheDefine::CACHE_KEY_DEFINITION_COUNTRY_CODE
        ],
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // 執行模式 Y > Function 全跑, N 逐一詢問
        $runType = $this->choice("Run all function?", ["Y", "N"], "Y");
        if ($runType === "Y") {
            $this->info("Run php artisan cache:clear");
            Artisan::call('cache:clear');
            $this->info("Run php artisan config:clear");
            Artisan::call('config:clear');
            $this->newLine();
        }

        $this->line("Start Initialize Cache.");

        foreach ($this->function_list as $function) {

            $runThis = "Y";
            if ($runType === "N") {
                $runThis = $this->choice("Run this function? $function()", ["Y", "N"], "Y");
            }

            if ($runThis === "Y") {
                // 有可能沒有設定 key
                if (isset($this->function_use_cache_key[$function])) {
                    $this->info("Clear cache start");
                    foreach ($this->function_use_cache_key[$function] as $cache_key) {
                        $this->info("Clear cache key : $cache_key");
                        Cache::forget($cache_key);
                    }
                }
                $this->info("Clear cache done");
                $this->info("Run start $function().");
                CacheHandler::$function();
                $this->info("Run finish $function().");
            }

            $this->line("Run Next.");
            $this->newLine();
        }

        return true;
    }
}
