<?php

namespace App\Console\Commands;

use App\Libraries\Handler\CurlHandler;
use App\Models\DefinitionAddressPostal;
use App\Models\DefinitionCountry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddressPostalInitialize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initialize:AddressPostalInitialize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize Address Postal Info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line("Run start initialize address postal info.");

        // https://opencube.tw/twzipcode
        $url = 'http://api.opencube.tw/twzipcode';
        $response = CurlHandler::get($url);
        $response = json_decode($response);

        if ($response !== false) {
            DB::table('definition_address_postal')->truncate();
            $response = $response->data;
            $definition_country_id = DefinitionCountry::getTaiwanId();
            $now = now();
            $insert_data = [];
            foreach ($response as $zip_data) {
                $insert_data[] = [
                    'definition_country_id' => $definition_country_id,
                    'zip_code' => $zip_data->zip_code,
                    'city' => $zip_data->city,
                    'district' => $zip_data->district,
                    'latitude' => $zip_data->lat,
                    'longitude' => $zip_data->lng,
                    'updated_at' => $now,
                    'created_at' => $now,
                ];
            }
            DefinitionAddressPostal::insert($insert_data);
        }

        $this->line("Run finish initialize address info.");

        return true;
    }
}
