<?php

return [
    'dot' => '.',
    'period' => '。',
    'comma' => '，',
    'semicolon' => ';',
    'exclamation' => '!',
    'at' => '@',
    'question_mark' => '?',
];
