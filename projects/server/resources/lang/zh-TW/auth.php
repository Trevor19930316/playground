<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login_failed' => '登入失敗',
    'logout_succeed' => '登出成功',
    'user_is_invalid' => '此為無效帳號',
    'token_create_failed' => 'Token 建立失敗',
    'please_contact_customer_service' => '請聯繫客服人員',
    'user_is_not_exists' => '使用者不存在',
    'old_password_cannot_same_as_new_password' => '舊密碼不能新密碼相同',
    'old_password_error' => '舊密碼錯誤',
    'change_password_success' => '變更密碼成功',
];
