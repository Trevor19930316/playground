<?php

return [
    'admins' => '管理員',
    'api_record_logs' => 'API 紀錄',
    'bulletin_boards' => '公佈欄',
    'cache' => '快取',
    'cache_locks' => 'cache locks',
    'failed_jobs' => 'failed jobs',
    'migrations' => 'migrations',
    'password_resets' => 'password resets',
    'product_items' => '商品明細',
    'products' => '商品',
    'sessions' => 'sessions',
    'system_extra_data' => '系統網站資料',
    'temp_logs' => '暫存紀錄',
    'user_levels' => '會員等級',
    'user' => '會員',
];
