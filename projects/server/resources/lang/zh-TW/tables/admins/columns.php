<?php

return [
    'id' => __('tables/base.id'),
    'account' => __('tables/base.account'),
    'password' => __('tables/base.password'),
    'name' => '名稱',
    'is_super' => '系統管理者',
    'valid_flag' => __('tables/base.valid_flag'),
    'created_at' => __('tables/base.created_at'),
    'updated_at' => __('tables/base.updated_at'),
];
