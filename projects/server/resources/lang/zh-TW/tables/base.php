<?php

return [
    'id' => 'id',
    'type' => '類型',
    'account' => '帳號',
    'password' => '密碼',
    'name' => '名字',
    'title' => '標題',
    'content' => '內容',
    'created_time' => '建立時間(時間戳)',
    'valid_flag' => '有效狀態',
    'created_at' => '建立於',
    'updated_at' => '更新於',
];
