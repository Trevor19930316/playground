<?php

return [
    'id' => __('tables/base.id'),
    'type' => __('tables/base.'),
    'title' => __('tables/base.title'),
    'content' => __('tables/base.content'),
    'created_time' => __('tables/base.created_time'),
    'created_at' => __('tables/base.created_at'),
    'updated_at' => __('tables/base.updated_at'),
];
