<?php

return [
    // Cache Category
    'system' => '系統',
    'definition' => '定義資料',
    'order' => '訂單',
    'product' => '商品',
    'product_item' => '商品明細',
    'user' => '會員',
    // Cache Key
    'lang_table_field_en' => '資料表欄位英文語系',
    'lang_table_field_tw' => '資料表欄位中文語系',
    'definition_country' => '國家資料',
    'definition_address_postal' => '地址資料',
    'definition_country_code' => '國碼資料',
    'order_key_by_id' => '訂單資料，健值為訂單id',
    'order_key_by_no' => '訂單資料，健值為訂單編號',
    'order_item_key_by_order_id' => '訂單明細資料，健值為訂單id',
    'order_item_key_by_order_no' => '訂單明細資料，健值為訂單編號',
    'product_key_by_id' => '商品資料，健值為商品id',
    'product_key_by_no' => '商品資料，健值為商品編號',
    'product_item_key_by_id' => '商品規格，健值為規格id',
    'product_item_key_by_no' => '商品規格，健值為規格編號',
    'user_key_by_id' => '會員資料，健值為會員id',
    'user_key_by_account' => '會員資料，健值為會員帳號',
];
