<?php

return [
    'put_file_failed' => '儲存檔案失敗',
    'import_type_is_not_legal' => '匯入類型不合法',
    'file_extension_is_not_legal' => '檔案副檔名不合法',
    'file_folder_is_empty' => '檔案資料夾不得為空',
    'model_not_exists' => 'Model 不存在',
    'image_validate_failed' => '圖檔資料驗證失敗',
];
