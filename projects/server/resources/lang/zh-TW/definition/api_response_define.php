<?php

return [
    'get_api_token_success' => '取得 API Token 成功',
    'get_api_token_failure' => '取得 API Token 失敗',

    'query_success' => '查詢成功',
    'query_failure' => '查詢失敗',

    'create_success' => '建立成功',
    'create_failure' => '建立失敗',
    'create_repeat_failure' => '此類型資料不可重複建立',

    'update_success' => '更新成功',
    'update_failure' => '更新失敗',

    'delete_success' => '刪除成功',
    'delete_failure' => '刪除失敗',

    'permission_denied' => '權限不足',
    'data_is_not_exists' => '資料不存在',

    'import_success' => '匯入成功',
    'import_fail' => '匯入失敗',
    'export_success' => '匯出成功',
    'export_fail' => '匯出失敗',
];
