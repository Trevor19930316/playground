<?php

return [
    // system_logs.type
    'log' => 'Log',
    'debug' => 'Debug',
    'info' => 'Info',
    'notice' => 'Notice',
    'warring' => 'Warring',
    'error' => 'Error',
];
