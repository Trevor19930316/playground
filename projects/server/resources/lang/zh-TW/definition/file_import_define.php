<?php

return [
    // 檔案匯入類型
    'order' => '訂單',
    'product' => '商品',
    'user' => '會員',

    // 檔案匯入模式
    'create' => '建立',
    'update' => '更新',
];
